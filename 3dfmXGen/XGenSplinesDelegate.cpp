#include "XGenSplinesDelegate.h"

#include <nsi.hpp>

#include <maya/MFnPluginData.h>
#include <maya/MPlug.h>
#include <maya/MPlugArray.h>
#include <maya/MPxData.h>
#include <maya/MTime.h>

#include <maya/MItDependencyGraph.h>

#if MAYA_API_VERSION >= 201700
#if MAYA_API_VERSION >= 20190000
#	include <xgen/src/xgsculptcore/api/XgSplineAPI.h>
#else
#	include <XGen/XgSplineAPI.h>
#endif

/*
	This define test comes from the interactive XGen example code. "Face Camera"
	attribute & the related info in the interactive XGen API were introduced in
	Maya 2017 update 4.

	For us, at the moment, it pretty much means that Maya 2018 is required to
	get the Face Camera option supported.
*/
#if defined(XGEN_SPLINE_API_VERSION) && XGEN_SPLINE_API_VERSION >= XGEN_SPLINE_VERSION_2017_UPDATE3
#define CAN_OUTPUT_NORMALS
#endif

#endif

#include <assert.h>
#include <sstream>
#include <float.h>

/*
	Fudge factor that tries to account for variations in hair density over the
	surface. Each footprint is made half the average footprint size to avoid
	blurring the texture too much in denser areas. This would not be required if
	we had a better method to compute hair footprints.
*/
static const float kFootprintMultiplier = 0.5f;

static bool
getAttrBool(const MObject& node, const MString& attr_name, bool default_value)
{
	if(node.isNull())
	{
		return default_value;
	}

    MFnDependencyNode node_fn(node);
    MPlug plug = node_fn.findPlug(attr_name, true);

    if(plug.isNull())
    {
		return default_value;
    }

	bool value = default_value;
	if(plug.getValue(value) == MStatus::kSuccess)
	{
		return value;
	}

	return default_value;
}


XGenSplinesDelegate::XGenSplinesDelegate( 
	MObject i_node, 
	NSIContext_t i_nsiContext, 
	const MString& i_handle,
	NSIUtils& i_utils,
	void* i_data)
:
	NSICustomDelegate( i_node, i_nsiContext, i_handle, i_utils, i_data ),
	m_readyForOutput( true ),
	m_initialOutputIsDone( false ),
	m_exportedN( false )
{
}

XGenSplinesDelegate::~XGenSplinesDelegate()
{
	RemoveCallbacks();
}

void XGenSplinesDelegate::Create()
{
	// One or more cubic curve children nodes will be output by SetAttributeAtTime
	NSICreate( m_context, m_handle.asChar(), "transform", 0, 0x0 );
}

void XGenSplinesDelegate::SetAttributes()
{
}

void XGenSplinesDelegate::SetAttributesAtTime( 
	double i_time, bool i_no_motion )
{
	//assert( IsValid() );

	// Do nothing unless we are OK to go.
	if( !m_readyForOutput )
		return;

#if MAYA_API_VERSION >= 201700
	/*
		Output the XGen spline shape data to a string stream, which is what the 
		XGen API expects to deal with.
	*/
	MStatus status;
	MFnDependencyNode depFn( m_object, &status );
	if(status != MStatus::kSuccess ) return;

	// Don't produce geometry for splines that are used as guides
	if( IsUsedAsGuide( depFn ) )
		return;

	/*
		Confirm that the node is indeed ready to go. The last monitorable event
		that happens when an XGen description is created during a live render 
		session is its assignment to a shading group. So, check if there is such
		an assignment, which is indicated by a connection on the instObjGroup[] 
		plug.

		Nodes that were existing when the render started will always pass this 
		check.

		When a new node is created during a live render, we will get here way before
		the shader assignment is done. The m_readyForOutput flag is set to false and
		we will wait for the proper event to turn it on. See AttributeChangedCB.
	*/
	MPlug plug = depFn.findPlug( MString( "instObjGroups" ), true, &status );
	if( plug.numConnectedElements() == 0 )
	{
		/*
			The XGen description setup is not complete yet. Don't attempt to
			output it as it will be broken
		*/
		m_readyForOutput = false;
		return;
	}

	MPlug dataPlug = depFn.findPlug( "outRenderData", true, &status );
	if(status != MStatus::kSuccess ) return;

	MFnPluginData fnPluginData( dataPlug.asMObject(), &status );
	if(status != MStatus::kSuccess ) return;

	MPxData* data = fnPluginData.data( &status );
	if(status != MStatus::kSuccess ) return;

	if( !data )
	{
		return;
	}

	bool cubic = IsCubic();

	std::stringstream opaqueStream;
	data->writeBinary( opaqueStream );

	using namespace XGenSplineAPI;

	/*
		The doc does not specify the expected size of the XgFnSpline::load()
		function, but based on the example Arnold procedural, it appears that the 
		correct thing to provide is the number of bytes to read.
	*/
	std::string opaqueData = opaqueStream.str();
	int size = opaqueData.size();

	/*
		Don't call the XGen API with data that is clearly invalid; this can happen
		when creating XGen descriptions during a live render; calling XGen with that
		data will break the new description initialization.
	*/
	if( size <= 0 )
	{
		return;
	}

	/*
		Figure the current value in seconds of user preference for fps. This is
		in Preferences window, Settings category -> Working Units -> Time:
	*/

	// 1 second
	MTime time( 1.0, MTime::kSeconds );

	// This 1 second expressed as the current Time preference
	double fps = time.as( MTime::uiUnit() );

	XgFnSpline fnSplines;
	fnSplines.setFps( fps );

	// Load the opaque data in the XgFnSpline object.
	fnSplines.load( opaqueStream, size, i_time );

	/*
		This is supposed to be called after all load() calls; some elements such 
		as density multipliers & maps will be applied by this call.
	*/
	fnSplines.executeScript();

	/*
		Fetch need faceCamera attribute value to figure if we must output normals.
		This will only be done with Maya 2018, as there is no way to access this
		data in 2017 before the 2017 update 4.
	*/
	bool faceCamera = true;

#ifdef CAN_OUTPUT_NORMALS
	MPlug faceCameraPlug = depFn.findPlug( "faceCamera", true );
	faceCameraPlug.getValue( faceCamera );
#endif

	NSI::Context nsi( m_context );
	
	/*
		There appears to be one iteration per shape on which this 
		xgmSplineDescription shape is growing hair on. We will output one NSI 
		curve node per iteration.
	*/
	int currIter = 0;

	for( XgItSpline itr = fnSplines.iterator(); !itr.isDone(); itr.next() )
	{
		const unsigned int numCurves = itr.primitiveCount();
		const unsigned int* primitiveInfos = itr.primitiveInfos();
		const unsigned int stride = itr.primitiveInfoStride();
		const SgVec3f* positions = itr.positions();
		const float* widths = itr.width();
		const SgVec3f* widthDirections = 0x0;
		const SgVec2f* patchUVs = itr.patchUVs();

		int numPoints = itr.vertexCount();

		int* nVertices = new int[ numCurves ];
		float* P = new float[ numPoints * 3 ];
		float* width = new float[ numPoints ];
		float* normal = 0x0;
		float* surfaceST = new float[ numCurves * 3 ];
		float st_bbox[4] = { FLT_MAX, FLT_MAX, -FLT_MAX, -FLT_MAX };

		if( !faceCamera )
		{
#ifdef CAN_OUTPUT_NORMALS
			widthDirections = itr.widthDirection();
			normal = new float[ numPoints * 3 ];
#endif
		}

		float* currP = P;
		float* currWidth = width;
		float* currNormal = normal;

		// Copy curve data
		for( int i = 0; i < numCurves; i++ )
		{
			int offset = primitiveInfos[ i * stride ];
			int length = primitiveInfos[ i * stride + 1 ];

			nVertices[ i ] = length;
			
			/*
				patchUVs contains the UV of the surface at the curve root; that value
				is duplicated once per hair vertex. We output it once per curve.
			*/
			float s, t;
			patchUVs[ offset ].getValue( s, t );
			surfaceST[ i * 3 ] = s;
			surfaceST[ i * 3 + 1 ] = t;
			st_bbox[0] = std::min(st_bbox[0], s);
			st_bbox[1] = std::min(st_bbox[1], t);
			st_bbox[2] = std::max(st_bbox[2], s);
			st_bbox[3] = std::max(st_bbox[3], t);

			for( int j = 0; j < length; j++ )
			{
				int index = j;

				positions[ offset + index ].getValue( currP[ 0 ], currP[ 1 ], currP[ 2 ] );
				currP += 3;
				*currWidth++ = widths[ offset + index ];

#ifdef CAN_OUTPUT_NORMALS
				if( !faceCamera )
				{
					int posIndex = offset + index;
					if( index == length - 1 )
					{
						posIndex --;
					}

					SgVec3f hairDirection = 
						positions[ posIndex + 1 ] - positions[ posIndex ];

					SgVec3f normal = widthDirections[ offset + index ] * hairDirection;

					normal.getValue( currNormal[ 0 ], currNormal[ 1 ], currNormal[ 2 ] );
					currNormal += 3;
				}
#endif
			}
		}

		/*
			Compute an approximation for the footprint of each curve on the
			attached surface's "st" domain. It will be used by 3Delight to
			compute derivatives for the texture coordinates, which will end up
			as an area for the texture lookup. This is repeated for each "st"
			coordinate in case we eventually need a more accurate computation.
			That could be achieved by splitting the surface in multiple areas or
			computing the average distance of the N nearest neighbors for each
			"st" coordinate.
		*/
		float surface_st_area =
			std::max(0.0f, st_bbox[2] - st_bbox[0]) *
			std::max(0.0f, st_bbox[3] - st_bbox[1]);
		float surface_st_curve_footprint =
			surface_st_area / float(numCurves) * kFootprintMultiplier;
		for(int i = 0; i < numCurves; i++)
		{
			surfaceST[ i * 3 + 2 ] = surface_st_curve_footprint;
		}

		MString currHandle = MString( m_handle ) + "|" + currIter;

		nsi.Create( currHandle.asChar(), "curves" );

		/* "extrapolate" is unused for linear curves so we always set it. */
		nsi.SetAttribute( currHandle.asChar(), (
			NSI::CStringPArg( "basis", cubic ? "catmull-rom" : "linear" ),
			NSI::IntegerArg( "extrapolate", 1 ) ) );

		nsi.Connect( currHandle.asChar(), "", m_handle.asChar(), "objects" );

		NSI::ArgumentList nsiAttributes;
		
		nsiAttributes.push(
			NSI::Argument::New( "nvertices" )
				->SetType( NSITypeInteger )
				->SetCount( numCurves )
				->SetValuePointer( nVertices ) );

		nsiAttributes.push(
			NSI::Argument::New( "P" )
				->SetType( NSITypePoint )
				->SetCount( numPoints )
				->SetValuePointer( P ) );

		nsiAttributes.push(
			NSI::Argument::New( "width" )
				->SetType( NSITypeFloat )
				->SetCount( numPoints )
				->SetValuePointer( width ) );

		if( !faceCamera )
		{
			nsiAttributes.push(
				NSI::Argument::New( "N" )
					->SetType( NSITypeNormal )
					->SetCount( numPoints )
					->SetValuePointer( normal ) );
		}
		else if( m_exportedN )
		{
			/*
				Since we don't need the normals that were exported last time,
				delete the attribute so changes on the "Face Camera" attributes
				are reflected correctly during live render.
			*/
			nsi.DeleteAttribute( currHandle.asChar(), "N" );
		}

		/*
			The 3rd value of "st" is the hair footprint. 3Delight knows how to
			handle this.
		*/
		nsiAttributes.push(
			NSI::Argument::New( "st" )
				->SetArrayType( NSITypeFloat, 3 )
				->SetCount( numCurves )
				->SetFlags( NSIParamPerFace )
				->SetValuePointer( surfaceST )  );

		if( i_no_motion )
			nsi.SetAttribute( currHandle.asChar(), nsiAttributes );
		else
			nsi.SetAttributeAtTime( currHandle.asChar(), i_time, nsiAttributes );

		delete[] nVertices;
		delete[] P;
		delete[] width;
		delete[] normal;
		delete[] surfaceST;

		currIter++;
	}

	// We can now ignore some events and process others.
	m_initialOutputIsDone = true;

	m_exportedN = !faceCamera;

#endif

}

bool XGenSplinesDelegate::RegisterCallbacks()
{
	//MObject& obj = (MObject&)Object();

	MCallbackId id = MNodeMessage::addAttributeChangedCallback(
		m_object, AttributeChangedCB, this );
	AddCallbackId( id );

	id = MNodeMessage::addNodeDirtyPlugCallback( m_object, NodeDirtyCB, this );
	AddCallbackId( id );

	return true;
}

void XGenSplinesDelegate::OverrideAttributes( MObject& i_attributes )
{
	m_overrides = MObjectHandle( i_attributes );
}

void XGenSplinesDelegate::RemoveCallbacks()
{
	for( int i = 0; i < m_ids.size(); i++ )
	{
		MMessage::removeCallback( m_ids[i] );
	}
}

void XGenSplinesDelegate::Connect()
{

}

void XGenSplinesDelegate::AttributeChangedCB(
	MNodeMessage::AttributeMessage i_msg,
	MPlug& i_plug,
	MPlug& i_otherPlug,
	void* i_delegate)
{
	bool processEvent = false;
	XGenSplinesDelegate* delegate = (XGenSplinesDelegate*)i_delegate;
	if( !delegate )
	{
		return;
	}

	/*
		Watch the kAttributeEval event on outSplineData. When this finally happpens
		on a XGen description created during a live render session, we may try to
		output it. Trying to do that before this event will disrupt the node and
		the viewport won't show it correctly.
	*/
	if( i_msg & MNodeMessage::kAttributeEval )
	{
		MString attrName = i_plug.partialName();

		/*
			Look for the outSplineData (osd) plug.

			Events like these happen way too often for us. Only process it if this
			will be the very first time that the XGen data will be translated to 
			NSI. The rest of the time, attribute change event & node dirty events
			are perfectly fine to figure if the XGen data must be re-output.
		*/
		if( attrName == "osd" && !delegate->m_initialOutputIsDone )
		{
			/* 
				Setting m_readyForOutput to true informs SetAttributesAtTime that it
				may proceed with its XGen API calls.
			*/
			delegate->m_readyForOutput = true;
			processEvent = true;
		}
	}
	else if( i_msg & MNodeMessage::kAttributeSet || 
		i_msg & MNodeMessage::kConnectionMade || 
		i_msg & MNodeMessage::kConnectionBroken )
	{
		/*
			Ignore events until the initial output (triggered by the code block 
			above) is done.
		*/
		if( delegate->m_initialOutputIsDone )
		{
			processEvent = true;
		}
	}

	if( processEvent )
	{
		delegate->SetAttributes( );
		delegate->SetAttributesAtTime( 0.0f, true );

		NSI::Context nsi( delegate->m_context ) ;
		nsi.RenderControl( NSI::CStringPArg( "action", "synchronize") );
	}
}

void XGenSplinesDelegate::NodeDirtyCB(
	MObject&,	MPlug& i_plug, void* i_delegate )
{
	// Ignore node dirty events unless the initial output has been done.
	XGenSplinesDelegate* delegate = (XGenSplinesDelegate*)i_delegate;

	/* 
		Handling dirty events allows us to refresh the XGen data when upstream nodes
		are modified - not all attributes that affect the splines are actually in
		the description shape.
	*/
	if( delegate && delegate->m_initialOutputIsDone )
	{
		delegate->SetAttributes( );
		delegate->SetAttributesAtTime( 0.0f, true );

		NSI::Context nsi( delegate->m_context );
		nsi.RenderControl( NSI::CStringPArg( "action", "synchronize") );
	}
}

bool XGenSplinesDelegate::IsUsedAsGuide( 
	const MFnDependencyNode& i_depFn ) const
{
	/*
		Returns true if the "outSplineData" plug of the specified node is connected
		to a xmgModifierGuide node.
	*/
	MStatus status = MStatus::kSuccess;
	MPlug outSplineData = i_depFn.findPlug( "outSplineData", true, &status );

	if(status != MStatus::kSuccess ) return false;

	MPlugArray destPlugs;
	if( outSplineData.connectedTo( destPlugs, false, true, &status ) )
	{
		for( unsigned int i = 0; i < destPlugs.length(); i++ )
		{
			if( MFnDependencyNode( destPlugs[ i ].node() ).typeName() == 
				MString( "xgmModifierGuide" ) )
			{
				return true;
			}
		}
	}

	return false;
}

bool XGenSplinesDelegate::IsCubic()const
{
	bool smooth = getAttrBool(m_object, "_3delight_smooth_curves", false);

	if(m_overrides.objectRef() != MObject::kNullObj &&
		getAttrBool(m_overrides.objectRef(), "enableSmoothCurves", false))
	{
		return getAttrBool(m_overrides.objectRef(), "smoothCurves", smooth);
	}

	return smooth;
}
