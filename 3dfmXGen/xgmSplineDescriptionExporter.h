/*
  Copyright (c) The 3Delight Team.
  Copyright (c) soho vfx inc.
*/

#ifndef __xgmSplineDescriptionExporter_h__
#define __xgmSplineDescriptionExporter_h__

#include <maya/MObject.h>
#include <maya/MPxCommand.h>
#include <maya/MSyntax.h>

#define EXPORT_CMD_STR "xgmSplineDescriptionExporter"

class xgmSplineDescriptionExporter : public MPxCommand
{

public:
	virtual MStatus doIt(const MArgList& i_args);

	virtual bool isUndoable() const;
	static void* creator();
	static MSyntax newSyntax();
};

#endif
