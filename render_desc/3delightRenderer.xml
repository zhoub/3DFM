<?xml version="1.0"?>
<!DOCTYPE renderer SYSTEM "renderer.dtd">

<!--
  Top level tag, mandatory:
   <renderer>: "desc" gives a one line description.

  Header tags, not mandatory, must be specified only once.
   <melheader>: "s" is a mel script executed just after the file is read 
   <meltrailer>: "s" is a mel script executed after all flags are converted
      to mel. Should contain at least the rendering command.

  Other tags:
   <sep>: "desc" produces a line in the help. Blank if desc is missing.
   <attr>: produces a setAttr line.
      "n" is the flag name.
      "s" the attribute name.
      "t" the parameter type, used in help description.
      "h" the help description.
   <attrString>: produces a setAttr line for a string attribute.
      Same paramters as <attr>, but for string attributes.
   <mel>: Calls a mel script.
      "n" is the flag name.
      "p" the number of parameters.
      "s" the string defining the action %1 ... %p are replaced with values
            read after the flag.
      "t" the parameter types, used in help description.
      "h" the help description.
-->
<renderer desc="3Delight renderer">
	<melheader s=
    '
    /* Kept for 3dfm_shave plugin name definition */
    float $version = getApplicationVersionAsFloat(); 
    string $version_string = $version; 

    string $plugin_name = "plugin_3delight_for_maya"; 

    if (!`pluginInfo -q -l $plugin_name`) 
    { 
      loadPlugin $plugin_name; 
    }

    string $opt = "";
    string $rp = "";
    string $rl = "";
    
    setAttr defaultRenderGlobals.renderAll 1; 
    RG_batchRenderInit(); 

    /* comma-separated list of render settings to render */
    string $settings_list = RG_getActiveRenderSettings();

    if ($settings_list == "")
    {
      $settings_list = RG_newActiveRenderSettings();
    }

    /* export filename */
    string $export_file = "";
    '/>
	<meltrailer s=
    '
    setMayaSoftwareLayers($rl, $rp);

    if ($settings_list != "") 
      $opt += "-renderSettingsList " + $settings_list + " ";

    if ($export_file != "")
      $opt += "-export \\\"" + encodeString($export_file) + "\\\" ";
    mayaBatchRenderProcedure(0, "", "", "_3Delight", $opt);
    '/>
	<sep/>
	<!-- ______________________________________________________________ -->
	<!--
  <sep desc="General options"/>
  <mel n="3dfmshave" p="1" s=
    '
    string $plugin_name = "3dfm_shave" + $version_string; 
    int $dfm_shave_plugin = `pluginInfo -q -l $plugin_name`; 

    if (%1) 
    { 
      if (!$dfm_shave_plugin) 
        loadPlugin $plugin_name; 
    } 
    else
    { 
      if ($dfm_shave_plugin) unloadPlugin $plugin_name; 
    }
    '
    t="bool" h="Load or unload 3dfmShave plugin."/>
	<sep/>
  -->
	<!-- ______________________________________________________________ -->
	<sep desc="Render Settings selection flags - specify this option FIRST:"/>
  <mel 
    n="rs"
    s='
    $settings_list = "%1";
    DCL_validateRenderSettingsList($settings_list);
    '
    p="1" t="string" 
    h="Defines the Render Settings to render with."/>
  <sep/>
  <!-- ______________________________________________________________ -->
  <sep desc="General purpose flags:"/>
  <mel
    n="rd"
    s='DCL_setImageDir($settings_list, "%1");'
    p="1" t="path"
    h="Directory in which to store image files"/>
  <mel
    n="im"
    s='DCL_setImageName($settings_list, "%1");'
    p="1" t="filename"
    h="Image file output name"/>
  <sep/>
  <!-- ______________________________________________________________ -->
  <sep desc="Mode option:"/>
  <mel 
    n="export"
    s='$export_file = "%1";'
    p="1" t="string"
    h="Exports to the specified NSI file. No image will be\n
                     rendered." />
	<!-- ______________________________________________________________ -->
  <sep/>
  <sep desc="Frame Range options:"/>
  <sep desc="        specifying any one of the following options will enable rendering"/>
  <sep desc="        of the frame range."/>"
  <mel 
    n="animation" 
    s='DCL_setAnimation($settings_list, %1);'
    p="1" t="bool"
    h="Toggles the rendering of the specified frame sequence\n
                     (animation) on or off."/>
  <mel 
    n="s" 
    s='DCL_setStartFrame($settings_list, %1);' 
    p="1" t="float"
    h="Animation sequence start frame number"/>
  <mel n="e" 
    s='DCL_setEndFrame($settings_list, %1);'
    p="1" t="float" 
    h="Animation sequence end frame number"/>
  <mel 
    n="increment" 
    s='DCL_setFrameIncrement($settings_list, %1);'
    p="1" t="float"
    h="Animation sequence frame increment"/>
  <!-- ______________________________________________________________ -->
  <sep/>
  <sep desc="Scene Elements options:"/>
  <mel 
    n="cam" 
    s='DCL_setCamera($settings_list, "%1");'
    p="1" t="string"
    h="Defines the camera to render."/>
  <mel 
    n="objects" 
    s='DCL_setObjectsToRender($settings_list, "%1");'
    p="1" t="string"
    h="Sets the object set to render. Specify an empty string to\n
                     render all visible objects."/>
  <mel 
    n="lights" 
    s='DCL_setLightsToRender($settings_list, "%1");'
    p="1" t="string"
    h="Sets the light set to render. Specify an empty string to\n
                     render all visible lights."/>
  <!-- ______________________________________________________________ -->
  <sep/>
  <sep desc="Quality options:"/>
  <mel
    n="pixelsamples"
    s='DCL_setPixelSamples($settings_list, %1);'
    p="1" t="int"
    h="Sets the number of pixel samples to use."/>
  <mel
    n="shadingsamples"
    s='DCL_setShadingSamples($settings_list, %1);'
    p="1" t="int"
    h="Set the number of shading samples to use."/>
  <mel
    n="pixelfilter"
    s='DCL_setPixelFilter($settings_list, "%1");'
    p="1" t="string"
    h="Sets the pixel filter type. Available filters are:\n
                     blackman-harris, mitchell, catmull-rom, sinc, box, triangle, gaussian"/>
  <mel
    n="filterwidth"
    s='DCL_setFilterWidth($settings_list, %1);'
    p="1" t="float"
    h="Sets the pixel filter width."/>
  <mel
    n="maxdistance"
    s='DCL_setMaxDistance($settings_list, %1);'
    p="1" t="int"
    h="Defines the maximum distance a ray can travel."/>

  <!-- ______________________________________________________________ -->
  <sep/>
  <sep desc="Motion Blur option:"/>
  <mel
    n="motionBlur"
    s='DCL_setEnableMotionBlur($settings_list, %1)'
    p="1" t="bool"
    h="Toggles motion blur on or off."/>
  <!-- ______________________________________________________________ -->
  <sep/>
	<sep desc="*** Remember to put a space between option flags and their arguments. ***"/>
	<sep desc="Any boolean flag will take the following values as TRUE: on, yes, true, or 1."/>
	<sep desc="Any boolean flag will take the following values as FALSE: off, no, false, or 0."/>
	<sep/>
<!--
	<sep desc="    e.g. -rp dlRenderPass1 file"/>
-->
</renderer>
