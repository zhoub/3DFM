if [[ -e ./setup.custom.sh ]]; then
	source ./setup.custom.sh
fi

# Set DEVROOT to (hopefully) the location of this file.
if [[ -z $DEVROOT ]]; then
	echo "Error: 3DFM build environment: DEVROOT variable not set"
	export DEVROOT=`pwd`
	echo "Error: 3DFM build environment: DEVROOT set to $DEVROOT"
fi

# Set PLAT to something platform-specific (matches 3Delight's setting)
if [[ -z $PLAT ]]; then
	export PLAT=`uname -s`-`uname -m | sed -e's/ /-/g'`
	echo "PLAT variable set to" $PLAT
fi

# Also set OS
if [[ -z $OS ]]; then
	export OS=`uname`
	echo "OS variable set to" $OS
fi

# 3Delight home directory where various stuff is needed for the build.
if [[ -z $HOME_3DELIGHT ]]; then
	export HOME_3DELIGHT=/net1/3delight
fi

export PATH=/usr/local/gcc332/bin:$DEVROOT/bin:${PATH}

# Set Maya environment (only needed to run maya, not for compiling)
if [[ -n $MAYA_LOCATION ]]; then
	VERSION=`echo $MAYA_LOCATION | sed -e 's/.*maya\([[:digit:]]\{4,\}\(\.[[:digit:]]\{1,\}\)\{0,1\}\).*/\1/'`
	if [[ -n $MAYA_PLUG_IN_PATH ]]; then
		export MAYA_PLUG_IN_PATH=${DEVROOT}/build/$PLAT/$VERSION/plugins:${MAYA_PLUG_IN_PATH}
	else
		export MAYA_PLUG_IN_PATH=${DEVROOT}/build/$PLAT/$VERSION/plugins
	fi
else
	# If MAYA_LOCATION is not set, then add all possible plug-in paths; this will
	# not work with plug-in auto-load.
	#
	if [[ -n $MAYA_PLUG_IN_PATH ]]; then
		export MAYA_PLUG_IN_PATH=${DEVROOT}/build/$PLAT/2022/plugins:${MAYA_PLUG_IN_PATH}
	else
		export MAYA_PLUG_IN_PATH=${DEVROOT}/build/$PLAT/2022/plugins
	fi
	MAYA_PLUG_IN_PATH=${DEVROOT}/build/$PLAT/2020/plugins:${MAYA_PLUG_IN_PATH}
	MAYA_PLUG_IN_PATH=${DEVROOT}/build/$PLAT/2019/plugins:${MAYA_PLUG_IN_PATH}
	MAYA_PLUG_IN_PATH=${DEVROOT}/build/$PLAT/2018/plugins:${MAYA_PLUG_IN_PATH}
fi

if [[ -n $MAYA_SCRIPT_PATH ]]; then
	export MAYA_SCRIPT_PATH=${DEVROOT}/build/$PLAT/scripts:${MAYA_SCRIPT_PATH}
else
	export MAYA_SCRIPT_PATH=${DEVROOT}/build/$PLAT/scripts	
fi

if [[ -n $PYTHONPATH ]]; then
	export PYTHONPATH=${DEVROOT}/build/$PLAT/scripts:${PYTHONPATH}
else
	export PYTHONPATH=${DEVROOT}/build/$PLAT/scripts
fi

if [[ -n $MAYA_RENDER_DESC_PATH ]]; then
	export MAYA_RENDER_DESC_PATH=${DEVROOT}/render_desc:${MAYA_RENDER_DESC_PATH}
else
	export MAYA_RENDER_DESC_PATH=${DEVROOT}/render_desc
fi

if [[ $OS == "Darwin" ]]; then
	if [[ -n $XBMLANGPATH ]]; then
		export XBMLANGPATH=${DEVROOT}/icons/:${XBMLANGPATH}
	else
		export XBMLANGPATH=${DEVROOT}/icons/
	fi
else
	if [[ -n $XBMLANGPATH ]]; then
		export XBMLANGPATH=${DEVROOT}/icons/%B:${XBMLANGPATH}
	else
		export XBMLANGPATH=${DEVROOT}/icons/%B
	fi
fi

if [[ -n $MAYA_PRESET_PATH ]]; then
	export MAYA_PRESET_PATH=${DEVROOT}/presets:${MAYA_PRESET_PATH}
else
	export MAYA_PRESET_PATH=${DEVROOT}/presets
fi

if [[ -n $MAYA_CUSTOM_TEMPLATE_PATH ]]; then
	export MAYA_CUSTOM_TEMPLATE_PATH=${DEVROOT}/build/$PLAT/templates:${MAYA_CUSTOM_TEMPLATE_PATH}
else
	export MAYA_CUSTOM_TEMPLATE_PATH=${DEVROOT}/build/$PLAT/templates	
fi

export _3DFM_OSL_PATH=${DEVROOT}/build/$PLAT/osl

if [[ -z $_3DFM_USER_FRAGMENT_PATH ]]; then
	export _3DFM_USER_FRAGMENT_PATH=${DEVROOT}/shaderFragments
fi
