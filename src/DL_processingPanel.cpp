#include <stdlib.h>

#include <maya/MSyntax.h>
#include <maya/MArgDatabase.h>
#include <maya/MIntArray.h>

#include "DL_processingPanel.h"

/* This is a 3Delight header. */
#include "delight.h"
#include "3Delight/ProcessingInfo.h"

#include <string.h>

MSyntax
DL_processingPanel::newSyntax()
{
	MSyntax syntax;

	syntax.addFlag("-op", "-openProcessingPanel");

	return syntax;
}

MStatus
DL_processingPanel::doIt(const MArgList& args)
{
	MStatus         status;
	MArgDatabase    argData(syntax(), args, &status);

	if (!status)
		return status;

	if (argData.isFlagSet("-openProcessingPanel"))
	{
		DlShowProcessingPreferences();
	}
	return status;
}
