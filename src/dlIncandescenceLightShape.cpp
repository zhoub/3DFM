/*
  Copyright (c) 2017 The 3Delight Team.
*/

#include "dlIncandescenceLightShape.h"
#include "DL_errors.h"
#include "DL_utils.h"

#include "MU_typeIds.h"

#include <maya/MFnMessageAttribute.h>
#include <maya/MFnNumericAttribute.h>

#define MAKE_INPUT(attr) \
	CHECK_MSTATUS ( attr.setKeyable(true) ); \
	CHECK_MSTATUS ( attr.setStorable(true) ); \
	CHECK_MSTATUS ( attr.setReadable(true) ); \
	CHECK_MSTATUS ( attr.setWritable(true) );

MTypeId dlIncandescenceLightShape::id( MU_typeIds::DL_INCANDESCENCELIGHT );

MObject dlIncandescenceLightShape::s_color;
MObject dlIncandescenceLightShape::s_intensity;
MObject dlIncandescenceLightShape::s_exposure;
MObject dlIncandescenceLightShape::s_geometry;

dlIncandescenceLightShape::dlIncandescenceLightShape()
{
}

dlIncandescenceLightShape::~dlIncandescenceLightShape()
{
}

void dlIncandescenceLightShape::postConstructor()
{
  setExistWithoutInConnections(true);
  setExistWithoutOutConnections(true);
  MFnDependencyNode depFn(thisMObject());
  depFn.setName("incandescencelightShape#");
}

MStatus dlIncandescenceLightShape::compute(
  const MPlug& /*plug*/,
  MDataBlock& /*datablock*/ )
{
  return MS::kUnknownParameter;
}

void* dlIncandescenceLightShape::creator()
{
  return new dlIncandescenceLightShape();
}

MStatus dlIncandescenceLightShape::initialize()
{
  MFnNumericAttribute nAttr;

  s_color = nAttr.createColor( "colorLight", "cl" );
  Utilities::makeInputAttribute(nAttr);
  nAttr.setConnectable(false);
  nAttr.setNiceNameOverride(MString("Color"));

  CHECK_MSTATUS( nAttr.setDefault(1.0f, 1.0f, 1.0f) );

  s_intensity = nAttr.create( "intensity", "i", MFnNumericData::kFloat, 1.0f );
  Utilities::makeInputAttribute(nAttr);
  nAttr.setConnectable(false);
  nAttr.setMin( 0.0 );
  nAttr.setSoftMax( 10.0 );

  s_exposure = nAttr.create( "exposure", "e", MFnNumericData::kFloat, 0.0f );
  Utilities::makeInputAttribute(nAttr);
  nAttr.setConnectable(false);
  nAttr.setMin( -5.0 );
  nAttr.setSoftMax( 10.0 );
	
  // Add the geometry objects attribute
  MStatus status;
  MString attrName("_3delight_geometry");

  MFnMessageAttribute msgAttrFn;
  s_geometry = msgAttrFn.create(attrName, attrName, &status);
  RETURN_ON_FAILED_MSTATUS(status);

  msgAttrFn.setNiceNameOverride(MString("Geometry"));
  msgAttrFn.setArray(true);
  // The attribute must be non-readable for indexMatters(false) to be 
  // effective.
  Utilities::makeInputAttribute(msgAttrFn);
  msgAttrFn.setIndexMatters(false);

  CHECK_MSTATUS( addAttribute( s_color ) );
  CHECK_MSTATUS( addAttribute( s_intensity ) );
  CHECK_MSTATUS( addAttribute( s_exposure ) );
  CHECK_MSTATUS( addAttribute( s_geometry ) );

  return MS::kSuccess;
}
