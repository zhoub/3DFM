#include <maya/MFn.h>

#include "DelegateTable.h"

class NSIExportDelegate;

/**
	\brief A class to contain all the delegates and, importantely, to iterate
	through them in the correct order.

	The idea here is to iterate first through DAG nodes and thenm dpendency
	nodes. It's important because dependecy nodes, such as sets, will point to
	DAG nodes.

	See implementation file for method comments.
*/
class NSIDelegatesContainer
{
public:
	class Iterator;

	NSIDelegatesContainer();
	~NSIDelegatesContainer();

	void Clear( void );

	void AddDelegate( NSIExportDelegate *i_node );
	void AddDagDelegate( NSIExportDelegate *i_node );
	void AddDependencyDelegate( NSIExportDelegate *i_node );

	void ResetIterator( void );
	NSIExportDelegate *Next( void );

	const DelegateTable* DagHashTable() { return &m_dag_nodes; }
	const DelegateTable* DependencyHashTable() { return &m_dependency_nodes; }

	bool ContainsDagNode( const char *i_handle ) const;
	NSIExportDelegate *Find( const char *i_handle ) const;
	bool Delete( const char *i_handle, bool i_recursive = false );
	void DeleteDagDelegates( MFn::Type i_type );

	void Print( void ) const;

private:
	DelegateTable m_dag_nodes;
	DelegateTable m_dependency_nodes;

	DelegateTable::iterator m_dag_iterator;
	DelegateTable::iterator m_dependency_iterator;
};
