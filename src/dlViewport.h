#ifndef __dlViewport_h
#define __dlViewport_h

#include <maya/M3dView.h>
#include <maya/MCallbackIdArray.h>
#include <maya/MDrawContext.h>
#include <maya/MPxNode.h>
#include <maya/MShaderManager.h>
#include <maya/MUiMessage.h>
#include <maya/MViewport2Renderer.h>

#include <map>

#include "DL_mstringSTLutils.h"
#include "NSIExport.h"
#include "RenderPassInterface.h"

class dlViewportRenderOverride;
class NSIViewportExport;

/**
	All operation types for the \sa dlViewportRenderOverride .
*/
enum eRenderOperationType
{
	/**
		3Delight color buffer, it keeps updating if there is new pixels.
	*/
	e_nsi_scene_op = 0,

	/**
		Maya scene buffer, we only draw the object no HUD.
	*/
	e_maya_scene_op,

	/**
		We blend the 2 color buffers generated from the above 2.
	*/
	e_blend_op,

	/**
		Draw the Maya active wireframe on selection.
	*/
	e_maya_ui_op,

	/**
		Draw the Maya HUD text.
	*/
	e_maya_hud_op,

	/**
		Present the final result to the viewport.
	*/
	e_present_op,

	/**
		Place holder for the total number of passes we have.
	*/
	e_number_of_ops
};

/**
	Implements the customized Maya VP2 user operation which updates the
	color texture from internal 3Delight display driver asynchronously.
*/
class dlViewportNsiSceneRenderer : public MHWRender::MUserRenderOperation
{
public:

	static void RegisterDisplayDriver();

	//
	dlViewportNsiSceneRenderer(const MString &name);

	virtual ~dlViewportNsiSceneRenderer();

	/**
		Override the MHWRender::MUserRenderOperation::execute
	*/
	virtual MStatus execute(const MHWRender::MDrawContext &drawContext);

	/**
		Added a new session for a specific model panel.
	*/
	void CreateSession(const MString &panel, NSIViewportExport *);

	/**
		Added a new session for a specific model panel.
	*/
	void UpdateSessionColorTexture(
		const MString &panel,
		unsigned int w,
		unsigned int h);

	/**
		Get the color texture of the model panel.
	*/
	MHWRender::MTexture *GetSessionColorTexture(const MString &panel);

	/**
		Notify a reset for an session, usually it releases the color texture.
	*/
	void SetSessionReset(const MString &panel, bool reset);

	/**
		Delete the session attached to a model panel.
	*/
	void DeleteSession(const MString &panel);
 
	/**
		Release the all resources.
	*/
	void ReleaseResource();

private:

	struct Session
	{
		Session(const MString &panel, NSIViewportExport *exporter) :
			m_panel(panel),
			m_exporter(exporter),
			m_color_texture(NULL),
			m_reset(false)
		{
		}

		MString m_panel;
		NSIViewportExport *m_exporter;
		MHWRender::MTexture *m_color_texture;
		bool m_reset;
	};

	typedef std::map<MString, Session*, delight_mstringLessThan>
		tStringSessionMap;
	tStringSessionMap m_session_map;
};

/**
	Reimplemented the MSceneRender, we only draw the Maya scene without
	active wireframe and handles.
*/
class dlViewportMayaSceneRenderer : public MHWRender::MSceneRender
{
public:

	dlViewportMayaSceneRenderer(
		const MString &name, dlViewportRenderOverride *ro);

	virtual ~dlViewportMayaSceneRenderer();

	//
	virtual MSceneFilterOption renderFilterOverride();

	virtual MHWRender::MClearOperation & clearOperation();

	virtual void postSceneRender(const MHWRender::MDrawContext &context);

	//
	void ReleaseResource();

private:

	dlViewportRenderOverride *m_ro;

	MHWRender::MTexture *m_color_tex;
};

/**
	It blends the two color buffers from the \sa dlViewportNsiSceneRenderer
	and \sa dlViewportMayaSceneRenderer, usually the blend scale should be 1,
	we always want to watch the progressive color buffer from 3Delight.
*/
class dlViewportBlendRenderer : public MHWRender::MQuadRender
{
public:

	dlViewportBlendRenderer(
		const MString &name,
		dlViewportRenderOverride *ro);

	virtual ~dlViewportBlendRenderer();

	//
	virtual const MFloatPoint * viewportRectangleOverride();

	virtual const MHWRender::MShaderInstance * shader();

	virtual MHWRender::MClearOperation & clearOperation();

	//
	void SetSourceColorTexture(MHWRender::MTexture *src_color_tex);

	void SetDestinationColorTexture(MHWRender::MTexture *dst_color_tex);

	void SetScale(float scale);

private:

	MFloatPoint m_view_rectangle;

	MHWRender::MShaderInstance *m_shader_instance;

	MHWRender::MTexture *m_src_color_tex;
	MHWRender::MTextureAssignment m_src_color_ta;

	MHWRender::MTexture *m_dst_color_tex;
	MHWRender::MTextureAssignment m_dst_color_ta;

	float m_scale;
};

/**
	It draws the active wireframe.
*/
class dlViewportMayaUIRenderer : public MHWRender::MSceneRender
{
public:

	dlViewportMayaUIRenderer(const MString &name);

	virtual ~dlViewportMayaUIRenderer();

	//
	virtual MSceneFilterOption renderFilterOverride();

	virtual MObjectTypeExclusions objectTypeExclusions();

	virtual MHWRender::MClearOperation & clearOperation();
};

/**
	It draws Maya HUD elements on viewport.
*/
class dlViewportMayaHudRenderer : public MHWRender::MHUDRender
{
public:

	dlViewportMayaHudRenderer();

	virtual ~dlViewportMayaHudRenderer();
};

/**
	It presents the final result to viewport.
*/
class dlViewportPresentTarget : public MHWRender::MPresentTarget
{
public:

	dlViewportPresentTarget(const MString& name);

	virtual ~dlViewportPresentTarget();
};

/**
	Implementation of our customized Maya VP2 render loop.
	It returns the all passes of \sa eRenderOperationType to Maya, blend the
	3Delight result with Maya VP2 result together, and display the
	interactive result in viewport.
*/
class dlViewportRenderOverride : public MHWRender::MRenderOverride
{
public:

	static const MString s_name;
	static const MString s_ui_name;

	static dlViewportRenderOverride *s_instance;

	//
	dlViewportRenderOverride(const MString &name);

	virtual ~dlViewportRenderOverride();

	//
	virtual MHWRender::DrawAPI supportedDrawAPIs() const;

	//
	virtual bool startOperationIterator();

	virtual MHWRender::MRenderOperation * renderOperation();

	virtual bool nextRenderOperation();

	//
	virtual MString uiName() const;

	virtual MStatus setup(const MString& panel_name);

	virtual MStatus cleanup();

	//
	MHWRender::MRenderOperation * GetRenderOperation(int i);

	/**
		Start the new render at a panel.
	*/
	void StartRender(const MString &panel, NSIViewportExport *);

	/**
		Stop the render at a panel.
	*/
	void StopRender(const MString &panel);

	/**
		Stop the all renders.
	*/
	void StopAllRenders();

	/**
		Get the all name of the modelPanel which are using viewport rendering.
	*/
	bool GetRenderPanels(MStringArray &render_panels) const;

private:

	//
	bool UpdateRenderOperations(const MString &panel);

	//
	typedef std::map<MString, NSIViewportExport*, delight_mstringLessThan>
		tPanelExporterMap;

	tPanelExporterMap m_create_panel_exporter_map;

	//
	MStringArray m_delete_panels;

	//
	int m_curr_op;

	MHWRender::MRenderOperation *m_ops[e_number_of_ops];
	MString m_op_names[e_number_of_ops];

	//
	MCallbackIdArray m_render_override_changed_cbs;

	static void RenderOverrideChangedCallback(
		const MString &panel_name,
		const MString &old_renderer,
		const MString &new_renderer,
		void *client_data);
};

/**
	Empty Maya node as \sa dlRenderSettings to hold the dynamic attributes
	for viewport rendering.
*/
class dlViewportRenderSettings : public MPxNode
{
public:

	dlViewportRenderSettings();
	virtual ~dlViewportRenderSettings();

	virtual MStatus compute(const MPlug& plug, MDataBlock& data);

	virtual void postConstructor();

	static void* creator();
	static MStatus initialize();

	static MTypeId id;
};

/**
	Special render pass used by NSIExportCamera delegate to get the correct
	resolution.
*/
class dlViewportPanelRenderPass : public RenderPassInterfaceForScreen
{
public:

	dlViewportPanelRenderPass(
		const RenderPassInterfaceForScreen &parent_render_pass,
		const MString &panel);

	virtual ~dlViewportPanelRenderPass();

	// @{
	/** Method to output the info only from the model panel. */
	virtual int ResolutionX() const;

	virtual int ResolutionY() const;

	virtual double PixelAspectRatio() const;

	virtual void Crop(float top_left[2], float bottom_right[2]) const;

	virtual int PixelSamples() const;

	virtual int PixelFilterIndex(void) const;
	// @}

	const MString& Panel()const { return m_panel; }

private:

	void GetViewport(unsigned pos[2], unsigned size[2]) const;

	const RenderPassInterfaceForScreen &m_parent_render_pass;

	MString m_panel;
};

/**

	It overrides the default behaviors of \sa RenderPassInterface, the viewport
	rendering has special rendering parameters than standard rendering pass.
*/
class dlViewportRenderPass : public RenderPassInterface
{
public:

	dlViewportRenderPass(MObject& i_renderPassNode, bool i_canApplyOverrides);
	virtual ~dlViewportRenderPass();

	virtual int ResolutionX( void ) const;

	virtual int ResolutionY( void ) const;

	virtual double PixelAspectRatio( void ) const;

	virtual void Crop( float o_top_left[2], float o_bottom_right[2] ) const;

	virtual const char *PixelFilter( void ) const;

	virtual double FilterWidth( void ) const;

	virtual bool DisableMotionBlur() const;

	virtual bool DisableDepthOfField() const;

	virtual std::vector<MObject> cameras() const;

	virtual int ShadingSamples( void ) const;

	virtual int PixelSamples( void ) const;

	virtual int PixelFilterIndex(void) const;

	virtual void layerIndices(MIntArray& o_indices) const;

	virtual bool layerIgnoreMatte( void ) const;

	virtual int layerMatteType( void ) const;

	virtual MString layerDriver(int i_index) const;

	virtual MString layerFilename(int i_index) const;

	virtual void layerVariable(
		int i_index,
		MString& o_variable_source,
		MString& o_layer_type,
		int& o_with_alpha,
		MString& o_nsi_name,
		MString& o_token,
		MString& o_label,
		MString* o_description)const;

	virtual void layerLightCategories(MStringArray& o_categories) const;

	virtual void MatteSets( std::vector<MObject>& o_sets ) const;

	virtual MString layerScalarFormat(int i_index) const;
};

/**
	Derived class of NSIExport, which has a special way about camera.
*/
class NSIViewportExport : public NSIExport
{
public:

	NSIViewportExport(
		MObject &i_pass,
		bool i_canApplyOverrides);

	virtual ~NSIViewportExport();

	/**
		Create the screen+outputlayer+driver for a specific model panel.
	*/
	void CreateRenderOutput(const MString &panel);

	/**
		Delete the screen and its outputlayer+driver.
	*/
	void DeleteRenderOutput(const MString &panel);

	/**
		Update attributes of the screen node without changing its connections.
	*/
	void UpdateScreen(const MString& i_panel);

protected:

	/**
		Does nothing to avoid outputting the render settings' output layers.
	*/
	virtual void ExportImageLayers();

	/**
		\brief A live render callback for renderpass's attribute changes
	*/
	virtual void RenderPassAttributeChanged(
		MNodeMessage::AttributeMessage i_msg,
		MPlug &i_plug,
		MPlug &i_otherPlug );

private:

	void ExportImageLayer(const MString &panel, const MDagPath &camera_path);

	/// Returns the special render pass for the panel.
	dlViewportPanelRenderPass& GetRenderPass(const MString& i_panel)const;

	struct RenderPanelData
	{
		RenderPanelData(
			NSIViewportExport *exporter,
			RenderPassInterfaceForScreen &parent_render_pass,
			const MString &panel);

		~RenderPanelData();

		NSIViewportExport* m_exporter;
		std::unique_ptr<dlViewportPanelRenderPass> m_render_pass;

		MCallbackIdArray m_callback_ids;

		const MString& Name()const { return m_render_pass->Panel(); }

		// Called when a panel get connected to a different camera
		static void PanelCameraChangedCallback(
			const MString &panel,
			MObject &node,
			void *data);
		/*
			Called when a camera attribute changes.
			Could be called before or after NSIExport::AttributeChangedCallback,
			but the call order shouldn't matter here.
		*/
		static void CameraAttributeChangedCallback(
			MNodeMessage::AttributeMessage i_msg,
			MPlug& i_plug,
			MPlug& i_otherPlug,
			void* i_data);
	};

	typedef std::map<MString, RenderPanelData*, delight_mstringLessThan>
		tStringRenderPanelDataMap;

	tStringRenderPanelDataMap m_render_panel_data_map;
};

#endif
