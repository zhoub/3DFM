#include <maya/MGlobal.h>

#define _3DFM_ERR_PREFIX( cmd_str ) MString("3DFM - ") + MString( cmd_str ) + MString(": ")

#ifdef DEBUG
#define RETURN_ON_FAILED_MSTATUS( _status ) \
CHECK_MSTATUS_AND_RETURN_IT( ( _status ) )
#else
#define RETURN_ON_FAILED_MSTATUS( _status ) \
{ \
	if( ( _status ) != MStatus::kSuccess) \
	{ \
		return ( _status ); \
	} \
}
#endif
