#ifndef __ShaderDataCache_h
#define __ShaderDataCache_h

#include <maya/MFnDependencyNode.h>

#include <3Delight/ShaderQuery.h>

#include "NSIExportUtilities.h"

#include <map>
#include <memory>
#include <mutex>
#include <string>
#include <unordered_map>

/**
	The cached data per shader parameter.

	m_name is expected to receive and own a copy of the param name string. It
	will be freed in the destructor.
*/
struct ParamData
{
public:

	enum Flags
	{
		Flag_ConnectUVCoord = 1,
		Flag_TextureFile = 2,
		Flag_SkipInit = 4
	};

	std::string m_name;
	DlShaderInfo::TypeDesc m_type;
	int m_flags{0};
};

/**
	The cached parameter data per shader.
*/
struct ShaderData
{
	ShaderData();
	~ShaderData();

	/** \brief cached the OSL data related to the specified shading node. */
	void CacheShaderData( MFnDependencyNode& i_shader );

	/**
		\brief
		Returns the OSL parameter data that corresponds to the specified
		maya attribute. A null pointer is returned when no OSL parameter
		corresponds to that attribute.
	*/
	const ParamData* GetParameterData(
		const MPlug& i_plug,
		NSIExportUtilities::IoType i_type ) const;

	/**
		\brief Same as above but takes a const char * instead of MPlug.
	*/
	const ParamData* GetParameterData(
		const char *i_parameter,
		NSIExportUtilities::IoType i_type ) const;

	/**
		\brief Returns a table key formed from the specified attribute name.
	*/
	MString AttributeNameToTableKey(
		const MString& i_attributeName,
		NSIExportUtilities::IoType i_type ) const;

	/**
		\brief Returns a table key formed from the specified Maya attribute (plug).
	*/
	MString AttributeToTableKey(
		const MPlug& i_plug,
		NSIExportUtilities::IoType i_type ) const;

	/// The cached OSL shader full path name
	MString m_fullPathName;

	/// Table of cached parameter data
	std::unordered_map<std::string, ParamData> m_paramDataTable;

private:
	/**
		\brief Returns the OSL parameter data for the specified table key.
	*/
	const ParamData* GetParameterData( const MString& i_tableKey ) const;
};

/**
	The cached shader data.
*/
class ShaderDataCache
{
public:
	~ShaderDataCache();

	/** \brief Returns the singleton instance, creating one if necessary */
	static ShaderDataCache* Instance();

	/** \brief Creates the singleton instance */
	static void CreateInstance();

	/** \brief Deletes the singleton instance */
	static void DeleteInstance();

	/**
		\brief Finds and returns the cached ShaderData of a shading node. Null is
		returned if the shading node was not in the cache.
	*/
	const ShaderData* Find( MObject i_shader );
	/**
		\brief Cache the OSL data of the specified shading node, and return it.
	*/
	const ShaderData* Add( MObject i_shader );

	/** \brief Clear all cached data. */
	void Clear();

private:
	ShaderDataCache();
	ShaderDataCache(ShaderDataCache&);

	static ShaderDataCache* m_instance;
	static std::mutex m_mutex;

	std::map<unsigned, std::shared_ptr<ShaderData>> m_shaderTable;

};

#endif
