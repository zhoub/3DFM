#include "NSIExportStandIn.h"

#include <maya/MPlug.h>
#include <maya/MFnDependencyNode.h>

#include <nsi.hpp>


NSIExportStandIn::NSIExportStandIn(
	MObject i_object,
	const NSIExportDelegate::Context& i_context)
	:	NSIExportDelegate(i_object, i_context)
{
}

NSIExportStandIn::~NSIExportStandIn()
{
}

void NSIExportStandIn::Create()
{
	NSI::Context nsi(m_nsi);
	nsi.Create(m_nsi_handle, "procedural");
}

void NSIExportStandIn::SetAttributes()
{
	MFnDependencyNode depFn(Object());
	MPlug plug = depFn.findPlug("filename", true);
	MString filename;
	plug.getValue(filename);

	NSI::Context nsi(m_nsi);
	nsi.SetAttribute(
		m_nsi_handle,
		(
			NSI::CStringPArg("type", "apistream"),
			NSI::CStringPArg("filename", filename.asChar())
		) );
}

void NSIExportStandIn::SetAttributesAtTime(double, bool)
{
	// Nothing to do
}
