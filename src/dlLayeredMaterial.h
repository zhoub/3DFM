#ifndef __dlLayeredMaterial_H__
#define __dlLayeredMaterial_H__

#include <maya/MPxNode.h>

class dlLayeredMaterial : public MPxNode
{
public:
	static void* creator();
	static MStatus initialize();

	virtual void postConstructor();
	virtual MStatus compute( const MPlug&, MDataBlock& );

private:
	static MObject s_color;
	static MObject s_outColor;
};

#endif

