#ifndef __NSIExportCamera_h
#define __NSIExportCamera_h

#include "NSIExportDelegate.h"
#include "dlVec.h"

#include <nsi.hpp>

#if MAYA_API_VERSION >= 201800
#include <maya/MApiNamespace.h>
#else
class MFloatArray;
class MFnCamera;
#endif
class RenderPassInterfaceForCamera;

/**
	\sa NSIExportDelegate
*/
class NSIExportCamera : public NSIExportDelegate
{
public:
	NSIExportCamera(
		const RenderPassInterfaceForCamera &,
		MFnDagNode &,
		const NSIExportDelegate::Context& i_context );

	/*
		For multiple view rendering we have to assign
		the unique handle for this camera shape.
	*/
	NSIExportCamera(
		const RenderPassInterfaceForCamera &,
		MFnDagNode &,
		const NSIExportDelegate::Context& i_context,
		const MString & );

	virtual void Create();

	virtual void SetAttributes( void );
	virtual void SetAttributesAtTime( double, bool );

	static void OverrideScreen(
		const MFnCamera &i_camera,
		dl::V2f &io_screen_size,
		dl::V2f &io_screen_center);

private:
	/**
		\brief Sets depth-of-field NSI parameters.

		\param MFnCamera
			The Maya camera
		\param NSI::ArgumentList
			The output argument list.
	*/
	void SetDepthOfField( const MFnCamera &, NSI::ArgumentList &) const;

	const char *CameraNodeType() const;

private:
	const RenderPassInterfaceForCamera &m_render_pass;
	MString m_lens_shader_handle;
};
#endif
