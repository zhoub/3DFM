/*
	Copyright (c) The 3Delight Team.
	Copyright (c) soho vfx inc.
*/

#include "VPSurfaceShaderOverride.h"

#include <maya/MFnDependencyNode.h>
#include <maya/MShaderManager.h>

#include "VPFragmentRegistry.h"

MHWRender::MPxSurfaceShadingNodeOverride* VPSurfaceShaderOverride::creator(
	const MObject& i_obj )
{
	return new VPSurfaceShaderOverride( i_obj );
}

VPSurfaceShaderOverride::VPSurfaceShaderOverride( const MObject& i_obj ):
	MPxSurfaceShadingNodeOverride( i_obj )
{
#if MAYA_API_VERSION >= 201700
	MString typeName = MFnDependencyNode( i_obj ).typeName();
	MStringArray fragments;
	VPFragmentRegistry::Instance()->Add( typeName, fragments );

	if( fragments.length() > 0 )
	{
		m_fragmentName = fragments[0];
	}
	else
#endif
	{
		// Fallback on the built-in phong shader if no fragment is found
		m_fragmentName = "mayaPhongSurface";
	}
}

VPSurfaceShaderOverride::~VPSurfaceShaderOverride()
{
}

MHWRender::DrawAPI VPSurfaceShaderOverride::supportedDrawAPIs() const
{
	return MHWRender::kOpenGL |
		MHWRender::kDirectX11 |
		MHWRender::kOpenGLCoreProfile;
}

MString VPSurfaceShaderOverride::fragmentName() const
{
	return m_fragmentName;
}

MString VPSurfaceShaderOverride::primaryColorParameter() const
{
	// Use the color parameter from the phong fragment as the primary color
	return "color";
}

MString VPSurfaceShaderOverride::transparencyParameter() const
{
	if( m_fragmentName == "mayaPhongSurface" )
	{
		return "transparency";
	}

	// Disable transparency until we properly manage "opacity".
	return "";
}

MString VPSurfaceShaderOverride::bumpAttribute() const
{
	// Use the "normalCamera" attribute to recognize bump connections
	return "normalCamera";
}

void VPSurfaceShaderOverride::updateShader(
	MHWRender::MShaderInstance& m_shader,
	const MHWRender::MAttributeParameterMappingList& m_mappings )
{
	// Avoid ugly specular highlights with the mayaPhongSurface shader.
	if( m_fragmentName == "mayaPhongSurface" )
	{
		m_shader.setParameter( "cosinePower", 50.0f );
	}
}
