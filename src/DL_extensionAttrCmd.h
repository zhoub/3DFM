/*
  Copyright (c) The 3Delight Team.
  Copyright (c) soho vfx inc.
*/

#ifndef __DL_EXTENSIONATTRCMD_H__
#define __DL_EXTENSIONATTRCMD_H__

#include <maya/MPxCommand.h>
#include <maya/MSyntax.h>
#include <maya/MArgList.h>

#define DL_EXTENSIONATTR_CMD_STR "delightExtensionAttr"

//
// DL_extensionAttrCmd
// A command that provides access to DL_extensionAttrRegistry operations.
//
class DL_extensionAttrCmd : public MPxCommand
{
public:
	virtual MStatus doIt(const MArgList& args);
	virtual bool isUndoable() const;

	static void* creator();
	static MSyntax newSyntax();
};

#endif
