/*
  Copyright (c) The 3Delight Team.
  Copyright (c) soho vfx inc.
*/

#include "ndspy.h"
#include <stdio.h>
#include <string.h>

#include "maya_material_viewer.h"
#include "MaterialViewer.h"

#if MAYA_API_VERSION >= 201600

PtDspyError MatViewerDspyImageOpen(
	PtDspyImageHandle *i_phImage,
	const char *i_drivername,
	const char * i_filename,
	int i_width, int i_height,
	int i_paramCount,
	const UserParameter *i_parameters,
	int i_numFormats,
	PtDspyDevFormat i_formats[],
	PtFlagStuff * /*flagstuff*/ )
{
	if( !i_phImage )
	{
		return PkDspyErrorBadParams;
	}

	void *mvobject = nullptr;
	for( int i = 0; i < i_paramCount; ++i )
	{
		const UserParameter &p = i_parameters[i];
		if( p.valueType == 'p' && p.valueCount == 1 &&
		    0 == strcmp(p.name, "mvobject") )
		{
			mvobject = *(void**)p.value;
		}
	}
	if( !mvobject )
	{
		return PkDspyErrorBadParams;
	}

	// We need floating point data
	for( int i=0; i<i_numFormats; i++ )
		i_formats[i].type = PkDspyFloat32;

	*i_phImage = mvobject;

	int numPasses = 1;
	PtDspyError result =
		DspyFindIntInParamList("passes", &numPasses, i_paramCount, i_parameters);

	// Pass the numnber of passes to the renderer so it can compute the progress
	// correctly/
	MaterialViewer* renderer = (MaterialViewer*)*i_phImage;
	renderer->setNumPasses(numPasses);

	return PkDspyErrorNone;
}

PtDspyError MatViewerDspyImageQuery(
	PtDspyImageHandle i_hImage,
	PtDspyQueryType i_type,
	int i_datalen,
	void *i_data )
{
	if( !i_data )
	{
		return PkDspyErrorBadParams;
	}

	switch(i_type)
	{
		case PkProgressiveQuery:
		{
			if(i_datalen < int(sizeof(PtDspyProgressiveInfo)))
			{
				return PkDspyErrorBadParams;
			}

			((PtDspyProgressiveInfo*)i_data)->acceptProgressive = 1;
			break;
		}

		default : return PkDspyErrorUnsupported;
	}

	return PkDspyErrorNone;
}

PtDspyError MatViewerDspyImageData(
	PtDspyImageHandle i_hImage,
	int i_xmin, int i_xmax_plusone,
	int i_ymin, int i_ymax_plusone,
	int i_entrySize,
	const unsigned char* i_cdata )
{
	// Maya expects an image that is flipped in Y, so flip the bucket.
	//
	int width = i_xmax_plusone - i_xmin;
	int height = i_ymax_plusone - i_ymin;

	// MaterialViewer expects a 32-bit float RGBA image.
	float* flippedBucket = (float*)malloc(width * height * 4 * sizeof(float));
	float* destRow = flippedBucket + width * (height - 1) * 4;

	// This assumes that we are dealing with a 32-bit float image.
	bool hasAlpha = (i_entrySize == 4 * sizeof(float)) ? true : false;

	for(int j = 0; j < height; j++)
	{
		float* dest = destRow;
		destRow -= width * 4;

		for(int i =0; i < width; i++, i_cdata += i_entrySize)
		{
			float* origBucketRow = (float*)i_cdata;
			*dest++ = origBucketRow[0];
			*dest++ = origBucketRow[1];
			*dest++ = origBucketRow[2];
			*dest++ = hasAlpha ? origBucketRow[3] : 1.0f;
		}
	}

	// send it to MaterialViewer
	//
	MaterialViewer* renderer = (MaterialViewer*)i_hImage;
	renderer->receiveBucket(
		i_xmin, i_xmax_plusone, 
		i_ymin, i_ymax_plusone, 
		i_entrySize, 
		flippedBucket);

	free(flippedBucket);
	flippedBucket = NULL;

	return PkDspyErrorNone;
}

PtDspyError MatViewerDspyImageClose( PtDspyImageHandle i_hImage )
{
	return PkDspyErrorNone;
}

#endif

void RegisterMayaMaterialViewer()
{
#if MAYA_API_VERSION >= 201600
	PtDspyDriverFunctionTable table;
	memset( &table, 0, sizeof(table) );

	table.Version = k_PtDriverCurrentVersion;
	table.pOpen = &MatViewerDspyImageOpen;
	table.pQuery = &MatViewerDspyImageQuery;
	table.pWrite = &MatViewerDspyImageData;
	table.pClose = &MatViewerDspyImageClose;

	DspyRegisterDriverTable( "maya_material_viewer", &table );
#endif
}


