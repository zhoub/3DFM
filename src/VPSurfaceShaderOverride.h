/*
	Copyright (c) The 3Delight Team.
	Copyright (c) soho vfx inc.
*/

#ifndef __VPSurfaceShaderOverride__
#define __VPSurfaceShaderOverride__

#include <maya/MPxSurfaceShadingNodeOverride.h>

class VPSurfaceShaderOverride : public MHWRender::MPxSurfaceShadingNodeOverride
{
public:
	static MHWRender::MPxSurfaceShadingNodeOverride* creator(
		const MObject& i_obj );

	virtual ~VPSurfaceShaderOverride();

	virtual MHWRender::DrawAPI supportedDrawAPIs() const;
	virtual MString fragmentName() const;

	virtual MString primaryColorParameter() const;
	virtual MString transparencyParameter() const;
	virtual MString bumpAttribute() const;

	void updateShader(
		MHWRender::MShaderInstance& m_shader,
		const MHWRender::MAttributeParameterMappingList& m_mappings );

private:
	VPSurfaceShaderOverride( const MObject& i_obj );

	MString m_fragmentName;
};

#endif
