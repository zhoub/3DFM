#ifndef __DL_PROCESSING_PANEL_H__
#define __DL_PROCESSING_PANEL_H__

#include <maya/MPxCommand.h>

class DL_processingPanel : public MPxCommand
{
public:
	virtual MStatus doIt(const MArgList& args);

	virtual bool isUndoable() const { return false; }

	static void* creator() { return new DL_processingPanel; }

	static MSyntax newSyntax();
private:
};

#endif
