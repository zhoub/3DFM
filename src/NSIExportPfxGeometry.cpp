#include "NSIExportPfxGeometry.h"
#include "DL_utils.h"
#include "OSLUtils.h"
#include "dlVec.h"

#include <nsi.hpp>

#include <maya/MGlobal.h>
#include <maya/MNodeMessage.h>
#include <maya/MFnTransform.h>
#include <maya/MFnDependencyNode.h>
#include <maya/MFnPfxGeometry.h>
#include <maya/MRenderLine.h>
#include <maya/MRenderLineArray.h>
#include <maya/MVectorArray.h>
#include <maya/MPointArray.h>
#include <maya/MPlugArray.h>
#include <maya/MPlug.h>
#include <maya/MRenderLine.h>
#include <maya/MDagPath.h>
#include <maya/MFnMesh.h>
#include <maya/MMatrix.h>

#include <algorithm>
#include <vector>
#include <assert.h>

/**
	\brief Only noteable thing is that we connect a basic shader for pfx.
*/
void NSIExportPfxGeometry::Create()
{
	MString attributes = NSIAttributesHandle( Object(), m_live );
	MString shader = MString(Handle()) + "|shader";

	NSI::Context nsi(m_nsi);
	nsi.Create( Handle(), "transform" );
	nsi.Create( attributes.asChar(), "attributes" );
	nsi.Create( shader.asChar(), "shader");
	nsi.SetAttribute( shader.asChar(), NSI::CStringPArg("shaderfilename", OSLUtils::GetFullpathname("pfx").asChar()) );
	nsi.Connect( shader.asChar(), "", attributes.asChar(), "surfaceshader" );
	nsi.Connect( attributes.asChar(), "", Handle(), "geometryattributes" );
}

/**
	All transform attributes are time-dependent.
*/
void NSIExportPfxGeometry::SetAttributesAtTime(
	double i_time, bool i_no_motion )
{
	assert( IsValid() );
	assert( Object().hasFn(MFn::kPfxGeometry) );
}

/**
*/
void NSIExportPfxGeometry::SetAttributes( void )
{
	assert( IsValid() );
	assert( Object().hasFn(MFn::kPfxGeometry) );

	bool cubic = IsCubic();

	MStatus status;
	MFnPfxGeometry pfx_fn(Object());

	MRenderLineArray main_lines, leaf_lines, flower_lines;

	pfx_fn.getLineData(main_lines,
		leaf_lines,
		flower_lines,
		true, // doLines
		false, // doTwist
		true, // doWidth
		false, // doFlatness
		false, // doParameter
		true, // doColor
		false, // doIncandescence
		true, // doTransparency
		false ); // objectSpace

	MRenderLine curr_line;
	MRenderLineArray *render_lines[3] =
		{ &main_lines, &flower_lines, &leaf_lines };

	std::vector<MRenderLine> rendered_lines;
	for( int line_set = 0; line_set < 3; line_set++ )
	{
		for(
			int curr_line_num = 0;
			curr_line_num < render_lines[line_set]->length();
			curr_line_num++ )
		{
			curr_line = render_lines[line_set]->renderLine(
				curr_line_num, &status );

			if( status != MS::kSuccess )
				continue;

			MVectorArray curr_line_vertices = curr_line.getLine();

			if( curr_line_vertices.length() < 4 )
			{
				/* FIXME: NSI should allow this? */
				continue;
			}

			PipelineOnePfxLine( &curr_line, cubic );
		}
	}

	/* This will expulse last chunk if we still have some data */
	PipelineOnePfxLine( 0x0, cubic );

	/*
		The reset of the chunk number is important so that if we re-do the
		operation again we have the same node handles so that we don't
		duplicate geometry to NSI!
	*/
	m_total_chunks = m_chunk_number;
	m_chunk_number = 0;

	/*
		The reset of this counter signals that we went through the entire
		set attribute process.
	*/
	m_last_nvertices = -1;

    main_lines.deleteArray();
    leaf_lines.deleteArray();
    flower_lines.deleteArray();

	m_exportedAsCubic = cubic;
}

/**
	\brief Exports one pfx line as an bspline with an interpolated end points.

	It groups consecutive curves with the same number of vertices together in a
	single primitive, but closes it and starts a new primitive when it
	encounters a curve with a different number of vertices.
*/
void NSIExportPfxGeometry::PipelineOnePfxLine(
	const MRenderLine *i_line,
	bool i_cubic )
{
	MVectorArray P;
	MDoubleArray width;
	MVectorArray Cs;
	MVectorArray Os;

	if( i_line )
	{
		P = i_line->getLine();
		width = i_line->getWidth();
		Cs = i_line->getColor();
		Os = i_line->getTransparency();

		if( P.length() != width.length() || width.length() != Cs.length() ||
			Cs.length() != Os.length() )
		{
			assert( !"Invalid Pfx Geometry" );
			return;
		}

		if( P.length() == 0 )
			return;
	}

	bool export_last_chunk = i_line==0x0 ||
		(m_last_nvertices!=-1 && P.length()!=m_last_nvertices);

	/*
		Number of extra vertices at each end of the curve : 0 for linear curves
		and 1 for cubic curves.
	*/
	int extra = i_cubic;

	if( export_last_chunk )
	{
		MString line_handle = MString( Handle() ) + "_chunk_" + m_chunk_number;
		NSI::Context nsi( m_nsi );

		// Delete the node if it was previously created with a different type.
		if(m_exportedAsCubic != i_cubic)
		{
			nsi.Delete( line_handle.asChar() );
		}

		nsi.Create(
			line_handle.asChar(),
			i_cubic ? "cubiccurves" : "linearcurves" );

		NSI::ArgumentList args;
		args.Add( NSI::Argument::New( "P" )
				->SetType( NSITypePoint )
				->SetCount( m_P.size() )
				->SetValuePointer( &m_P[0] ) );
		args.Add( NSI::Argument::New( "width" )
				->SetType( NSITypeFloat )
				->SetCount( m_width.size() )
				->SetValuePointer( &m_width[0] ) );
		args.Add( NSI::Argument::New( "Cs" )
				->SetType( NSITypeColor )
				->SetCount( m_Cs.size() )
				->SetValuePointer( &m_Cs[0] ) );
		args.Add( NSI::Argument::New( "Ts" )
				->SetType( NSITypeColor )
				->SetCount( m_Os.size() )
				->SetValuePointer( &m_Os[0] ) );
		args.Add( new NSI::IntegerArg( "nvertices", m_last_nvertices + 2*extra ) );
		if(i_cubic)
		{
			args.Add( new NSI::CStringPArg( "basis", "b-spline") );
		}

		nsi.SetAttribute( line_handle.asChar(), args );
		nsi.Connect( line_handle.asChar(), "", Handle(), "objects" );

		m_chunk_number ++;

		m_P.clear();
		m_width.clear();
		m_Os.clear();
		m_Cs.clear();

		if( i_line == 0x0 )
			return;
	}

	m_last_nvertices = P.length();

	// Output all attributes, duplicating the first and last point if necessary
	for( int i = -extra; i < int(P.length())+extra; i++ )
	{
		int index = std::max(0, std::min<int>(i, P.length()-1));

		m_P.push_back( dl::V3f(P[index][0],P[index][1],P[index][2]) );
		m_Cs.push_back( dl::V3f(Cs[index][0],Cs[index][1],Cs[index][2]) );
		m_Os.push_back( dl::V3f(Os[index][0],Os[index][1],Os[index][2]) );
		m_width.push_back( width[index] );
	}
}

/**
	\brief Process node dirty events. We do that until the node becomes stable.
*/
void NSIExportPfxGeometry::NodeDirtyCB( MObject &i_node, void *i_delegate )
{
	NSIExportPfxGeometry *delegate = (NSIExportPfxGeometry *) i_delegate;

	int total_chunks = delegate->m_total_chunks;

	/* Can happen at node creation. */
	delegate->SetAttributes();
	NSI::Context nsi( delegate->NSIContext() );
	nsi.RenderControl( NSI::CStringPArg( "action", "synchronize") );

	if( total_chunks == delegate->m_total_chunks )
	{
		delegate->m_stabilized ++;
	}

	if( delegate->m_stabilized == 5 )
	{
		/* Node creation finished, we don't need to monitor it anymore */
		delegate->RemoveCallbacks();
	}
}


/**
	\brief Register a dirty callback for this node to monitor it's creation.

	We keep the callback until the node did output some valid NSI chunks.
*/
bool NSIExportPfxGeometry::RegisterCallbacks( void )
{
	MObject &object = (MObject &)Object();
	MCallbackId id = MNodeMessage::addNodeDirtyCallback(
		object, NodeDirtyCB, (void *)this );
	AddCallbackId( id );

	return true;
}

void NSIExportPfxGeometry::MemberOfSet(MObject& i_set)
{
	m_set = MObjectHandle(i_set);
}

bool NSIExportPfxGeometry::IsCubic()const
{
/* 
Pfx node currently lacks the smooth curve option.
Probably linear curves would only be desired for perfomance reasons,
defaulting to smooth ones as its the most useful case.
*/
return true;
}
