#ifndef __SwatchRender_h
#define __SwatchRender_h

#if MAYA_API_VERSION >= 201500

#include <maya/MSwatchRenderBase.h>

#include "nsi.hpp"

/**
	\brief Our implementation of Maya's 2015 swatch renderer.

	Flow of events related to swatches with Maya 2015:
	Maya creates a swatch render object when a rendering is needed. Once the
	  rendering has completed, it destroys it.
	- Because renderParallel() returns 1, Maya will call doIteration() exactly
	  once.  If it returns true, then Maya destroys the swatch render object.
	  So we return true only if the render could not be started.
	- The swatch render object contains an image; Maya specifies the expected
	  swatch image resolution upon construction. It may successively create
	  swatch render objects of increasing resolution (may happen when
	  populating the Hypershade, especially if the swatches in there are
	  large).
	- Editing a shader attribute will trigger another swatch render. Dragging a
	  slider will usually create two swatch objects; a first one for "low
	  quality rendering" (renderQuality() returns 0) and later one for the
	  final attribute value (renderQuality() returns 1). Drag operations seem
	  to create two swatch render objects concurrently, but the doIteration of
	  the second one appear to be called only once the first object is
	  destroyed.
	- Changing an attribute will:
  		1 - create a new swatch render object;
		2 - trigger the attribute change / node dirty plug callbacks (in
		    delightNodeWatch for instance);
		3 - call doIteration() on the swatch render object created in #1.
	- Maya may destroy a swatch render object prematurely in some cases; for
	  instance, it will try to cancel a ongoing render for a swatch that is not
	  visible anymore in the Hypershade (because the suer scrolled it out of
	  view).
*/

class SwatchRender : public MSwatchRenderBase
{
	SwatchRender(const SwatchRender&);
	SwatchRender& operator =(const SwatchRender&);

public:
	SwatchRender(MObject swatchObj, MObject renderObj, int res);
	virtual ~SwatchRender();

	static MSwatchRenderBase* creator(
		MObject swatchObj, MObject renderObj, int res);

	virtual bool doIteration();
	virtual bool renderParallel();
	virtual void cancelParallelRendering();

private:
	void ExportScene( void );
	void OutputShadingNetwork( const char *i_shading_network_head ) const;
	void OutputDisplayChain( const char *i_camera );
	bool startRender();

	static void Stopped(void*, NSIContext_t, int );


private:
	MString m_color_space;
	NSIContext_t m_nsi;
};

#endif  // MAYA_API_VERSION
#endif

