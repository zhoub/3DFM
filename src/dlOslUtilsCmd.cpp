#include "dlOslUtilsCmd.h"

#include <maya/MArgParser.h>
#include <maya/MSelectionList.h>
#include <maya/MSyntax.h>

#include "DL_errors.h"
#include "DL_autoLoadOSL.h"
#include "NSIExportUtilities.h"
#include "ShaderDataCache.h"

MSyntax dlOslUtilsCmd::newSyntax()
{
	MSyntax syntax;

	syntax.addFlag( "-rl", "-reload", MSyntax::kString );
	/* This could use kSelectionItem but there's not much point besides getting
	   errors. I think returning nothing is better. */
	syntax.addFlag( "-sfn", "-shaderfilename", MSyntax::kString );

	syntax.enableQuery( false );
	syntax.enableEdit( false );

	return syntax;
}

MStatus dlOslUtilsCmd::doIt( const MArgList& i_argList )
{
	MStatus status;
	MArgParser argParser( syntax(), i_argList, &status );

	if( status != MStatus::kSuccess )
	{
		return status;
	}

	bool isReloadSet = argParser.isFlagSet( "-reload" );
	if( isReloadSet )
	{
		MString shader;
		argParser.getFlagArgument( "-reload", 0, shader );

		DL_OSLShadingNode::ReloadShader(shader);
	}

	if( argParser.isFlagSet("-shaderfilename") )
	{
		MString shader;
		argParser.getFlagArgument("-shaderfilename", 0, shader);
		MSelectionList sel;
		sel.add(shader);
		if( !sel.isEmpty() )
		{
			MObject shader_node;
			sel.getDependNode(0, shader_node);
			const ShaderData* shaderData =
				NSIExportUtilities::GetShaderData(shader_node);
			if( shaderData )
			{
				setResult(shaderData->m_fullPathName);
			}
		}
	}

	return MStatus::kSuccess;
}
