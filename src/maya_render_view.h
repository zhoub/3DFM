#ifndef __maya_render_view_h
#define __maya_render_view_h

/* Register the maya_render_view display with 3Delight. */
void RegisterMayaRenderView();
void MayaRenderViewTimerBucketCallback( float, float, void* );
#endif

