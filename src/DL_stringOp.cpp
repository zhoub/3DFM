#include <maya/MArgDatabase.h>
#include <maya/MSyntax.h>
#include <maya/MArgList.h>

#include <string.h>

#include "DL_stringOp.h"

MSyntax
DL_stringOp::newSyntax()
{
  MSyntax syntax;

  syntax.addFlag(
    "-sas", 
    "-substituteAllString", 
    MSyntax::kString, 
    MSyntax::kString,
    MSyntax::kString);

  return syntax;
}

inline static
bool target_match( const char *target, const char *source )
{
  while( true )
  {
    if( *target != *source )
      return false;

    ++target;
    ++source;

    if( *target == '\0' )
      return true;
  }
}

MStatus
DL_stringOp::doIt(const MArgList& args)
{
  MString string_arg1, string_arg2, string_arg3;

  MStatus status;
  MArgDatabase argData(syntax(), args, &status);

  if( !status )
    return status;

  if( argData.isFlagSet( "-sas" ) )
  {
    argData.getFlagArgument( "-sas", 0, string_arg1 );
    argData.getFlagArgument( "-sas", 1, string_arg2 );
    argData.getFlagArgument( "-sas", 2, string_arg3 );

    const char* arg1_cstring = string_arg1.asChar();
    const char* target_cstring = string_arg2.asChar();
    unsigned target_len = strlen( target_cstring );
    
    unsigned target_count = 0;

    for(const char* source = arg1_cstring; *source != '\0'; source++)
    {
      if( target_match( target_cstring, source ) )
      {
        source += target_len - 1;
        target_count++;
      }
    }
    
    if(target_count == 0)
    {
      setResult(string_arg1);
      return MS::kSuccess;
    }
    
    unsigned result_length =
      int(string_arg1.length()) +
      int(target_count) * (int(string_arg3.length()) - int(target_len)) + 1;
    char* result_string = new char[result_length];
    
    const char* arg3_cstring = string_arg3.asChar();
    unsigned result_index = 0;
    
    for(const char* source = arg1_cstring; *source != '\0' && result_index < result_length; source++)
    {
      if( target_match( target_cstring, source ) )
      {
        for(const char* repl = arg3_cstring; *repl != '\0' && result_index < result_length; repl++)
        {
          result_string[result_index++] = *repl;
        }
        source += target_len - 1;
      }
      else
        result_string[result_index++] = *source;
    }
    
    result_string[result_index] = '\0';
    setResult(MString(result_string));
    
    delete[] result_string;
  }

  return status;
}
