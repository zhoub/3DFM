#ifndef __DL_PACKAGESPECIFIC_H__
#define __DL_PACKAGESPECIFIC_H__

#include <maya/MString.h>

#define _3DFM_PACKAGE_NAME "3DelightForMaya"
#define _3DELIGHT_PACKAGE_NAME "3Delight"

#if MAYA_API_VERSION >= 201800
#include <maya/MApiNamespace.h>
#else
class MFnPlugin;
#endif

class DL_packageSpecific
{
public:
  static const MString getPackageDescriptionString();
	static bool isPackage3dfm();
	static bool isPackage3delight();
	
	static void set3dfmRibOption();
	static MStatus registerNSIArchiveTranslator(MFnPlugin& i_plugin);
	static MStatus deregisterNSIArchiveTranslator(MFnPlugin& i_plugin);
};

inline bool
DL_packageSpecific::isPackage3dfm()
{
	return getPackageDescriptionString() == 
		MString(_3DFM_PACKAGE_NAME);
}

inline bool
DL_packageSpecific::isPackage3delight()
{
	return getPackageDescriptionString() == 
		MString(_3DELIGHT_PACKAGE_NAME);
}

#endif
