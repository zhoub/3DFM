#include <maya/MSyntax.h>
#include <maya/MArgDatabase.h>

#include "DL_hash.h"

MSyntax
DL_hash::newSyntax()
{
  MSyntax syntax;

  syntax.addFlag("-hs", "-hashstring", MSyntax::kString);

  return syntax;
}

MStatus
DL_hash::doIt(const MArgList& args)
{
  MStatus         status;
  MArgDatabase    argData(syntax(), args, &status);

  if (status == MS::kSuccess)
  {
    if( argData.isFlagSet("-hs"))
    {
      MString string_to_hash;
      argData.getFlagArgument("-hs", 0, string_to_hash);

      const char *str = string_to_hash.asChar();
      
      // cheap X31 hash
      unsigned h = 0;
      while( *str )
      {
        h = ( h << 5 ) - h + *str++;
      }

      setResult( int( h & 0x7fffffff ) );
    }
  }

  return status;
}
