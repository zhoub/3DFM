#ifndef __DL_STRINGARRAYCMD_H__
#define __DL_STRINGARRAYCMD_H__

#include <maya/MDagPath.h>
#include <maya/MObject.h>
#include <maya/MPxCommand.h>

class DL_stringArrayCmd : public MPxCommand
{

public:
  virtual MStatus       doIt(const MArgList& args);

  virtual bool          isUndoable() const
                        { return false; }

  static void*          creator()
                        { return new DL_stringArrayCmd; }

  static MSyntax        newSyntax();

private:
};

#endif
