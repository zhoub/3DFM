#include "ShaderDataCache.h"

#include "OSLUtils.h"

#include <maya/MFnAttribute.h>
#include <maya/MObject.h>
#include <maya/MPlug.h>
#include <maya/MGlobal.h>


ShaderDataCache* ShaderDataCache::m_instance = 0x0;
std::mutex ShaderDataCache::m_mutex;

ShaderData::ShaderData()
{
}

ShaderData::~ShaderData()
{
}

void ShaderData::CacheShaderData( MFnDependencyNode& i_shader )
{
	/*
		Check a possible shaderFilename string attribute and use its value to
		search for an OSL shader.
	*/
	MPlug shaderFilenamePlug;
	MStatus status;
	shaderFilenamePlug = i_shader.findPlug( "shaderFilename", true, &status );

	MString shaderFilename;
	MString shaderPath;
	DlShaderInfo *info;

	if( status == MStatus::kSuccess )
	{
		shaderFilenamePlug.getValue( shaderFilename );

		if( shaderFilename.length() > 0 )
		{
			shaderPath = OSLUtils::OpenShader( shaderFilename, info );
			if( !info )
			{
				shaderFilename = MString( "" );
			}
		}
	}

	// The the node type name for OSL shader file lookup as a last resort
	if( shaderFilename.length() == 0 )
	{
		shaderFilename = i_shader.typeName();
		shaderPath = OSLUtils::OpenShader( shaderFilename, info );

		if( !info )
		{
			return;
		}
	}

	if( info->shadername().size() > 0 )
	{
		m_fullPathName = shaderPath + "/" + info->shadername().c_str() + ".oso";
	}
	/*
		For each shader param, store its param name as the key (attribute name),
		unless the 'attribute' or 'maya_attribute' meta-data is found, in which
		case they dictate the actual key string to be used.
	*/
	for( size_t i = 0;  i < info->nparams(); i++ )
	{
		const DlShaderInfo::Parameter* p = info->getparam( i );
		if( !p )
			break;

		MString param_name = p->name.c_str();
		MString attr_name = param_name;
		bool maya_attribute_found = false;
		int flags = 0;

		/*
			Never attempt to initialize a closure param
		*/
		if( p->isclosure )
		{
			flags |= ParamData::Flag_SkipInit;
		}

		for( unsigned int j = 0;  j < p->metadata.size(); j++ )
		{
			const DlShaderInfo::Parameter& d = p->metadata[ j ];

			if( d.type.IsOneString() && d.sdefault.size() > 0 )
			{
				/*
					maya_attribute metadata overrides the attribute one.

					"*none" is simply ignored as an attribute name. This allows
					interrogating the data cache to figure if attr_name is a shader
					parameter, which is something the incandescence light delegate
					needs to do.
				*/
				if( d.name == "maya_attribute" && d.sdefault[0] != "*none" )
				{
					attr_name = d.sdefault[0].c_str();
					maya_attribute_found = true;
				}
				else if( d.name == "attribute" && !maya_attribute_found )
				{
					attr_name = d.sdefault[0].c_str();
				}
				/*
					Since only "uvcoord" is used as a value for default_connection,
					it is cached as a flag.
				*/
				else if( d.name == "default_connection" && d.sdefault[0] == "uvCoord" )
				{
					flags |= ParamData::Flag_ConnectUVCoord;
				}
			}
			else if( d.type.IsOneInteger() )
			{
				if( d.name == "texturefile" && d.idefault[0] != 0 )
				{
					flags |= ParamData::Flag_TextureFile;
				}
				else if( d.name == "skip_init" && d.idefault[0] != 0 )
				{
					flags |= ParamData::Flag_SkipInit;
				}
			}
		}

		/*
			A single Maya attribute may be used a both an input and an output. This
			can be associated with one OSL shader parameter for the input context,
			and another OSL param for the output context. For this reason, the
			key used to index the table is the maya attribute name, postfixed with
			'i' or 'o' to indicate that the value is param data for an input OSL
			param or an output OSL param, respectively.
	`	*/
		NSIExportUtilities::IoType type =
			p->isoutput ? NSIExportUtilities::eOutput : NSIExportUtilities::eInput;

		attr_name = AttributeNameToTableKey( attr_name, type );

		ParamData &param_data = m_paramDataTable[attr_name.asChar()];
		param_data.m_name = p->name.string();
		param_data.m_type = p->type;
		param_data.m_flags = flags;
	}
}


const ParamData* ShaderData::GetParameterData(
	const MString& i_attributeName ) const
{
	auto it = m_paramDataTable.find(i_attributeName.asChar());
	return it == m_paramDataTable.end() ? nullptr : &it->second;
}

const ParamData* ShaderData::GetParameterData(
		const MPlug& i_plug,
		NSIExportUtilities::IoType i_type ) const
{
	return GetParameterData( AttributeToTableKey( i_plug, i_type ) );
}

const ParamData* ShaderData::GetParameterData(
	const char *i_parameter,
	NSIExportUtilities::IoType i_type ) const
{
	return GetParameterData( AttributeNameToTableKey(i_parameter, i_type) );
}

/**
	FIXME: why is output "i" and intput "o" ?
*/
MString ShaderData::AttributeNameToTableKey(
	const MString& i_attributeName,
	NSIExportUtilities::IoType i_type ) const
{
	if( i_type == NSIExportUtilities::eInput )
	{
		return MString( "o " ) + i_attributeName;
	}

	return MString( "i " ) + i_attributeName;
}

MString ShaderData::AttributeToTableKey(
	const MPlug& i_plug,
	NSIExportUtilities::IoType i_type ) const
{
	MString attribute;

	if( NSIExportUtilities::IsChildOfArrayOfCompoundAttr( i_plug ) )
	{
		/*
			Remove possible [x] from the compound child plug name; these attributes
			will have a partial name formatted like colorEntryList[2].position.
			OSL shaders expec	something like colorEntryList.position, which is an
			array containing values from the colorEntryList[*].position child
			attributes.
		*/
		MFnAttribute childFnAttr( i_plug.attribute() );
		MFnAttribute parentFnAttr( i_plug.parent().attribute() );

		attribute = parentFnAttr.name() + MString( "." ) + childFnAttr.name();
	}
	else if( i_plug.isElement() )
	{
		// we could always use this code branch
		MFnAttribute attr( i_plug.attribute() );
		attribute = attr.name();
	}
	else
	{
		attribute = NSIExportUtilities::AttributeName(i_plug);
	}

	return AttributeNameToTableKey( attribute, i_type );
}

ShaderDataCache::ShaderDataCache()
{
}

ShaderDataCache::~ShaderDataCache()
{
	Clear();
}

ShaderDataCache* ShaderDataCache::Instance()
{
	if( !m_instance )
	{
		CreateInstance();
	}

	return m_instance;
}

void ShaderDataCache::CreateInstance()
{
	m_mutex.lock();

	if( !m_instance )
	{
		m_instance = new ShaderDataCache;
	}

	m_mutex.unlock();
}

void ShaderDataCache::DeleteInstance()
{
	delete m_instance;

	m_mutex.lock();
	m_instance = 0x0;
	m_mutex.unlock();
}

const ShaderData* ShaderDataCache::Find( MObject i_shader )
{
	std::lock_guard guard{m_mutex};
	MFnDependencyNode depFn( i_shader );
	unsigned int id = depFn.typeId().id();
	auto it = m_shaderTable.find(id);
	return it == m_shaderTable.end() ? nullptr : it->second.get();
}

const ShaderData* ShaderDataCache::Add( MObject i_shader )
{
	MStatus status;
	MFnDependencyNode depFn( i_shader, &status );

	if( status != MStatus::kSuccess )
		return 0x0;

	std::lock_guard guard{m_mutex};

	// Use the type ID as the key for the shader data map.
	unsigned int id = depFn.typeId().id();
	std::shared_ptr<ShaderData> &data = m_shaderTable[id];
	if( !data )
	{
		data.reset(new ShaderData);
		data->CacheShaderData( depFn );
	}

	return data.get();
}

void ShaderDataCache::Clear()
{
	std::lock_guard guard{m_mutex};
	m_shaderTable.clear();
}
