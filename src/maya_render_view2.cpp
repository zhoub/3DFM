/******************************************************************************/
/*                                                                            */
/*    Copyright (c)The 3Delight Developers.                                   */
/*    All Rights Reserved.                                                    */
/*                                                                            */
/******************************************************************************/

#include "maya_render_view2.h"
#include "queue.h"
#include "ndspy.h"

#include "rx.h"
#include "RixInterfaces.h"

#include <maya/MEventMessage.h>
#include <maya/MGlobal.h>
#include <maya/MSelectionList.h>
#include <maya/MRenderView.h>
#include <maya/MComputation.h>
#include <maya/M3dView.h>
#include <maya/MTimerMessage.h>

#include <algorithm>
#include <mutex>
#include <queue>

#include <assert.h>
#include <limits.h>
#include <string.h>
#include <errno.h>

#define HERE  fprintf( stderr, "at %d in %s\n", __LINE__, __FUNCTION__ );

/*
*/
bool FindCameraNode( const char *camera_name, MObject &camera_node );
void ProcessEvents( void *i_image );

const void* GetParameter(
	const char *name, unsigned n,
	const UserParameter parms[] )
{
	for( unsigned i=0; i<n; i++ )
		if( !strcmp(name, parms[i].name) )
			return parms[i].value;
	return 0x0;
}

struct ImageInfos
{
	ImageInfos()
	:
		m_refresh_xmin(INT_MAX), m_refresh_xmax(INT_MIN), 
		m_refresh_ymin(INT_MAX), m_refresh_ymax(INT_MIN),

		m_valid(false), m_progressive(false)
	{
	}

	/*
		width and height of the render. This is the "original size" in our
		display driver system.
	*/
	unsigned m_width, m_height;
	   
	/* number of formats in this image */
	int m_num_formats;

	/* Crop origin. */
	int m_originx, m_originy;

	/* To catch ESC in the render view. */
	bool m_interrupted;

	/* Camera user for computation. */
	const char *m_camera_name;
	MDagPath m_camera;

	/* Refresh BBox. */
	int m_refresh_xmin, m_refresh_xmax, m_refresh_ymin, m_refresh_ymax;

	/*
		The update call back is called in the "idle" Maya state. The refresh
		callback is a timer that calls the render view refresh function.
	*/
	MCallbackId m_update_callback;
	MCallbackId m_refresh_callback;

	/* = true if it is "region" render. */
	bool m_cropped_render;

	/* = true if opening sequence succeeded. */
	bool m_valid;

	/* = true if progressive render. */
	bool m_progressive;

	/* It is a story of a queue and its mutex ... */
	std::queue<Event> m_event_queue;
	std::mutex m_mutex;
};

/*
*/
void RefreshRenderView( float, float, void *i_param )
{
	ImageInfos *image = (ImageInfos *)i_param;

	MRenderView::refresh(
		image->m_refresh_xmin, image->m_refresh_xmax,
		image->m_refresh_ymin, image->m_refresh_xmax );

#if 0
	/*
		You might think that it is a good idea to reset the bbox because we just
		painted what we need and now we can go to another region of the screen.
	
		Stop right there. This is Maya. The refresh area can only grow in size.
		You can try to activate this code to see what it does ... a strange
		flickering between the front and back buffer (if such things exist in 
		Maya).
	*/
	image->m_refresh_xmin = INT_MAX;
	image->m_refresh_xmax = INT_MIN; 
	image->m_refresh_ymin = INT_MAX;
	image->m_refresh_ymax = INT_MIN;
#endif
}

PtDspyError MayaDspyImageOpen(
	PtDspyImageHandle *i_phImage,
	const char * /*i_drivername*/,
	const char * /*i_filename*/,
	int i_width, int i_height,
	int i_paramCount,
	const UserParameter *i_parameters,
	int i_numFormats,
	PtDspyDevFormat i_formats[],
	PtFlagStuff * /*flagstuff*/ )
{
	if( !i_phImage )
	{
		return PkDspyErrorBadParams;
	}

	/*
		We can't render to the render view if we are in batch mode.
	*/
	if( !MRenderView::doesRenderEditorExist() )
	{
		fprintf(
			stderr,
			"maya_render_view.dpy: cannot render to maya's render view "
				"(running in batch mode).\n" );

		return PkDspyErrorNoResource;
	}

	int *original_size = (int *)
		GetParameter( "OriginalSize", i_paramCount, i_parameters );
	int *origin = (int *)
		GetParameter( "origin", i_paramCount, i_parameters );
	char **camera_name = (char **)
		GetParameter( "cameraname", i_paramCount, i_parameters );
	int *passes = (int *)
		GetParameter( "passes", i_paramCount, i_parameters );

	if( !camera_name || !camera_name[0] )
	{
		fprintf( stderr,
				"maya_render_view.dpy: no camera name specified, cannot proceede." );

		return PkDspyErrorBadParams;
	}


	/* We need floating point data */
	for( int i=0; i<i_numFormats; i++ )
		i_formats[i].type = PkDspyFloat32;

	ImageInfos *image = new ImageInfos;

	/*
		Start by setting up a callback that will process our events queue. The
		callback is run in Maya's "idle" render.
	*/
	MStatus maya_status;
    image->m_update_callback =
		MEventMessage::addEventCallback(
			"idle", ProcessEvents, image, &maya_status);

	if (maya_status != MS::kSuccess)
	{	
		fprintf( stderr,
			"maya_render_view.dpy: unable to start a Maya callback to process events." );
		delete image;
		return PkDspyErrorNoResource;
	}
 
	*i_phImage = (PtDspyImageHandle)image;

	assert( origin );
	assert( original_size );

	image->m_cropped_render =
		i_width != original_size[0] || i_height != original_size[1];

	image->m_progressive = passes && *passes!=0;
	image->m_num_formats = i_numFormats;
	image->m_width = original_size[0];
	image->m_height = original_size[1];
	image->m_originx = origin[0];
	image->m_originy = origin[1];
	image->m_interrupted = false;
	image->m_camera_name = strdup( *camera_name );

	image->m_event_queue.push( Event(Event::e_open, image) );

	return PkDspyErrorNone;
}

void ProcessOpenEvent( const Event &io_event )
{
	assert( io_event.m_type == Event::e_open );

	ImageInfos *image = io_event.m_image;

	/* Find out which camera is used for this render. First try to check
	   if we got a camera name as a parameter. Then try to get the active
	   camera. Then fail ... */

	MStatus maya_status;
	bool status = false;
	const char *camera_name = image->m_camera_name;

	if( camera_name )
	{
		MObject camera_node;

		if( (status = FindCameraNode( camera_name, camera_node)) )
		{
			image->m_camera.getAPathTo( camera_node );
		}
	}
	else
	{
		M3dView view = M3dView::active3dView(&maya_status);

		if( maya_status == MS::kSuccess )
			status = view.getCamera(image->m_camera) == MS::kSuccess;
	}

	if( status == true )
	{
		status = MRenderView::setCurrentCamera( image->m_camera ) == MS::kSuccess;
	}

	if( !status )
	{
		fprintf( stderr, 
			"dspy_maya: cannot render to maya view port "
				"(unable to get/set camera).\n" );

		return;
	}


	/* Start the render using a region. This will also work if the region
	   is actually the entire view */

	if( !image->m_cropped_render )
	{
		maya_status = MRenderView::startRender( 
			image->m_width, image->m_height, image->m_progressive, true);

		RV_PIXEL *black = new RV_PIXEL[ image->m_width * image->m_height ];
		memset( black, 0, image->m_width * image->m_height );

		MRenderView::updatePixels(
			0, image->m_width-1, 0, image->m_height-1, black, true);
		MRenderView::refresh(0, image->m_width-1, 0, image->m_height-1);

		delete [] black; /* racist! */
	}
	else
	{
		/* This will cause a red box to be drawn around the crop region. */
		unsigned left, right, bottom, top;
		MRenderView::getRenderRegion(left, right, bottom, top);
		
		maya_status = MRenderView::startRegionRender( 
			image->m_width, image->m_height,
			left, right, bottom, top,
			image->m_progressive, true);
	}

	if( maya_status != MS::kSuccess )
	{
		fprintf( stderr, "dspy_maya: unable to connect to Maya's Render View.\n");
		return;
	}

	/*
		We need a time to call Maya's render view refresh function.

		IMPORTANT: Don't move this to the DspyOpen function since the context of
		that function is not suitable to start a timer callback when starting
		a normal (non-IPR) render. For example, under Maya 2014, you would get
		the following error message:

			"QObject::startTimer: QTimer can only be used with 
				threads started with QThread"
	*/
 	image->m_refresh_callback = MTimerMessage::addTimerCallback(
		0.2f, &RefreshRenderView, image, &maya_status );

	if( maya_status != MS::kSuccess )
	{
		/*
			Should never happen but if it happens, the user will see no buckets
			in the render view but the render will still proceede.
		*/
		fprintf( stderr, 
			"maya_render_view.dpy: unable to setup refresh timer callback.\n" );
	}
	else
		image->m_valid = true;

   MString cmd;
   cmd += "global string $gMainProgressBar;";
   cmd += "progressBar -edit -beginProgress -status \"3Delight render in progress...\" -maxValue 100 -progress 0 $gMainProgressBar;";
   MGlobal::executeCommand(cmd, false);
}


/* Query */
PtDspyError MayaDspyImageQuery(
	PtDspyImageHandle i_hImage,
	PtDspyQueryType i_type,
	int i_datalen, 
	void *i_data )
{
	if( !i_data && i_type != PkStopQuery )
		return PkDspyErrorBadParams;

	ImageInfos* image = (ImageInfos*)i_hImage;

	switch(i_type)
	{
	case PkSizeQuery:
	{
		PtDspySizeInfo sizeQ;

		sizeQ.width = 512;
		sizeQ.height = 512;
		sizeQ.aspectRatio = 1;

		memcpy( i_data, &sizeQ,
		    i_datalen>int(sizeof(sizeQ)) ? sizeof(sizeQ) : i_datalen );

		break;
	}

	case PkOverwriteQuery:
	{
		PtDspyOverwriteInfo overwQ;

		overwQ.overwrite = 1;

		memcpy( i_data, &overwQ,
		    i_datalen>int(sizeof(overwQ)) ? sizeof(overwQ) : i_datalen );
		break;
	}

	case PkProgressiveQuery:
	{
		if( i_datalen < int(sizeof(PtDspyProgressiveInfo)) )
			return PkDspyErrorBadParams;

		((PtDspyProgressiveInfo*)i_data)->acceptProgressive = 1;
		break;
	}

	case PkCookedQuery:
	{
		PtDspyCookedInfo cookedQ;

		cookedQ.cooked = 1;

		memcpy( i_data, &cookedQ,
		    i_datalen>int(sizeof(cookedQ)) ? sizeof(cookedQ) : i_datalen );

		break;
	}

	case PkStopQuery:
	{
		if( image && image->m_interrupted )
			return PkDspyErrorStop;

		break;
	}

	default : return PkDspyErrorUnsupported;
	}

	return PkDspyErrorNone;
}

/*
	NOTES
	- We assume that the caller correctly set the quantize to
	  "0 255 0 255"
	- We have to reverse the image in Y since Maya does it the
	  other way around
*/
PtDspyError MayaDspyImageData(
	PtDspyImageHandle i_hImage,
	int i_xmin, int i_xmax_plusone,
	int i_ymin, int i_ymax_plusone,
	int i_entrySize,
	const unsigned char* i_cdata )
{
	ImageInfos* image = (ImageInfos*)i_hImage;

	int bucket_w = i_xmax_plusone - i_xmin;
	int bucket_h = i_ymax_plusone - i_ymin;

	RV_PIXEL *buffer = new RV_PIXEL[ bucket_h * bucket_w ];
	RV_PIXEL *d = buffer + bucket_w * (bucket_h-1); 

	for( int j = 0; j<bucket_h; j++ )
	{
		RV_PIXEL *current = d;
		d -= bucket_w;

		for( int i=0; i<bucket_w; i++, i_cdata+=i_entrySize )
		{
			float *in_data = (float *)i_cdata;

			switch( image->m_num_formats )
			{
			case 1:
			case 2:
				current->r = in_data[0];
				current->g = in_data[0];
				current->b = in_data[0];
				current->a = 255.0f;
				break;
			case 3:
				current->r = in_data[0];
				current->g = in_data[1];
				current->b = in_data[2];
				current->a = 255.0f;
				break;
			case 4:	
				current->r = in_data[0];
				current->g = in_data[1];
				current->b = in_data[2];
				current->a = in_data[3];
				break;
			}

			current ++;
		}
	}

	Event event(
		image,
		buffer,
		image->m_originx + i_xmin,
		image->m_originx + i_xmax_plusone-1,
		image->m_height-i_ymax_plusone - image->m_originy,
		image->m_height-i_ymin-1 - image->m_originy );

	image->m_event_queue.push( event );

	return PkDspyErrorNone;
}

void ProcessWriteEvent( Event &io_event )
{
	/*
		Inform Maya about the updated region but don't refresh it just
		yet. The refresh is generated in a separate timer. We keep a 
		bbox of update areas to refresh for that refresh timer.
	*/

	MRenderView::updatePixels(
		io_event.m_xmin, io_event.m_xmax,
		io_event.m_ymin, io_event.m_ymax,
		io_event.m_buffer );
		
	/*
		Update the the bbox we use for the refresh.
	*/
	io_event.m_image->m_refresh_xmin =
		std::min( io_event.m_image->m_refresh_xmin, io_event.m_xmin );
	io_event.m_image->m_refresh_xmax =
		std::max( io_event.m_image->m_refresh_xmax, io_event.m_xmax );

	io_event.m_image->m_refresh_ymin = 
		std::min( io_event.m_image->m_refresh_ymin, io_event.m_ymin );
	io_event.m_image->m_refresh_ymax = 
		std::max( io_event.m_image->m_refresh_ymax, io_event.m_ymax );

	delete [] io_event.m_buffer;

   MString cmd;
   cmd += "global string $gMainProgressBar;";
   cmd += "progressBar -edit -beginProgress -status \"3Delight render ...\" -maxValue 100 -progress 0 $gMainProgressBar;";
   //MGlobal::executeCommand(cmd, false);
}

PtDspyError MayaDspyImageClose( PtDspyImageHandle i_hImage )
{
	ImageInfos* image = (ImageInfos*)i_hImage;

	image->m_event_queue.push( Event(Event::e_close, image) );

	return PkDspyErrorNone;
}

bool FindCameraNode( const char *camera_name, MObject &camera_node )
{
	MObject node;
	MSelectionList list;

	if( MGlobal::getSelectionListByName((char*)camera_name, list) != MS::kSuccess )
		return false;

	if( list.getDependNode(0,node) != MS::kSuccess )
		return false;

	camera_node = node;

	return true;
}

void ProcessCloseEvent( const Event &i_event )
{
	ImageInfos *image = i_event.m_image;
	assert( image );

	if( !image->m_valid )
	{
		assert( !"Passing invalid image to close event." );
		return;
	}

	assert( image->m_update_callback );
	assert( image->m_refresh_callback );

	MMessage::removeCallback( image->m_update_callback );
	MMessage::removeCallback( image->m_refresh_callback );

	/*
		Issue a last refresh since be removing that timer we could
		still have non-refreshed but updated regions.
	*/
	RefreshRenderView( 0.0f, 0.0f, image );
}

void ProcessEvents( void *i_image )
{
	ImageInfos* image = (ImageInfos*)i_image;

	assert( image );

	bool done = false;
	image->m_mutex.lock();

	while( image->m_event_queue.size() != 0  )
	{
		Event &event = image->m_event_queue.front();

		switch( event.m_type )
		{
			case Event::e_open:
				ProcessOpenEvent( event );
				break;
			case Event::e_write_bucket:
				ProcessWriteEvent( event );
				break;
			case Event::e_close:
				done = true;
				ProcessCloseEvent( event );
				break;
		}

		image->m_event_queue.pop();
	}

	image->m_mutex.unlock();

	if( done == true )
	{
		delete image;
		return;
	}
	
	/*
		Wait for some time here so that we give Maya some room
		for exuting whatever evil instructions it is executing.
	*/
	unsigned i_microseconds =  5000;

#if defined(_WIN32)
	/* This waits for cancellation, up to i_microseconde microseconds. */
	WaitForSingleObject(
		m_data->m_cancellation_event, microseconds / 1000u );
#else
	timespec to_sleep, remain;
	to_sleep.tv_sec = i_microseconds / 1000000u;
	to_sleep.tv_nsec = (i_microseconds % 1000000u) * 1000u;

	while( 0 != nanosleep( &to_sleep, &remain ) )
	{
		if( errno != EINTR )
			break;

		to_sleep = remain;
	}
#endif
}

void RegisterMayaRenderView()
{
	PtDspyDriverFunctionTable table;
	memset( &table, 0, sizeof(table) );

	table.Version = k_PtDriverCurrentVersion;
	table.pOpen = &MayaDspyImageOpen;
	table.pQuery = &MayaDspyImageQuery;
	table.pWrite = &MayaDspyImageData;
	table.pClose = &MayaDspyImageClose;

	DspyRegisterDriverTable( "maya_render_view", &table );
}

/*
	This is needed by DL_bgRenderCmd.cpp but not really useful in our 
	context. Just here so that it links.
*/
void MayaRenderViewTimerBucketCallback(float, float, void*)
{

}
