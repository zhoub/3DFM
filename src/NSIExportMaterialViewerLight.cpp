
#include "NSIExportMaterialViewerLight.h"
#include "NSIExportMesh.h"

#include "DL_utils.h"

#include <nsi.hpp>

#include <maya/MColor.h>
#include <maya/MGlobal.h>
#include <maya/MPlug.h>
#include <maya/MPlugArray.h>
#include <maya/MFnLight.h>
#include <maya/MFnDirectionalLight.h>
#include <maya/MFnSpotLight.h>

#include <assert.h>

NSIExportMaterialViewerLight::NSIExportMaterialViewerLight(
	MFnDagNode &i_dag,
	const NSIExportDelegate::Context& i_context )
:
	NSIExportDelegate( i_dag, i_context ),
	m_type(e_invalid)
{
	MStatus status;
	const MObject &object =  Object();
	MFnLight light( object, &status );

	if( status != MStatus::kSuccess )
		return;

	if( object.hasFn(MFn::kAreaLight) )
		m_type = e_area;
	else if( object.hasFn(MFn::kSpotLight) )
		m_type = e_spot;
	else if( object.hasFn(MFn::kDirectionalLight) )
		m_type = e_distant;
	else
	{
		m_type = e_invalid;
		MGlobal::displayError(
			"3Delight for Maya: unsupported light type at DAG position " +
			i_dag.fullPathName() );
	}
}

NSIExportMaterialViewerLight::~NSIExportMaterialViewerLight()
{
}

/**
	\brief Create light shader node as well as the geometry of the
	light

	Three things are created here:

	0) The light shader itself
	1) The attribute node that will receive the light shader
	2) The geometry that will represent the light
*/
void NSIExportMaterialViewerLight::Create( void )
{
	if( m_type == e_invalid )
		return;

	NSI::Context nsi(m_nsi);
	MString shader_handle( MString(m_nsi_handle) + "|shader" );
	MString attribute_handle( MString(m_nsi_handle) + "|attributes" );

	/*
		Create the necessary shader node
	*/
	const char *oslshader = 0x0;
	if( m_type == e_spot )
		oslshader = "spotLight.oso";
	else if( m_type == e_distant )
		oslshader = "distantLight.oso";
	else if( m_type == e_area )
		oslshader = "areaLight.oso";
	assert( oslshader );

	MString shader_path = InstallationPath( oslshader );

	nsi.Create( shader_handle.asChar(), "shader" );
	nsi.SetAttribute(
		shader_handle.asChar(),
		NSI::CStringPArg("shaderfilename", shader_path.asChar()) );

	bool mesh_light = false;

	if( m_type == e_distant )
	{
		bool dummy;
		nsi.Create( m_nsi_handle, "environment" );
		double angle =
			getAttrDouble( Object(), "_3delight_solar_angle", 0.0f, dummy );
		nsi.SetAttribute( m_nsi_handle, NSI::DoubleArg( "angle", angle) );
	}
	else
	{
		float scale = (m_type == e_spot) ? 1e-3f : 1.f;
		float P[] = {
			-scale, -scale, 0.0f,  -scale, scale, 0.0f,
			scale, scale, 0.0f,   scale, -scale, 0.0f };
		int nvertices[] = { 4 };

		NSI::ArgumentList mesh_attributes;
		mesh_attributes.Add(
			NSI::Argument::New( "P" )
			->SetType( NSITypePoint )
			->SetCount( 4 )
			->SetValuePointer( const_cast<float*>(P)) );

		mesh_attributes.Add(
			NSI::Argument::New( "nvertices" )
			->SetType( NSITypeInteger )
			->SetCount( 1 )
			->SetValuePointer( const_cast<int*>(nvertices)) );

		nsi.Create( m_nsi_handle, "mesh" );
		nsi.SetAttribute( m_nsi_handle, mesh_attributes );
	}

	nsi.Create( attribute_handle.asChar(), "attributes" );
	nsi.SetAttribute( attribute_handle.asChar(),
		(
			NSI::IntegerArg( "visibility.camera", 0),
			NSI::IntegerArg( "visibility.camera.priority", 1)
		) );
}

/**
	\brief A no-op method because everyhing is done in the mesh delegate
*/
const char *NSIExportMaterialViewerLight::NSINodeType( void ) const
{
	return "transform";
}

/**
*/
void NSIExportMaterialViewerLight::SetAttributes( void )
{
	if( m_type == e_invalid )
	{
		assert( false );
		return;
	}

	bool dummy;

	NSI::Context nsi(m_nsi);
	MString shader_handle( MString(m_nsi_handle) + "|shader" );
	MString geometry_handle( MString(m_nsi_handle) + "|attributes" );

	MFnLight light( Object() );
	float intensity = light.intensity();
	MColor color = light.color();
	float i_color[3] = { color.r, color.g, color.b };

	NSI::ArgumentList parameters;
	parameters.Add( new NSI::FloatArg("intensity", intensity) );
	parameters.Add( new NSI::ColorArg("i_color", i_color) );

	if( m_type == e_spot )
	{
		MFnSpotLight spot( Object() );
		float coneAngle = spot.coneAngle();
		float penumbraAngle = spot.penumbraAngle();
		float dropoff = spot.dropOff();

		parameters.Add( new NSI::FloatArg("i_penumbraAngle", penumbraAngle) );
		parameters.Add( new NSI::FloatArg("i_coneAngle", coneAngle) );
		parameters.Add( new NSI::FloatArg("i_dropOff", dropoff) );
	}
	else if( m_type == e_area )
	{
		parameters.Add(new NSI::IntegerArg("normalize_area", 0));
		parameters.Add(new NSI::IntegerArg("twosided", 1));
		parameters.Add(new NSI::IntegerArg("decayRate", 2));
	}

	nsi.SetAttribute( shader_handle.asChar(), parameters );
}

/**
	\brief A no-op method because everything is done in the mesh delegate
*/
void NSIExportMaterialViewerLight::SetAttributesAtTime(
	double i_time, bool i_no_motion )
{
}

/**
	\brief In case of mesh lights, we need to undo some connetions and make
	some others.

	We need to connect the shader to the meshes that defining this light.
	FIXME: This will not override any material already assigned to them and
	thus this doesn't work. We need to have some way to  prioritize this
	assigment.
*/
void NSIExportMaterialViewerLight::Connect( const DelegateTable * )
{
	NSI::Context nsi( m_nsi );

	MString shader_handle( MString(m_nsi_handle) + "|shader" );
	MString attribute_handle( MString(m_nsi_handle) + "|attributes" );

	nsi.Connect(
		shader_handle.asChar(), "",
		attribute_handle.asChar(), "surfaceshader" );

	nsi.Connect(
		attribute_handle.asChar(), "",
		m_nsi_handle, "geometryattributes" );
}

/**
	\brief returns the full installation path to a given shader
*/
MString NSIExportMaterialViewerLight::InstallationPath( const char *i_shader )
{
	return getDelightPath() + "/maya/osl/" + i_shader;
}
