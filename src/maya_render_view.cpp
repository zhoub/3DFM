/******************************************************************************/
/*                                                                            */
/*    Copyright (c)The 3Delight Developers.                                   */
/*    All Rights Reserved.                                                    */
/*                                                                            */
/******************************************************************************/

#include "ndspy.h"

#include <maya/MEventMessage.h>
#include <maya/MGlobal.h>
#include <maya/MSelectionList.h>
#include <maya/MRenderView.h>
#include <maya/MComputation.h>
#include <maya/M3dView.h>
#include <maya/MTimerMessage.h>
#include <maya/MAnimControl.h>

#include <algorithm>
#include <condition_variable>
#include <list>
#include <mutex>

#include <time.h>
#include <assert.h>
#include <limits.h>
#include <string.h>

#define HERE  fprintf( stderr, "at %d in %s\n", __LINE__, __FUNCTION__ );

/* */
struct ImageInfos;
void PrintMiniStats( const ImageInfos * );
bool FindCameraNode( char *camera_name, MObject &camera_node );

const void* GetParameter(
	const char *name, unsigned n,
	const UserParameter parms[] )
{
	for( unsigned i=0; i<n; i++ )
		if( !strcmp(name, parms[i].name) )
			return parms[i].value;
	return 0x0;
}


struct ImageInfos
{
	ImageInfos()
	:
		m_camera_name(0x0),
		m_start_time( ::time(0x0) )
	{
	} 

	~ImageInfos()
	{
		if( m_camera_name )
			::free( m_camera_name );
	}
	
	/* number of formats in this image */
	int m_num_formats;

	/* Image height (without crop) */
	int m_fullheight;

	/* Crop origin. */
	int m_originx, m_originy;

    /* True if we're in IPR mode or not rendering from the UI thread for some
       other reason. */
	bool m_background_render;

	/* True if we're in IPR mode. */
	bool m_ipr;

	/* To catch ESC in the render view. */
	MComputation m_computation;
	bool m_interrupted;

	/* camera used to render */
	char *m_camera_name;

	/* */
	time_t m_start_time;
};

struct SyncStuff
{
	std::mutex m_mutex;
	std::condition_variable m_end_cond;
};

/*
	When doing IPR, maya does not like if we send new buckets while its main UI
	thread is doing something else. So we queue the buckets here and send them
	in bulk from a timer callback.
*/
struct UpdateJob
{
	RV_PIXEL *buffer;
	int xmin, xmax, ymin, ymax;
};

static std::list<UpdateJob*> s_jobs;
static SyncStuff s_sync;
static MCallbackId s_id;
static bool s_do_end = false;
static ImageInfos *s_image = 0x0;

/*
	This is a bit of a hack for windows in 2016 and all platforms in 2017 which 
	crash if we remove the callback from DspyImageClose. Instead, we set this flag
	and have the callback remove itself on the next call. Some extra care 
	(probably useless) is taken in case a new render is started before that 
	happens.
*/
static bool s_removeCallback;

bool s_has_last_region = false;
int s_last_xmin, s_last_xmax, s_last_ymin, s_last_ymax;

static void SendBuckets()
{
	int xmin = INT_MAX, xmax = INT_MIN, ymin = INT_MAX, ymax = INT_MIN;

	while( !s_jobs.empty() )
	{
		UpdateJob *job = s_jobs.front();
		s_jobs.pop_front();

		xmin = std::min( xmin, job->xmin );
		xmax = std::max( xmax, job->xmax );
		ymin = std::min( ymin, job->ymin );
		ymax = std::max( ymax, job->ymax );

		MRenderView::updatePixels(
			job->xmin,
			job->xmax,
			job->ymin,
			job->ymax,
			job->buffer );

		delete[] job->buffer;
		delete job;
	}

	if( xmax < xmin || ymax < ymin )
	{
		return;
	}

	/*
		We have to update the two last modified regions because of Maya's
		double buffering bullshit.
	*/
	if( s_has_last_region )
	{
		int new_xmin = std::min( xmin, s_last_xmin );
		int new_xmax = std::max( xmax, s_last_xmax );
		int new_ymin = std::min( ymin, s_last_ymin );
		int new_ymax = std::max( ymax, s_last_ymax );

		MRenderView::refresh( new_xmin, new_xmax, new_ymin, new_ymax );
	}
	else
	{
		MRenderView::refresh( xmin, xmax, ymin, ymax );
	}

	s_last_xmin = xmin;
	s_last_ymin = ymin;
	s_last_xmax = xmax;
	s_last_ymax = ymax;

	s_has_last_region = true;
}

void MayaRenderViewTimerBucketCallback( float, float, void* )
{
	std::unique_lock<std::mutex> lock(s_sync.m_mutex);
	if( s_image && !s_image->m_ipr &&
	    s_image->m_computation.isInterruptRequested() )
	{
		s_image->m_interrupted = true;
	}

	SendBuckets();

	if( s_removeCallback && s_id )
	{
		MMessage::removeCallback( s_id );
		s_id = 0x0;
	}

	if( s_do_end )
	{
		/* Inform maya that the render is finished. */
		PrintMiniStats( s_image );
		MRenderView::endRender();

		if( !s_image->m_ipr )
			s_image->m_computation.endComputation();

		s_do_end = false;
		s_sync.m_end_cond.notify_all();
	}
}

static void QueueBucket( UpdateJob *job )
{
	std::unique_lock<std::mutex> lock(s_sync.m_mutex);
	s_jobs.push_back( job );
}

static void FinishRender()
{
	std::unique_lock<std::mutex> lock(s_sync.m_mutex);

	if( s_image->m_ipr )
	{
		/* Must finish here as the UI thread is stuck in RiFrameEnd which is
		   itself waiting on this thread to finish IPR. */
		SendBuckets();
		PrintMiniStats( s_image );
		MRenderView::endRender();
	}
	else
	{
		/* Go finish things in the UI thread. See 
		   MayaRenderViewTimerBucketCallback above. */
		s_do_end = true;
		s_sync.m_end_cond.wait(lock);
		assert( !s_do_end );
	}

	s_removeCallback = true;

	s_image = 0x0;
}

PtDspyError MayaDspyImageOpen(
	PtDspyImageHandle *i_phImage,
	const char *i_drivername,
	const char *i_filename,
	int i_width, int i_height,
	int i_paramCount,
	const UserParameter *i_parameters,
	int i_numFormats,
	PtDspyDevFormat i_formats[],
	PtFlagStuff *flagstuff );

struct OpenData
{
	PtDspyImageHandle *phImage;
	int width, height;
	int paramCount;
	const UserParameter *parameters;
	int numFormats;
	PtDspyDevFormat *formats;

	PtDspyError return_value;

	std::mutex mutex;
	std::condition_variable cond;
};

static void OpenIdleCallback( void *i_data )
{
	OpenData &od = *reinterpret_cast<OpenData*>(i_data);

	if( od.phImage )
	{
		od.mutex.lock();

		od.return_value = MayaDspyImageOpen(
			od.phImage,
			0x0, /* We use this null to know where the call is from. */
			0x0,
			od.width, od.height,
			od.paramCount,
			od.parameters,
			od.numFormats,
			od.formats,
			0x0 );

		od.phImage = 0x0;

		od.mutex.unlock();
		od.cond.notify_all();
	}
}

/* Open */
PtDspyError MayaDspyImageOpen(
	PtDspyImageHandle *i_phImage,
	const char *i_drivername,
	const char * /*i_filename*/,
	int i_width, int i_height,
	int i_paramCount,
	const UserParameter *i_parameters,
	int i_numFormats,
	PtDspyDevFormat i_formats[],
	PtFlagStuff * /*flagstuff*/ )
{
	if( !i_phImage )
	{
		return PkDspyErrorBadParams;
	}

	/* We need floating point data */
	for( int i=0; i<i_numFormats; i++ )
		i_formats[i].type = PkDspyFloat32;

	ImageInfos *image = new ImageInfos;

	int *original_size = (int *)
		GetParameter( "OriginalSize", i_paramCount, i_parameters );
	int *origin = (int *)
		GetParameter( "origin", i_paramCount, i_parameters );
	int *passes = (int *)
		GetParameter( "passes", i_paramCount, i_parameters );
	int *bgrender = (int *)
		GetParameter( "backgroundrender", i_paramCount, i_parameters );
	
	if( !original_size || !origin )
	{
		return PkDspyErrorBadParams;	
	}

	image->m_num_formats = i_numFormats;
	image->m_fullheight = original_size[1];
	image->m_originx = origin[0];
	image->m_originy = origin[1];
	image->m_ipr = passes && *passes == 0;
	image->m_background_render = image->m_ipr || (bgrender && *bgrender == 1);
	image->m_interrupted = false;

	if( image->m_background_render && !image->m_ipr && i_drivername )
	{
		delete image;

		/*
			This crazy code is because we need to call the maya API functions
			from the main UI thread.
		*/
		OpenData od;
		od.phImage = i_phImage;
		od.width = i_width;
		od.height = i_height;
		od.paramCount = i_paramCount;
		od.parameters = i_parameters;
		od.numFormats = i_numFormats;
		od.formats = i_formats;

		std::unique_lock<std::mutex> lock(od.mutex);

		MStatus mstatus;
		MCallbackId open_idle_cb = MEventMessage::addEventCallback(
			"idleHigh", OpenIdleCallback, &od, &mstatus );
		if( !mstatus )
		{
			fprintf( stderr, "dspy_maya: callback setup failed.\n" );
			return PkDspyErrorNoResource;
		}

		od.cond.wait(lock);

		MMessage::removeCallback( open_idle_cb );

		return od.return_value;
	}

	if( !MRenderView::doesRenderEditorExist() )
	{
		fprintf(
			stderr,
			"dspy_maya: cannot render to maya's render view "
				"(running in batch mode).\n" );

		return PkDspyErrorNoResource;
	}

	*i_phImage = (PtDspyImageHandle)image;

	/* Find out which camera is used for this render. First try to check
	   if we got a camera name as a parameter. Then try to get the active
	   camera. Then fail ... */
	MDagPath camera;
	bool status = false;

	char **camera_name = (char **)
		GetParameter( "cameraname", i_paramCount, i_parameters );

	if( camera_name && *camera_name )
	{
		MObject camera_node;

		if( (status = FindCameraNode( *camera_name, camera_node)) )
		{
			status = MDagPath::getAPathTo( camera_node, camera );
		}
	}
	else
	{
		MStatus maya_status;
		M3dView view = M3dView::active3dView(&maya_status);

		if( maya_status == MS::kSuccess )
			status = view.getCamera(camera) == MS::kSuccess;
	}

	if( !status )
	{
		fprintf( stderr, 
			"dspy_maya: cannot render to maya view port "
				"(unable to get camera).\n" );

		return PkDspyErrorNoResource;
	}

	image->m_camera_name = ::strdup( *camera_name );

	MStatus maya_status = MRenderView::setCurrentCamera( camera );

	/* Start the render using a resgion. This will also work if the region
	   is actually the entire view */

	if( maya_status == MS::kSuccess )
	{
		if( original_size[0] == i_width && original_size[1] == i_height )
		{
			maya_status = MRenderView::startRender( 
				original_size[0], original_size[1],
				false, image->m_background_render );
		}
		else
		{
			/* This will cause a red box to be drawn around the crop region. */
			maya_status = MRenderView::startRegionRender( 
				original_size[0], original_size[1],
				origin[0], origin[0] + i_width-1,
				original_size[1]-1 - (origin[1] + i_height-1),
				original_size[1]-1 - origin[1],
				false, image->m_background_render );
		}
	}

	if( maya_status != MS::kSuccess )
	{
		fprintf( stderr, "dspy_maya: unable to connect to Maya's Render View.\n");
		return PkDspyErrorNoResource;
	}

	if( image->m_background_render )
	{
		/*
			On windows, this assert may be triggered if IPR is stopped and
			restarted very quickly (less than the callback delay). It's not
			really a problem.
		*/
		assert( !s_id );
		s_sync.m_mutex.lock();

		s_removeCallback = false;

		if( !image->m_ipr )
			image->m_computation.beginComputation();

		assert( !s_image );
		s_image = image;
		s_sync.m_mutex.unlock();

		if( !s_id )
		{
			s_id = MTimerMessage::addTimerCallback(
				0.2f, 
				&MayaRenderViewTimerBucketCallback, 
				0x0, 
				&maya_status );
		}

		if( maya_status != MS::kSuccess )
		{
			fprintf( stderr, "dspy_maya: unable to setup callback.\n" );
			return PkDspyErrorNoResource;
		}
	}

	return PkDspyErrorNone;
}


/* Query */
PtDspyError MayaDspyImageQuery(
	PtDspyImageHandle i_hImage,
	PtDspyQueryType i_type,
	int i_datalen, 
	void *i_data )
{
	if( !i_data && i_type != PkStopQuery )
		return PkDspyErrorBadParams;

	ImageInfos* image = (ImageInfos*)i_hImage;

	switch(i_type)
	{
	case PkSizeQuery:
	{
		PtDspySizeInfo sizeQ;

		sizeQ.width = 512;
		sizeQ.height = 512;
		sizeQ.aspectRatio = 1;

		memcpy( i_data, &sizeQ,
		    i_datalen>int(sizeof(sizeQ)) ? sizeof(sizeQ) : i_datalen );

		break;
	}

	case PkOverwriteQuery:
	{
		PtDspyOverwriteInfo overwQ;

		overwQ.overwrite = 1;

		memcpy( i_data, &overwQ,
		    i_datalen>int(sizeof(overwQ)) ? sizeof(overwQ) : i_datalen );
		break;
	}

	case PkProgressiveQuery:
	{
		if( i_datalen < int(sizeof(PtDspyProgressiveInfo)) )
			return PkDspyErrorBadParams;

		((PtDspyProgressiveInfo*)i_data)->acceptProgressive = 1;
		break;
	}

	case PkCookedQuery:
	{
		PtDspyCookedInfo cookedQ;

		cookedQ.cooked = 1;

		memcpy( i_data, &cookedQ,
		    i_datalen>int(sizeof(cookedQ)) ? sizeof(cookedQ) : i_datalen );

		break;
	}

	case PkStopQuery:
	{
		if( image && image->m_interrupted )
			return PkDspyErrorStop;

		break;
	}

	default : return PkDspyErrorUnsupported;
	}

	return PkDspyErrorNone;
}

/*
	NOTES
	- We assume that the caller correctly set the quantize to
	  "0 255 0 255"
	- We have to reverse the image in Y since Maya does it the
	  other way around
	- We use "MComputation to check for termination"
*/
PtDspyError MayaDspyImageData(
	PtDspyImageHandle i_hImage,
	int i_xmin, int i_xmax_plusone,
	int i_ymin, int i_ymax_plusone,
	int i_entrySize,
	const unsigned char* i_cdata )
{
	ImageInfos* image = (ImageInfos*)i_hImage;

	int bucket_w = i_xmax_plusone - i_xmin;
	int bucket_h = i_ymax_plusone - i_ymin;

	RV_PIXEL *buffer = new RV_PIXEL[ bucket_h * bucket_w ];
	RV_PIXEL *d = buffer + bucket_w * (bucket_h-1); 

	for( int j = 0; j<bucket_h; j++ )
	{
		RV_PIXEL *current = d;
		d -= bucket_w;

		for( int i=0; i<bucket_w; i++, i_cdata+=i_entrySize )
		{
			float *in_data = (float *)i_cdata;

			switch( image->m_num_formats )
			{
			case 1:
			case 2:
				current->r = in_data[0];
				current->g = in_data[0];
				current->b = in_data[0];
				current->a = 255.0f;
				break;
			case 3:
				current->r = in_data[0];
				current->g = in_data[1];
				current->b = in_data[2];
				current->a = 255.0f;
				break;
			case 4:	
				current->r = in_data[0];
				current->g = in_data[1];
				current->b = in_data[2];
				current->a = in_data[3];
				break;
			}

			current ++;
		}
	}

	PtDspyError ret = PkDspyErrorNone;

	if( image->m_background_render )
	{
		/* Because maya UI is a POS, we can't send data from this thread. */
		UpdateJob *job = new UpdateJob;
		job->buffer = buffer;
		job->xmin = image->m_originx + i_xmin;
		job->xmax = image->m_originx + i_xmax_plusone-1;
		job->ymin = image->m_fullheight-i_ymax_plusone - image->m_originy;
		job->ymax = image->m_fullheight-i_ymin-1 - image->m_originy;
		QueueBucket( job );

		if( image->m_interrupted )
			ret = PkDspyErrorStop;
	}
	else
	{
		MComputation computation;
		computation.beginComputation();

		MRenderView::updatePixels(
			image->m_originx + i_xmin,
			image->m_originx + i_xmax_plusone-1,
			image->m_fullheight-i_ymax_plusone - image->m_originy,
			image->m_fullheight-i_ymin-1 - image->m_originy,
			buffer );

		MRenderView::refresh(
			image->m_originx + i_xmin,
			image->m_originx + i_xmax_plusone-1,
			image->m_fullheight-i_ymax_plusone - image->m_originy,
			image->m_fullheight-i_ymin-1 - image->m_originy );

		delete [] buffer;

		if( computation.isInterruptRequested() )
			ret = PkDspyErrorStop;

		computation.endComputation();
	}

	return ret;
}

PtDspyError MayaDspyImageClose( PtDspyImageHandle i_hImage )
{
	ImageInfos* image = (ImageInfos*)i_hImage;

	if( image->m_background_render )
	{
		FinishRender();
	}
	else
	{
		/* Inform maya that the render is finished */
		MRenderView::endRender();
	}

	delete image;

	return PkDspyErrorNone;
}

bool FindCameraNode( char *camera_name, MObject &camera_node )
{
	MObject node;
	MSelectionList list;

	if( MGlobal::getSelectionListByName(camera_name, list) != MS::kSuccess )
		return false;

	if( list.getDependNode(0,node) != MS::kSuccess )
		return false;

	camera_node = node;

	return true;
}

void RegisterMayaRenderView()
{
	PtDspyDriverFunctionTable table;
	memset( &table, 0, sizeof(table) );

	table.Version = k_PtDriverCurrentVersion;
	table.pOpen = &MayaDspyImageOpen;
	table.pQuery = &MayaDspyImageQuery;
	table.pWrite = &MayaDspyImageData;
	table.pClose = &MayaDspyImageClose;

	DspyRegisterDriverTable( "maya_render_view", &table );
}

void PrintMiniStats( const ImageInfos *i_image )
{
	const time_t elapsed = ::time(0x0) - i_image->m_start_time;

	MString mini_stats("renderWindowEditor -edit -pcaption (\"    (3Delight)\\n");

	const int frame = int( MAnimControl::currentTime().as(MTime::uiUnit()) );
	mini_stats += "Frame: ";
	mini_stats += frame;
	mini_stats += "    ";

	mini_stats += "Render Time: ";
	mini_stats += int(elapsed / 60);
	mini_stats += ":";

	const int seconds= int( elapsed % 60 );
	if (seconds < 10)
		mini_stats += "0";

	mini_stats += seconds;
	mini_stats += "   ";

	mini_stats += "Camera: ";
	mini_stats += MString( i_image->m_camera_name );
	mini_stats += "\") renderView";

	MGlobal::executeCommandOnIdle(mini_stats, false);
}

