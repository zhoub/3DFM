#include "dlWorleyNoise.h"

#include <maya/MFloatVector.h>
#include <maya/MObjectArray.h>

#include "DL_autoLoadOSL.h"
#include "DL_utils.h"
#include "OSLUtils.h"

#include <cassert>

MObject dlWorleyNoise::s_color_in_cell;
MObject dlWorleyNoise::s_color_across_cell;
MObject dlWorleyNoise::s_outColor;

void* dlWorleyNoise::creator()
{
	return new dlWorleyNoise();
}

MStatus dlWorleyNoise::initialize()
{
	MStringArray shaderPaths = OSLUtils::GetBuiltInSearchPaths();
	MString shaderName( "dlWorleyNoise" );
	DlShaderInfo *info;

	OSLUtils::OpenShader( shaderName, shaderPaths, info );

	MObjectArray objects;
	MStringArray objectNames;

	DL_OSLShadingNode::CreateAttributesFromShaderParameters(
		info, 0x0, &objects, &objectNames );

	if(
		!DL_OSLShadingNode::FindAttribute(
			"color_in_cell.color_in_cell_Color", s_color_in_cell,
			objects, objectNames)
		|| !DL_OSLShadingNode::FindAttribute(
			"color_across_cell.color_across_cell_Color", s_color_across_cell,
			objects, objectNames)
		|| !DL_OSLShadingNode::FindAttribute(
			"outColor", s_outColor, objects, objectNames) )
	{
		assert(false);
		// It makes no sense to return success here, but if we don't, Maya crashes.
		return MStatus::kSuccess;
	}

	for( unsigned i = 0; i < objects.length(); i++ )
	{
		addAttribute( objects[ i ] );
	}

	attributeAffects( s_color_in_cell, s_outColor );
  attributeAffects( s_color_across_cell, s_outColor );

	MString name = info->shadername().c_str();
	MString niceName = OSLUtils::GetShaderNiceName( info );
	DL_OSLShadingNode::DefineShaderNiceName( name, niceName );

	return MStatus::kSuccess;
}

void dlWorleyNoise::postConstructor()
{
	setExistWithoutInConnections(true);
	setExistWithoutOutConnections(true);
}

MStatus dlWorleyNoise::compute(
	const MPlug& i_plug,
	MDataBlock& i_block )
{
	if ((i_plug != s_outColor) && (i_plug.parent() != s_outColor))
	{
		return MS::kUnknownParameter;
	}

  /*
    FIXME: set s_outColor to
    average(colors in s_color_in_cell) * average(colors in s_color_across_cell)
  */
	return MS::kSuccess;
}
