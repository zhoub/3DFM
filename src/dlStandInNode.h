#ifndef __dlStandInNode_h
#define __dlStandInNode_h

#include <maya/MPxLocatorNode.h>

/// Node type that allows importing an NSI archive into a render
class dlStandInNode : public MPxLocatorNode
{
public:

	/// The node type associated to this class
	static MTypeId id;

	/// Constructor
	dlStandInNode();
	/// Destructor
	virtual ~dlStandInNode();

	/// Returns whether a bounding box is available or not.
	virtual bool isBounded()const;
	/// Returns the node's bouding box.
	virtual MBoundingBox boundingBox()const;

	/// Returns a newly allocated instance of the node
	static void* creator();
	/// Initializes the node type itself by adding attributes to it.
	static MStatus initialize();

private:

	double m_bbox[6];

	static MObject m_filename;
};

#endif
