#include "dlStandInNode.h"

#include "DL_utils.h"
#include "MU_typeIds.h"

#include <maya/MFnTypedAttribute.h>


MTypeId dlStandInNode::id = MU_typeIds::DL_STANDIN;
MObject dlStandInNode::m_filename;

dlStandInNode::dlStandInNode()
{
	m_bbox[2] = m_bbox[1] = m_bbox[0] = -0.5;
	m_bbox[5] = m_bbox[4] = m_bbox[3] = 0.5;
}

dlStandInNode::~dlStandInNode()
{
}

bool dlStandInNode::isBounded()const
{
	return true;
}

MBoundingBox dlStandInNode::boundingBox()const
{
	return
		MBoundingBox(
			MPoint(m_bbox[0], m_bbox[1], m_bbox[2]),
			MPoint(m_bbox[3], m_bbox[4], m_bbox[5])); 
}

void* dlStandInNode::creator()
{
	return new dlStandInNode();
}

MStatus dlStandInNode::initialize()
{
	MFnTypedAttribute file_attr;
	m_filename = file_attr.create("filename", "file", MFnData::kString);
	Utilities::makeInputAttribute(file_attr);
	file_attr.setUsedAsFilename(true);
	addAttribute(m_filename);

	return MStatus::kSuccess;
}
