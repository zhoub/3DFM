#ifndef __dlToonGlass_H__
#define __dlToonGlass_H__

#include <maya/MPxNode.h>

class dlToonGlass : public MPxNode
{
public:
  static void* creator();
  static MStatus initialize();

  virtual void postConstructor();
};

#endif
