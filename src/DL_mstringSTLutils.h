#ifndef __DL_MSTRINGSTLUTILS_H__
#define __DL_MSTRINGSTLUTILS_H__

#include <string.h>

#include <maya/MString.h>

struct delight_mstringEquals
{
  bool operator()(const MString& s1, const MString& s2) const
  {
    return strcmp(s1.asChar(), s2.asChar()) == 0;
  }
};

struct delight_mstringLessThan
{
  bool operator()(const MString& s1, const MString& s2) const
  {
    return strcmp(s1.asChar(), s2.asChar()) < 0;
  }
};


namespace __gnu_cxx
{
/*  // this is a specialization of an existing hash<type> template
  template<> struct hash<MString>
  {
    size_t operator()(const MString& __s) const
    {
      return __stl_hash_string(__s.asChar());
    }
  }; */
}

#endif
