#ifndef __OpenVDBCache_h
#define __OpenVDBCache_h

#include <maya/MPxCommand.h>

struct OpenVDBCache : public MPxCommand
{
	static void* creator() { return new OpenVDBCache; }

	static MSyntax newSyntax();
	virtual MStatus doIt(const MArgList& args);
	virtual bool isUndoable() const { return false; }

	static bool GetGridVertices(
		const char *i_filename,
		const char *i_grid,
		const std::vector<float> **o_vertices,
		const std::vector<float> **o_colors);
};

#endif

