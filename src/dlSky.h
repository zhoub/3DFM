#ifndef __dlSky_H__
#define __dlSky_H__

#include <maya/MPxNode.h>

class dlSky : public MPxNode
{
public:
	static void* creator();
	static MStatus initialize();

	virtual void postConstructor();
	virtual MStatus compute( const MPlug&, MDataBlock& );

private:
	static MObject s_intensity;
	static MObject s_turbidity;
	static MObject s_ground_albedo;

	static MObject s_elevation;
	static MObject s_azimuth;

	static MObject s_sky_tint;
	static MObject s_sun_tint;
	static MObject s_sun_size;
	static MObject s_sun_enable;
	static MObject s_ground_enable;

	static MObject s_wavelengthR;
	static MObject s_wavelengthG;
	static MObject s_wavelengthB;
	static MObject s_uvCoord;
	static MObject s_uCoord;
	static MObject s_vCoord;

	static MObject s_outColor;
	static MObject s_outTransparency;
};

#endif
