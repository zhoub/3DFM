/*
  Copyright (c) 2014 The 3Delight Team.
*/

#include "DL_directXRenderAgent.h"

/* DirectX only. */
#ifdef _WIN32
#if MAYA_API_VERSION >= 201400

#include <assert.h>
#include <algorithm>

// Viewport 2.0
#include <maya/MDrawContext.h>

// DX stuff
#include <d3dcompiler.h>

DL_directXRenderAgent::DL_directXRenderAgent() :
  m_d3d_device( NULL ),
  m_d3d_device_context( NULL ),
  m_d3d_constant_buffer( NULL ),
  m_d3d_vertex_shader( NULL ),
  m_d3d_pixel_shader( NULL ),
  m_d3d_vertex_layout( NULL )
{
}

DL_directXRenderAgent* DL_directXRenderAgent::getAgent()
{
  static DL_directXRenderAgent obj;
  return &obj;
}

ID3D11Device* DL_directXRenderAgent::getDevice()
{
  return m_d3d_device;
}

void DL_directXRenderAgent::init()
{
  // Initial device
  if( !m_d3d_device || !m_d3d_device_context)
  {
    // get renderer
    MHWRender::MRenderer* theRenderer = MHWRender::MRenderer::theRenderer();
    if ( theRenderer )
    {
      m_d3d_device = (ID3D11Device*)theRenderer->GPUDeviceHandle();
      if ( m_d3d_device )
      {
        m_d3d_device->GetImmediateContext( &m_d3d_device_context);
      }
    }
  }
  assert( m_d3d_device );
  assert( m_d3d_device_context);

  // Init shaders
  initShaders();

  // Init buffers
  initBuffers();
}

void DL_directXRenderAgent::beginDraw(
      const MMatrix& i_world_view_matrix,
      const MMatrix& i_projection_matrix,
      const float* i_color)
{
  assert( m_d3d_device_context);
  if ( !m_d3d_device_context )
  {
    return;
  }

  if( m_d3d_vertex_shader && m_d3d_pixel_shader && m_d3d_vertex_layout )
  {
    // Set up shaders
    m_d3d_device_context->VSSetShader(m_d3d_vertex_shader, NULL, 0);
    m_d3d_device_context->IASetInputLayout(m_d3d_vertex_layout);
    m_d3d_device_context->PSSetShader(m_d3d_pixel_shader, NULL, 0);
  }

  // Compute matrix
  XMMATRIX dxTransform = XMMATRIX(
      (float)i_world_view_matrix.matrix[0][0],
      (float)i_world_view_matrix.matrix[0][1],
      (float)i_world_view_matrix.matrix[0][2],
      (float)i_world_view_matrix.matrix[0][3],

      (float)i_world_view_matrix.matrix[1][0],
      (float)i_world_view_matrix.matrix[1][1],
      (float)i_world_view_matrix.matrix[1][2],
      (float)i_world_view_matrix.matrix[1][3],

      (float)i_world_view_matrix.matrix[2][0],
      (float)i_world_view_matrix.matrix[2][1],
      (float)i_world_view_matrix.matrix[2][2],
      (float)i_world_view_matrix.matrix[2][3],

      (float)i_world_view_matrix.matrix[3][0],
      (float)i_world_view_matrix.matrix[3][1],
      (float)i_world_view_matrix.matrix[3][2],
      (float)i_world_view_matrix.matrix[3][3]);
  XMMATRIX dxProjection = XMMATRIX(
      (float)i_projection_matrix.matrix[0][0],
      (float)i_projection_matrix.matrix[0][1],
      (float)i_projection_matrix.matrix[0][2],
      (float)i_projection_matrix.matrix[0][3],

      (float)i_projection_matrix.matrix[1][0],
      (float)i_projection_matrix.matrix[1][1],
      (float)i_projection_matrix.matrix[1][2],
      (float)i_projection_matrix.matrix[1][3],

      (float)i_projection_matrix.matrix[2][0],
      (float)i_projection_matrix.matrix[2][1],
      (float)i_projection_matrix.matrix[2][2],
      (float)i_projection_matrix.matrix[2][3],

      (float)i_projection_matrix.matrix[3][0],
      (float)i_projection_matrix.matrix[3][1],
      (float)i_projection_matrix.matrix[3][2],
      (float)i_projection_matrix.matrix[3][3]);

  // Set constant buffer
  ConstantBufferDef cb;
  cb.fWVP = XMMatrixTranspose(dxTransform * dxProjection);
  cb.fMatColor = XMFLOAT4( i_color[0], i_color[1], i_color[2], i_color[3]);
  m_d3d_device_context->UpdateSubresource(
      m_d3d_constant_buffer, 0, NULL, &cb, 0, 0);
  m_d3d_device_context->VSSetConstantBuffers(0, 1, &m_d3d_constant_buffer);
  m_d3d_device_context->PSSetConstantBuffers(0, 1, &m_d3d_constant_buffer);
}

void DL_directXRenderAgent::draw(int i_object)
{
  if( i_object < m_d3d_buffers.size() )
  {
    D3D_Object& object = m_d3d_buffers[i_object];

    if( object.m_d3d_vertex_buffer && object.m_d3d_vertex_buffer )
    {
      drawSingleBuffer(
          object.m_d3d_vertex_buffer,
          object.m_d3d_index_buffer,
          object.m_d3d_plane_num_indexes);
    }
  }
}

int DL_directXRenderAgent::addObject(
      ID3D11Buffer* i_vertex_buffer,
      ID3D11Buffer* i_index_buffer,
      int i_indexes )
{
  if( i_vertex_buffer && i_index_buffer && i_indexes>0 )
  {
    m_d3d_buffers.push_back(
        D3D_Object(
          i_vertex_buffer,
          i_index_buffer,
          i_indexes));
    return m_d3d_buffers.size() - 1;
  }

  return -1;
}

void DL_directXRenderAgent::releaseResources()
{
  for(std::vector<D3D_Object>::iterator it = m_d3d_buffers.begin();
      it != m_d3d_buffers.end();
      it++)
  {
    it->m_d3d_vertex_buffer->Release();
    it->m_d3d_index_buffer->Release();
  }

  m_d3d_buffers.clear();

  if (m_d3d_vertex_shader)
  {
    m_d3d_vertex_shader->Release();
    m_d3d_vertex_shader = NULL;
  }
  if (m_d3d_pixel_shader)
  {
    m_d3d_pixel_shader->Release();
    m_d3d_pixel_shader = NULL;
  }
  if (m_d3d_vertex_layout)
  {
    m_d3d_vertex_layout->Release();
    m_d3d_vertex_layout = NULL;
  }
  if (m_d3d_constant_buffer)
  {
    m_d3d_constant_buffer->Release();
    m_d3d_constant_buffer = NULL;
  }
}

bool DL_directXRenderAgent::initShaders()
{
  assert( m_d3d_device );
  if ( !m_d3d_device )
    return false;

  HRESULT hr;
  DWORD dwShaderFlags = D3DCOMPILE_ENABLE_STRICTNESS;
  ID3DBlob* vsBlob = NULL;
  ID3DBlob* psBlob = NULL;

  /* Vertex shader */
  if (!m_d3d_vertex_shader)
  {
    hr = D3DCompile(
        getShaderCode().asChar(),
        getShaderCode().length(),
        NULL,
        NULL,
        NULL,
        "mainVS",
        "vs_5_0",
        dwShaderFlags,
        0,
        &vsBlob,
        NULL);

    if (FAILED(hr))
    {
      fprintf(
          stderr,
          "3Delight coordSystemNode: Failed to compile DX vertex shader\n");
      return false;
    }

    hr = m_d3d_device->CreateVertexShader(
        vsBlob->GetBufferPointer(),
        vsBlob->GetBufferSize(),
        NULL,
        &m_d3d_vertex_shader);

    if (FAILED(hr))
    {
      fprintf(
          stderr,
          "3Delight coordSystemNode: Failed to create DX vertex shader\n");
      vsBlob->Release();
      return false;
    }
  }

  /* Layout */
  if (!m_d3d_vertex_layout)
  {
    D3D11_INPUT_ELEMENT_DESC layout[] =
    {
      { "POSITION",
        0,
        DXGI_FORMAT_R32G32B32_FLOAT,
        0,
        0,
        D3D11_INPUT_PER_VERTEX_DATA,
        0 },
    };

    int numLayoutElements = sizeof(layout)/sizeof(layout[0]);

    hr = m_d3d_device->CreateInputLayout(
        layout,
        numLayoutElements,
        vsBlob->GetBufferPointer(),
        vsBlob->GetBufferSize(),
        &m_d3d_vertex_layout);

    vsBlob->Release();

    if (FAILED(hr))
    {
      fprintf(
          stderr,
          "3Delight coordSystemNode: Failed to create DX input layout\n");
      return false;
    }
  }

  /* Pixel shader */
  if (!m_d3d_pixel_shader)
  {
    hr = D3DCompile(
        getShaderCode().asChar(),
        getShaderCode().length(),
        NULL,
        NULL,
        NULL,
        "mainPS",
        "ps_5_0",
        dwShaderFlags,
        0,
        &psBlob,
        NULL);

    if (FAILED(hr))
    {
      fprintf(
          stderr,
          "3Delight coordSystemNode: Failed to compile DX vertex shader\n");
      m_d3d_vertex_shader->Release();
      m_d3d_vertex_layout->Release();
      return false;
    }

    hr = m_d3d_device->CreatePixelShader(
        psBlob->GetBufferPointer(),
        psBlob->GetBufferSize(),
        NULL,
        &m_d3d_pixel_shader);

    psBlob->Release();

    if (FAILED(hr))
    {
      fprintf(
          stderr,
          "3Delight coordSystemNode: Failed to create DX pixel shader\n");
      m_d3d_vertex_shader->Release();
      m_d3d_vertex_layout->Release();
      return false;
    }
  }

  return true;
}

bool DL_directXRenderAgent::initBuffers()
{
  assert( m_d3d_device );
  if ( !m_d3d_device )
  {
    return false;
  }

  if (!m_d3d_constant_buffer)
  {
    HRESULT hr;
    D3D11_BUFFER_DESC bd;
    ZeroMemory(&bd, sizeof(bd));

    bd.Usage = D3D11_USAGE_DEFAULT;
    bd.ByteWidth = sizeof(ConstantBufferDef);
    bd.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
    bd.CPUAccessFlags = 0;
    hr = m_d3d_device->CreateBuffer(&bd, NULL, &m_d3d_constant_buffer);
    if (FAILED(hr))
    {
      return false;
    }
  }

  return true;
}

void DL_directXRenderAgent::drawSingleBuffer(
    ID3D11Buffer* i_vertex_buffer,
    ID3D11Buffer* i_index_buffer,
    int i_indexes )
{
  assert( m_d3d_device_context);
  assert( i_vertex_buffer);
  assert( i_index_buffer);

  if ( !m_d3d_device_context || !i_vertex_buffer || !i_index_buffer )
  {
    return;
  }

  unsigned int mStride = sizeof(float)*3;
  unsigned int mOffset = 0;
  m_d3d_device_context->IASetVertexBuffers(
      0, 1, &i_vertex_buffer, &mStride, &mOffset);

  m_d3d_device_context->IASetIndexBuffer(
      i_index_buffer, DXGI_FORMAT_R16_UINT, 0);
  m_d3d_device_context->IASetPrimitiveTopology(
      D3D11_PRIMITIVE_TOPOLOGY_LINELIST);
  m_d3d_device_context->DrawIndexed(i_indexes, 0, 0);
}

const MString& DL_directXRenderAgent::getShaderCode()
{
  static const MString m_d3d_shader(
      "cbuffer ConstantBuffer : register(b0)\n"
      "{\n"
      "  matrix wvp : WorldViewProjection;\n"
      "  float4 matColor = float4(0.8, 0.2, 0.0, 1.0);\n"
      "}\n"
      "void mainVS(float4 vertex : POSITION, out float4 position: SV_POSITION)\n"
      "{\n"
      "  position = mul(vertex, wvp);\n"
      "}\n"
      "float4 mainPS() : SV_Target\n"
      "{\n"
      "  return matColor;\n"
      "}\n" );

  return m_d3d_shader;
}

#endif // MAYA_API_VERSION
#endif //_WIN32

