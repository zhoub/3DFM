/*
  Copyright (c) The 3Delight Team.
  Copyright (c) soho vfx inc.
*/

#ifndef __NSIExportCustom_h
#define __NSIExportCustom_h

#include "NSIExportDelegate.h"
#include "NSICustomExporter.h"


/**
	\brief Implementation of the NSIUtils interface class.

	This class allows 3DFM to add implementation details to NSIUtils such as its
	m_live member, without having to modify the published API.

	Of course, 3DFM also has the responsibility of creating an instance of
	NSIUtilsImp instead of NSIUtils when communicating with a NSICustomExporter
	or a NSICustomDelegate.
*/
struct NSIUtilsImp : public NSIUtils
{
	NSIUtilsImp(bool i_live) : m_live(i_live) {}

	virtual MString NSIHandle( MObject i_node );
	virtual MString NSIAttributesHandle( MObject i_node );
	virtual MString NSIShaderHandle( MObject i_node );

	bool m_live;
};


class NSIExportCustom : public NSIExportDelegate
{
public:
	NSIExportCustom( 		
		const NSICustomExporter* i_exporter, 
		MObject& i_object, 
		const NSIExportDelegate::Context& i_context, 
		void* i_data );

	virtual ~NSIExportCustom();
	
	/**
		\brief Creates the NSI node for this delegate
	*/
	virtual void Create();

	/**
		\brief Sets motion-less attributes
	*/
	virtual void SetAttributes( void );

	/**
		\brief Set attributes that depend on time

		\param i_time
			The time at which to output the attributes

		\param i_no_motion
			There are is actually no motion blur in the render, the delegate
			can output attributes without relying on i_time
	*/
	virtual void SetAttributesAtTime(	double i_time, bool i_no_motion );

	/**
		\brief Make any needed connections that must be done _after_ all the
		Create()s of the scene export.

		\param i_da_hash
			A hash table containing a Handle() -> Delegate lookup. This can
			be used to assess if an object is *really* part of the rendered
			scene. Even though an object is part of, e.g., a Maya set it doesn't
			mean that we are rendering it. This can happen when using the
			"Scene Elements" feature with a different objects/lights to render
			set. In short, do not connect to elements that are not part of the
			hash table if you want to avoid render error message (although the
			image will still be good if you do useless connections)
	*/
	virtual void Connect( const DelegateTable *i_dag_hash );

	/**
		\brief Any processing which must be done after all the
		SetAttributesAtTime
	*/
	virtual void Finalize();

	/**
		\brief Return true if this object is deformed.
	*/
	virtual bool IsDeformed( void ) const;

	/**
		\brief Register callbacks specific to this exporter

		\return true if the delegate registers it's own callbacks, false if
		it lets the exported deal with it.
	*/
	virtual bool RegisterCallbacks( void );

	/*
		\brief Provide a set object which should be inspected for attributes that
		affect how the geometry is produced.
	*/
	virtual void MemberOfSet( MObject& i_set );

private:
	const NSICustomExporter* m_exporter;
	NSICustomDelegate* m_delegate;
	NSIUtilsImp m_utils;
};

#endif
