/*
	Copyright (c) The 3Delight Team.
	Copyright (c) soho vfx inc.
*/

#include "VPFragmentRegistry.h"

#include <maya/MGlobal.h>

#include "DL_errors.h"
#include "VPUtilities.h"

#define VERBOSE_PREFIX "VP Shader Fragments"

VPFragmentRegistry* VPFragmentRegistry::m_instance = 0x0;
std::mutex VPFragmentRegistry::m_mutex;

VPFragmentRegistry::VPFragmentRegistry() :
	m_verbosity(0)
{
	MString verbosity( getenv( "_3DFM_FRAGMENT_VERBOSITY" ) );
	if( verbosity == MString("1") )
	{
		m_verbosity = 1;
	}
	else if( verbosity == MString("2") )
	{
		m_verbosity = 2;
	}
	else
	{
		m_verbosity = 0;
	}
}

VPFragmentRegistry::~VPFragmentRegistry()
{
	Clear();
}

VPFragmentRegistry* VPFragmentRegistry::Instance()
{
	if( !m_instance )
	{
		CreateInstance();
	}

	return m_instance;
}

void VPFragmentRegistry::CreateInstance()
{
	m_mutex.lock();

	if( !m_instance )
	{
		m_instance = new VPFragmentRegistry;
		m_mutex.unlock();

		m_instance->AddPath( VPUtilities::GetBuiltinFragmentPath() );
		m_instance->AddPath( VPUtilities::GetUserFragmentPath() );
	}
	else
	{
		m_mutex.unlock();
	}
}

void VPFragmentRegistry::DeleteInstance()
{
	delete m_instance;

	m_mutex.lock();
	m_instance = 0x0;
	m_mutex.unlock();
}

const MStringArray* VPFragmentRegistry::Find( const MString& i_nodeType )
{
	std::lock_guard guard{m_mutex};

	auto it = m_table.find(i_nodeType);
	return it == m_table.end() ? nullptr : &it->second;
}

void VPFragmentRegistry::Clear()
{
	std::lock_guard guard{m_mutex};

	for( const auto &item : m_table )
	{
		const MStringArray &data = item.second;
		for( unsigned int i = 0; i < data.length(); i++ )
		{
			VPUtilities::RemoveFragment(data[i]);
		}
	}

	m_table.clear();

}

void VPFragmentRegistry::AddPath( const MString& i_path )
{
	m_mutex.lock();

	if( m_paths.length() > 0 )
	{
#if defined(_WIN32)
		m_paths += MString(";");
#else
		m_paths += MString(":");
#endif
	}

	m_paths += i_path;
	ReportInfo( "Search path is now: " + m_paths, 2 );

	m_mutex.unlock();

	VPUtilities::AddFragmentPath( i_path );
}

MString VPFragmentRegistry::SearchPath()
{
	return m_paths;
}

void VPFragmentRegistry::Add(
	const MString& i_nodeType,
	MStringArray& o_fragmentNames )
{
	o_fragmentNames.clear();

	std::lock_guard guard{m_mutex};

	auto it = m_table.find(i_nodeType);
	if( it == m_table.end() )
	{
		/*
			Checking for the file existence is not mandatory but the Maya fragment
			is excessively verbose when a file is not found.
		*/
		MFileObject file;
		file.setRawPath( m_paths );
		file.setResolveMethod( MFileObject::kInputFile );

		/*
			The shading network can be made of a single fragment (fragmentName),
			which	is suitable for a single output.

			Shaders that produce more than one output var need to place them in a
			struct which is defined in its own fragment (outputName); the graph
			fragment (graphName) ties together the shading code (fragmentName)
			and the output struct fragment.
		*/
		MString fragmentName = i_nodeType + "Fragment";
		MString outputName = i_nodeType + "FragmentOutput";
		MString graphName = i_nodeType + "FragmentGraph";

		bool hasFragment = AddFragment( fragmentName, false, file );
		bool hasOutput = AddFragment( outputName, false, file );
		bool hasGraph = AddFragment( graphName, true, file );

		/*
			The order in o_fragmentNameorder matters. First element is the name of
			the fragment the VP shader override can announce to be using.
		*/
		if( hasGraph )
			o_fragmentNames.append( graphName );

		if( hasFragment )
			o_fragmentNames.append( fragmentName );

		if( hasOutput )
			o_fragmentNames.append( outputName );

		m_table.emplace(i_nodeType, o_fragmentNames);
	}
	else
	{
		o_fragmentNames = it->second;

		if( m_verbosity > 0 )
		{
			MString msg = MString( "Add fragments for " ) + i_nodeType +
				" skipped because fragments already exist:";

			for( unsigned i = 0; i < o_fragmentNames.length(); i++ )
			{
				msg += " " + o_fragmentNames[i];
			}
			ReportInfo( msg, 1 );
		}
	}
}

void VPFragmentRegistry::Remove( const MString& i_nodeType )
{
	std::lock_guard guard{m_mutex};

	auto it = m_table.find(i_nodeType);
	if( it == m_table.end() )
	{
		ReportInfo( "No known fragments to remove for " +	i_nodeType, 1 );
		return;
	}

	const MStringArray& fragments = it->second;
	for( unsigned int i = 0; i < fragments.length(); i++ )
	{
		RemoveFragment(fragments[i]);
	}

	m_table.erase(it);
}

void VPFragmentRegistry::Add(
	const MStringArray& i_fragments,
	const MStringArray& i_fragmentGraphs )
{
	std::lock_guard guard{m_mutex};

	MString key = "<common>";

	if( m_table.find(key) != m_table.end() )
		return;

	{
		MStringArray &fragmentsToCache = m_table[key];
		for( unsigned i = 0; i < i_fragments.length(); i++ )
		{
			MString result = VPUtilities::AddFragment( i_fragments[ i ], false );
			if( result.length() > 0 )
			{
				fragmentsToCache.append( result );
			}

			ReportInfo("Adding common fragment " + i_fragments[ i ] +	": " +
				(result == i_fragments[ i ] ? "success." : "failure." ), 1 );
		}

		for( unsigned i = 0; i < i_fragmentGraphs.length(); i++ )
		{
			MString result = VPUtilities::AddFragment( i_fragmentGraphs[ i ], true );
			if( result.length() > 0 )
			{
				fragmentsToCache.append( result );
			}

			ReportInfo( "Adding common fragment graph " +
				i_fragmentGraphs[ i ] + ": " +
				( result == i_fragments[ i ] ? "success." : "failure." ),
				1 );
		}
	}
}

void VPFragmentRegistry::Verbosity( int i_value )
{
	m_verbosity = i_value;
}

int VPFragmentRegistry::Verbosity()
{
	return m_verbosity;
}

bool VPFragmentRegistry::AddFragment(
	const MString& i_fragmentName,
	bool i_isGraph,
	MFileObject& i_file )
{
	bool hasFragment = VPUtilities::FragmentExists( i_fragmentName );
	if( !hasFragment )
	{
		i_file.setRawName( i_fragmentName + ".xml" );

		ReportInfo( "Searching for fragment " + i_fragmentName + ":", 2 );
		ReportInfo( "  unresolved file name:" + i_file.rawFullName(), 2 );
		ReportInfo( "  resolved file name:" +	i_file.resolvedFullName(), 2 );

		if( i_file.exists() )
		{
			MString regFragment =
				VPUtilities::AddFragment( i_fragmentName, i_isGraph );

			hasFragment = regFragment == i_fragmentName;

			ReportInfo( "Adding fragment " + i_fragmentName + ": " +
					( hasFragment ? MString("success") : MString("failure.") ), 1 );
		}
		else
		{
			ReportInfo( "Adding fragment " + i_fragmentName +
				" failed: file " + i_file.rawName() + " was not found.", 1 );
		}
	}
	else
	{
		ReportInfo( "Fragment " + i_fragmentName + " already exists.", 1 );
	}

	return hasFragment;
}

bool VPFragmentRegistry::RemoveFragment( const MString& i_fragmentName )
{
	bool exists;
	if( m_verbosity > 0 )
	{
		exists = VPUtilities::FragmentExists( i_fragmentName );
	}

	bool success = VPUtilities::RemoveFragment( i_fragmentName );

	MString msg = MString( "Removing fragment " )  + i_fragmentName +
		" : " + (success ? "success" : "failure");

	if( !exists )
	{
		msg += " (fragment does not exists)";
	}

	ReportInfo( msg, 1 );

	return success;
}

void VPFragmentRegistry::ReportInfo( const MString& i_msg, int i_threshold )
{
	if( m_verbosity >= i_threshold )
	{
		MGlobal::displayInfo( _3DFM_ERR_PREFIX(VERBOSE_PREFIX) + i_msg );
	}
}
