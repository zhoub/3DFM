#include "dlAOV.h"

#include <maya/MFnCompoundAttribute.h>
#include <maya/MFnEnumAttribute.h>
#include <maya/MFnMessageAttribute.h>
#include <maya/MFnNumericAttribute.h>
#include <maya/MFnStringData.h>
#include <maya/MFnTypedAttribute.h>

#include "dlCallbacksCmd.h"
#include "DL_errors.h"
#include "DL_utils.h"

dlAOVCommonAttrs dlAOVPrimitiveAttribute::m_common;

dlAOVCommonAttrs dlAOV::m_common;
MObject dlAOV::m_defaultValue;

MObject dlAOVGroup::m_colorAOVs;
MObject dlAOVGroup::m_colorAOVValues;
MObject dlAOVGroup::m_colorAOVNames;

MObject dlAOVGroup::m_floatAOVs;
MObject dlAOVGroup::m_floatAOVValues;
MObject dlAOVGroup::m_floatAOVNames;

MObject dlAOVGroup::m_outColor;

/*
	dlAOVCommonAttrs
*/
MStatus dlAOVCommonAttrs::initialize()
{
	MStatus status;
	MString attrName("variableName");

	MFnStringData stringDataFn;
	MObject emptyStringDataObj = stringDataFn.create();

	MFnTypedAttribute typedAttrFn;
	m_name = typedAttrFn.create(
		attrName,
		attrName,
		MFnData::kString,
		emptyStringDataObj,
		&status );
	RETURN_ON_FAILED_MSTATUS(status);

	Utilities::makeInputAttribute( typedAttrFn );
	typedAttrFn.setHidden( true );

	attrName = MString("type");

	MFnEnumAttribute enumAttrFn;
	m_type = enumAttrFn.create( attrName, attrName, 1, &status );
	RETURN_ON_FAILED_MSTATUS(status);

	enumAttrFn.addField( "Float", 0 );
	enumAttrFn.addField( "Color", 1 );
	enumAttrFn.addField( "Vector", 2 );
	enumAttrFn.addField( "Quad", 3 );

	enumAttrFn.setNiceNameOverride(MString("Output Format"));
	Utilities::makeInputAttribute(enumAttrFn);
	enumAttrFn.setHidden( true );

	attrName = MString("source");

	MString defaultSource( "shader" );
	MObject defaultSourceDataObj = stringDataFn.create( defaultSource );

	m_source = typedAttrFn.create(
		attrName,
		attrName,
		MFnData::kString,
		defaultSourceDataObj,
		&status );
	RETURN_ON_FAILED_MSTATUS(status);

	Utilities::makeInputAttribute( typedAttrFn );
	typedAttrFn.setHidden( true );

	return status;
}

/*
	dlAOVPrimitiveAttribute
*/
void* dlAOVPrimitiveAttribute::creator()
{
	return new dlAOVPrimitiveAttribute();
}

MStatus dlAOVPrimitiveAttribute::initialize()
{
	MStatus status = m_common.initialize();
	RETURN_ON_FAILED_MSTATUS(status);

	// Unhide the type attribute
	MFnAttribute attrFn( m_common.m_type );
	attrFn.setHidden( false );

	// Set "attribute" as the default value for the source attribute.
	MFnTypedAttribute sourceTypedAttrFn( m_common.m_source );

	MFnStringData stringDataFn;
	MString defaultSource( "attribute" );
	MObject defaultSourceDataObj = stringDataFn.create( defaultSource );

	sourceTypedAttrFn.setDefault( defaultSourceDataObj );

	MFnAttribute varNameAttrFn( m_common.m_name );
	varNameAttrFn.setNiceNameOverride( "Primitive Attribute" );

	status = addAttribute( m_common.m_name );
	status = addAttribute( m_common.m_type );
	status = addAttribute( m_common.m_source );
	RETURN_ON_FAILED_MSTATUS(status);

	return status;
}

void dlAOVPrimitiveAttribute::postConstructor()
{
	setExistWithoutInConnections(true);
	setExistWithoutOutConnections(true);

	// Execute callback on aov node created event
	if( !Utilities::isReadingSceneFile() )
	{
		MString cmd(DLCALLBACKSCMD_STR);
		cmd += MString(" -hook \"AOVNodeCreated\" -executeCallbacks");
		cmd += MString(" -executeStringArg \"") + typeName() + MString("\"");
		MGlobal::executeCommandOnIdle(cmd);
	}
}

MStatus dlAOVPrimitiveAttribute::compute(
	const MPlug& i_plug,
	MDataBlock& i_block )
{
	return MS::kSuccess;
}

/*
	dlAOV
*/
void* dlAOV::creator()
{
	return new dlAOV();
}

MStatus dlAOV::initializeFloat()
{
	MStatus status = m_common.initialize();
	RETURN_ON_FAILED_MSTATUS(status);

	status = addAttribute( m_common.m_name );
	status = addAttribute( m_common.m_type );
	status = addAttribute( m_common.m_source );
	RETURN_ON_FAILED_MSTATUS(status);

	MFnNumericAttribute numAttrFn;
	m_defaultValue = numAttrFn.create(
		"floatInput",
		"floatInput",
		MFnNumericData::kDouble,
		0.0,
		&status);
	RETURN_ON_FAILED_MSTATUS( status );

	Utilities::makeInputAttribute( numAttrFn );
	numAttrFn.setNiceNameOverride( "Float Input" );
	numAttrFn.setHidden( true );

	status = addAttribute( m_defaultValue );

	return status;
}

MStatus dlAOV::initializeColor()
{
	MStatus status = m_common.initialize();
	RETURN_ON_FAILED_MSTATUS(status);

	status = addAttribute( m_common.m_name );
	MFnEnumAttribute enumAttrFn( m_common.m_type );
	enumAttrFn.setDefault( 1 );
	status = addAttribute( m_common.m_type );
	status = addAttribute( m_common.m_source );
	RETURN_ON_FAILED_MSTATUS(status);

	MFnNumericAttribute numAttrFn;
	m_defaultValue = numAttrFn.createColor( "colorInput",	"colorInput", &status );
	RETURN_ON_FAILED_MSTATUS( status );

	numAttrFn.setDefault( 0.0, 0.0, 0.0 );
	numAttrFn.setNiceNameOverride( "Color Input" );
	Utilities::makeInputAttribute( numAttrFn );
	numAttrFn.setHidden( true );

	status = addAttribute( m_defaultValue );

	return status;
}

void dlAOV::postConstructor()
{
	setExistWithoutInConnections(true);
	setExistWithoutOutConnections(true);

	// Execute callback on aov node created event
	if( !Utilities::isReadingSceneFile() )
	{
		MString cmd(DLCALLBACKSCMD_STR);
		cmd += MString(" -hook \"AOVNodeCreated\" -executeCallbacks");
		cmd += MString(" -executeStringArg \"") + typeName() + MString("\"");
		MGlobal::executeCommandOnIdle(cmd);
	}
}

MStatus dlAOV::compute( const MPlug& i_plug, MDataBlock& i_block )
{
	return MS::kSuccess;
}

/*
	dlAOVGroup
*/
void* dlAOVGroup::creator()
{
	return new dlAOVGroup;
}

MStatus dlAOVGroup::initialize()
{
	MStatus status;
	MString attrName("colorAOV");

	MFnMessageAttribute msgAttrFn;
	m_colorAOVNames = msgAttrFn.create( attrName, attrName );
	Utilities::makeInputAttribute( msgAttrFn );
	msgAttrFn.setNiceNameOverride( "AOV" );
	status = addAttribute( m_colorAOVNames );

	attrName = MString("colorValue");
	MFnNumericAttribute numAttrFn;
	m_colorAOVValues = numAttrFn.createColor( attrName,	attrName, &status );
	RETURN_ON_FAILED_MSTATUS( status );

	numAttrFn.setDefault( 0.0, 0.0, 0.0 );
	numAttrFn.setNiceNameOverride( "Value" );
	Utilities::makeInputAttribute( numAttrFn );

	status = addAttribute( m_colorAOVValues );

	attrName = MString("colorAOVs");
	MFnCompoundAttribute cmpdAttrFn;
	m_colorAOVs = cmpdAttrFn.create( attrName, attrName );
	cmpdAttrFn.addChild( m_colorAOVNames );
	cmpdAttrFn.addChild( m_colorAOVValues );
	cmpdAttrFn.setArray( true );

	addAttribute( m_colorAOVs );

	attrName = "floatAOV";
	m_floatAOVNames = msgAttrFn.create( attrName, attrName );
	Utilities::makeInputAttribute( msgAttrFn );
	msgAttrFn.setNiceNameOverride( "AOV" );
	status = addAttribute( m_floatAOVs );

	attrName = MString("floatValue");
	m_floatAOVValues = numAttrFn.create(
		attrName,
		attrName,
		MFnNumericData::kDouble,
		0.0,
		&status);

	Utilities::makeInputAttribute( numAttrFn );
	numAttrFn.setNiceNameOverride( "Value" );
	numAttrFn.setSoftMin(0.0);
	numAttrFn.setSoftMax(1.0);
	status = addAttribute( m_floatAOVValues );

	attrName = MString("floatAOVs");
	m_floatAOVs = cmpdAttrFn.create( attrName, attrName );
	cmpdAttrFn.addChild( m_floatAOVNames );
	cmpdAttrFn.addChild( m_floatAOVValues );
	cmpdAttrFn.setArray( true );

	addAttribute( m_floatAOVs );


	attrName = "outColor";
	m_outColor = numAttrFn.createColor( attrName, attrName, &status );
	numAttrFn.setDefault( 0.0, 0.0, 0.0 );
	numAttrFn.setNiceNameOverride( "Out Color" );
	Utilities::makeOutputAttribute( numAttrFn );

	status = addAttribute( m_outColor );

	return status;
}

void dlAOVGroup::postConstructor()
{
	setExistWithoutInConnections(true);
	setExistWithoutOutConnections(true);
}

MStatus dlAOVGroup::compute( const MPlug&, MDataBlock& )
{
	return MStatus::kSuccess;
}
