#include "NSIExportLight.h"

#include "DelegateTable.h"
#include "DL_utils.h"
#include "MU_typeIds.h"
#include "NSIExportMesh.h"
#include "NSIExportPriorities.h"
#include "NSIExportUtilities.h"
#include "OSLUtils.h"

#include <nsi.hpp>

#include <maya/MColor.h>
#include <maya/MDagPath.h>
#include <maya/MGlobal.h>
#include <maya/MPlug.h>
#include <maya/MPlugArray.h>
#include <maya/MFnDirectionalLight.h>
#include <maya/MFnSpotLight.h>
#include <maya/MFnSet.h>
#include <maya/MSelectionList.h>

#include <assert.h>

#define CAMERA_VISIBILITY_PRIORITY 10

NSIExportLight::NSIExportLight(
	MFnDagNode &i_dag,
	const NSIExportDelegate::Context& i_context )
:
	NSIExportDelegate( i_dag, i_context ),
	m_shader_delegate( i_dag.object(), i_context ),
	m_type(e_invalid),
	m_is_environment(i_dag.typeId() == MU_typeIds::DL_ENVIRONMENTNODE)
{

	if( Object().hasFn(MFn::kAreaLight) )
		m_type = e_area;
	else if( Object().hasFn(MFn::kSpotLight) )
		m_type = e_spot;
	else if( Object().hasFn(MFn::kDirectionalLight) || m_is_environment )
	{
		m_type = e_distant;
	}
	else if( Object().hasFn(MFn::kPointLight) )
		m_type = e_point;
	else
	{
		m_type = e_invalid;
		MGlobal::displayError(
			"3Delight for Maya: unsupported light type at DAG position " +
			i_dag.fullPathName() );
	}
}

NSIExportLight::~NSIExportLight()
{
}

/**
	\brief Create light nodes

	This delegate's handle corresponds to the light shape; its NSI node type
	will be of the proper type depending on the type of light (mesh or
	environment), or a transform where custom mesh light shapes will be
	connected.

	The light shader is handled by the light material delegate.

	An attribute nodes is also created to define visibility properties.
*/
void NSIExportLight::Create( void )
{
	if( m_type == e_invalid )
		return;

	/*
		Create the material attribute node and its shader
	*/
	m_shader_delegate.Create();

	NSI::Context nsi(m_nsi);

	if( m_type == e_distant )
	{
		bool dummy;
		nsi.Create( m_nsi_handle, "environment" );
	}
	else
	{
		nsi.Create( m_nsi_handle, "transform" );
		CreateDefaultAreaLightShape();
	}

	// Create the attribute node for this light
	nsi.Create( AttributesHandle().asChar(), "attributes" );

	if( m_is_environment )
	{
		MString shaderFilename =
			OSLUtils::GetFullpathname( "uvCoordEnvironment" );

		if( shaderFilename.length()!=0 )
		{
			/*
				We need an extra UV lookup node. We can't use one for all as
				the mapping could change from one node to the other. There are
				also live render considerations.

				Note that we don't connect this node here, this is the job of
				\ref NSIExportShader.
			*/
			MString uvCoord = MString(Handle()) + "*uvCoord";
			nsi.Create( uvCoord.asChar(), "shader" );
			nsi.SetAttribute( uvCoord.asChar(),
				NSI::CStringPArg( "shaderfilename", shaderFilename.asChar() ) );
		}
		else
		{
			MGlobal::displayError(
				"3Delight for Maya: cannot load internal OSL shader "
				"'uvCoordEnvironment'. Environment light won't render properly"
				);
		}
	}
}

/**
	\brief A no-op method because everything is done in the mesh delegate
*/
const char *NSIExportLight::NSINodeType( void ) const
{
	return "transform";
}

/**
	\sa NSIExportDefaultLightSet, NSIExportLightLinker
*/
void NSIExportLight::SetAttributes( void )
{
	if( m_type == e_invalid )
	{
		/** Could be an unsupport light, e.g. ambient */
		return;
	}

	m_shader_delegate.SetAttributes();

	NSI::Context nsi(m_nsi);
	MString attributes_handle = AttributesHandle();

	bool dummy;

	if( m_type == e_area || m_is_environment || m_type == e_distant )
	{
		bool prelit =
			getAttrBool( Object(), "_3delight_prelit", false, dummy );

		nsi.SetAttribute( attributes_handle.asChar(),
			(
				NSI::IntegerArg("prelit", prelit),
				NSI::IntegerArg("prelit.priority", NSI_PRELIT_OBJ_PRIORITY)
			) );

		int camera_visibility =
			getAttrInt( Object(), "_3delight_arealight_visibility", 0, dummy );

		nsi.SetAttribute( attributes_handle.asChar(),
			(
				NSI::IntegerArg("visibility.camera", camera_visibility ? 1 : 0),
				NSI::IntegerArg("visibility.camera.priority",
					CAMERA_VISIBILITY_PRIORITY)
			) );
	}

	if( m_type == e_spot || m_type == e_point )
	{
		/* Always invisible to camera */
		nsi.SetAttribute( attributes_handle.asChar(),
			(
				NSI::IntegerArg("visibility.camera", 0),
				NSI::IntegerArg("visibility.camera.priority",
					CAMERA_VISIBILITY_PRIORITY )
			) );
	}

	if( m_type == e_distant && !m_is_environment  )
	{
		/*
			Directional angle is 0, environment is 360 but that's the default
			for angle so there is no need to specify it.
		*/
		double angle =
			getAttrDouble( Object(), "_3delight_angular_size", 0.0f, dummy );
		nsi.SetAttribute( m_nsi_handle, NSI::DoubleArg( "angle", angle) );
	}

	if( m_type == e_point )
	{
		double radius = getAttrDouble( Object(), "_3delight_radius", 1e-3, dummy );
		MString handle = DefaultMeshHandle();
		nsi.SetAttribute( handle.asChar(),
			NSI::FloatArg("width", float(radius)) );
	}

	/*
		All our lights are created invisible to all ray types. Their membership
		status in the defaultLightSet will override this visibility.
		\sa NSIExportDefaultLightSet
	*/
	nsi.SetAttribute( attributes_handle.asChar(),
		(
			NSI::IntegerArg("visibility", 0),
			NSI::IntegerArg("visibility.priority", NSI_LIGHT_PRIORITY)
		) );

	/*
		Also set the mode on the UV generator.
	*/
	if( m_is_environment )
	{
		MString envuvcoord = MString(Handle()) + "*uvCoord";
		int mapping = getAttrInt( Object(), "mapping", 0, dummy );
		nsi.SetAttribute( envuvcoord.asChar(),
			NSI::IntegerArg("mapping", mapping) );
	}

	bool found = false;
	int et_value = getAttrInt( Object(), "_3delight_emissionType", 0, found );
	if( found && (et_value != 0 || m_live) )
	{
		nsi.SetAttribute( attributes_handle.asChar(),
			(
				NSI::IntegerArg("regularemission", et_value != 1),
				NSI::IntegerArg("quantizedemission", et_value != 2)
			) );
	}
}

void NSIExportLight::SetAttributesAtTime(	double i_time, bool i_no_motion )
{
	if( m_type == e_invalid )
	{
		return;
	}

	m_shader_delegate.SetAttributesAtTime( i_time, i_no_motion );
}

/**
*/
void NSIExportLight::Connect( const DelegateTable *i_dag_hash )
{
	if( m_type == e_invalid )
	{
		return;
	}

	m_shader_delegate.Connect( i_dag_hash );

	NSI::Context nsi( m_nsi );

	/* Connect attributes to this geo */
	nsi.Connect(
		AttributesHandle().asChar(), "",
		m_nsi_handle, "geometryattributes" );

	nsi.Connect(
		m_shader_delegate.Handle(), "",
		AttributesHandle().asChar(), "surfaceshader",
		NSI::IntegerArg("priority", 1) );

	ConnectLightFilters();

	if( m_type == e_spot || m_type == e_point )
	{
		/* Spot is rendered using a very tiny square */
		ConnectDefaultMesh();
		return;
	}

	if( m_type != e_area )
		return;

	/*
		Parent any mesh lights under this light (transform)
	*/
	MObjectArray meshes;
	LightGeo( meshes );
	for( int i=0; i<meshes.length(); i++ )
	{
		if( i_dag_hash )
		{
			MString mesh_handle = NSIHandle( meshes[i], m_live );
			if( !i_dag_hash->contains(mesh_handle) )
				continue;
		}

		ConnectOneMeshLight( meshes[i] );
	}

	if( meshes.length() == 0 )
	{
		ConnectDefaultMesh();
	}
}

void NSIExportLight::ConnectDefaultMesh()
{
	NSI::Context nsi( m_nsi );
	nsi.Connect(
		DefaultMeshHandle().asChar(), "",
		m_nsi_handle, "objects" );
}

void NSIExportLight::DisconnectDefaultMesh()
{
	NSI::Context nsi( m_nsi );
	nsi.Disconnect(
		DefaultMeshHandle().asChar(), "",
		m_nsi_handle, "objects" );
}

void NSIExportLight::AdjustDefaultMeshConnection()
{
	MObjectArray meshes;
	LightGeo( meshes );

	if( meshes.length() > 0 )
	{
		DisconnectDefaultMesh();
	}
	else
	{
		ConnectDefaultMesh();
	}
}

void NSIExportLight::ConnectOneMeshLight( const MObject& i_mesh )
{
	MFnDagNode dag( i_mesh );
	MString mesh_handle = NSIHandle(dag, m_live);

	NSI::Context nsi( m_nsi );

	nsi.Disconnect(
		mesh_handle.asChar(), "",
		NSI_ALL_NODES, "objects" );

	nsi.Connect(
		mesh_handle.asChar(), "",
		m_nsi_handle, "objects" );
}

void NSIExportLight::DisconnectOneMeshLight( const MObject& i_mesh )
{
	MFnDagNode meshDag( i_mesh );
	MString mesh_handle = NSIHandle( meshDag, m_live );

	MDagPath path = meshDag.dagPath();
	path.pop();
	MString parent_handle = path.fullPathName();

	if( parent_handle == "" )
		parent_handle = NSI_SCENE_ROOT;

	NSI::Context nsi( m_nsi );

	nsi.Disconnect(
		mesh_handle.asChar(), "",
		m_nsi_handle, "objects" );

	nsi.Connect(
		mesh_handle.asChar(), "",
		parent_handle.asChar(), "objects" );
}

bool NSIExportLight::RegisterCallbacks()
{
	MObject &object = (MObject&)Object();

	MCallbackId id = MNodeMessage::addAttributeChangedCallback(
		object,
		AttributeChangedCallback,
		(void*) this );

	AddCallbackId( id );

	m_shader_delegate.RegisterCallbacks();

	return true;
}

/**
	\brief Handle attribute changes on this DAG node.

	Note that attribute changes on shader's parameters (such as exposure, tint
	and intensity) are handled in NSIExportShader.
*/
void NSIExportLight::AttributeChangedCallback(
	MNodeMessage::AttributeMessage i_msg,
	MPlug& i_plug,
	MPlug& i_otherPlug,
	void* i_data )
{
	NSIExportLight* delegate = (NSIExportLight*)i_data;
	MString attrName = NSIExportUtilities::AttributeName( i_plug );

	NSI::Context nsi( delegate->m_nsi );

	bool need_synchronise = false;

	if( i_msg & MNodeMessage::kAttributeSet )
	{
		if( attrName == MString("_3delight_arealight_visibility") )
		{
			int camera_visibility;
			i_plug.getValue( camera_visibility );

			nsi.SetAttribute( delegate->AttributesHandle().asChar(),
				(
					NSI::IntegerArg("visibility.camera", camera_visibility ? 1 : 0),
					NSI::IntegerArg("visibility.camera.priority",
						CAMERA_VISIBILITY_PRIORITY)
				) );

			need_synchronise = true;
		}
		else if( attrName == "_3delight_angular_size" )
		{
			assert( delegate->m_type == NSIExportLight::e_distant );
			double angle;
			i_plug.getValue( angle );
			nsi.SetAttribute(
				delegate->Handle(), NSI::DoubleArg("angle", angle) );

			need_synchronise = true;
		}
		else if( attrName == "_3delight_default_arealight_shape" )
		{
			MObjectArray meshes;
			delegate->LightGeo( meshes );

			if( meshes.length() == 0 )
			{
				nsi.Delete( delegate->DefaultMeshHandle().asChar() );
				delegate->CreateDefaultAreaLightShape();
				delegate->ConnectDefaultMesh();

				need_synchronise = true;
			}
		}
		else if( attrName == "_3delight_radius" )
		{
			if( delegate->m_type == e_spot )
			{
				nsi.Delete( delegate->DefaultMeshHandle().asChar() );
				delegate->CreateDefaultAreaLightShape();
				delegate->ConnectDefaultMesh();

				need_synchronise = true;
			}
			else if( delegate->m_type == e_point )
			{
				delegate->SetAttributes();
				need_synchronise = true;				
			}
		}
		else if( attrName == "mapping" && delegate->m_is_environment )
		{
			int mapping = 0;
			i_plug.getValue( mapping );

			MString envuvcoord = MString(delegate->Handle()) + "*uvCoord";
			nsi.SetAttribute( envuvcoord.asChar(),
					NSI::IntegerArg("mapping", mapping) );

			need_synchronise = true;
		}
		else if( attrName == "_3delight_emissionType" )
		{
			delegate->SetAttributes();
			need_synchronise = true;
		}
	}
	else if( attrName == MString("_3delight_geometry") && i_msg & (
		MNodeMessage::kConnectionMade |
		MNodeMessage::kConnectionBroken ) )
	{
		if( i_msg & MNodeMessage::kConnectionMade )
		{
			delegate->DisconnectDefaultMesh();
			delegate->ConnectOneMeshLight( i_otherPlug.node() );
		}
		else if( i_msg & MNodeMessage::kConnectionBroken )
		{
			delegate->DisconnectOneMeshLight( i_otherPlug.node() );
			delegate->AdjustDefaultMeshConnection();
		}

		need_synchronise = true;
	}
	else if( attrName == MString("_3delight_filters") && i_msg & (
		MNodeMessage::kConnectionMade |
		MNodeMessage::kConnectionBroken ) )
	{
		delegate->FlushLightFilters();
		delegate->ConnectLightFilters();
		need_synchronise = true;
    }

	if( need_synchronise )
	{
		nsi.RenderControl( NSI::CStringPArg( "action", "synchronize") );
	}
}

bool NSIExportLight::HasFilters( MPlugArray& o_filters ) const
{
	MFnDependencyNode dep( Object() );

	MPlug plug = dep.findPlug( "_3delight_filters", true );
	for(unsigned i = 0; i < plug.numElements(); i++)
	{
		MPlug element_plug(plug.elementByPhysicalIndex(i));

		MPlugArray plugs;
		element_plug.connectedTo( plugs, true, false );

		for(unsigned j = 0; j < plugs.length(); j++)
			o_filters.append( plugs[j] );
	}

	return o_filters.length() > 0;
}

void NSIExportLight::ConnectLightFilters()
{
	using namespace NSIExportUtilities;

	m_last_filter_handle.clear();
	m_last_filter_param.clear();
	m_num_mix_filters = 0;

	MPlugArray filters;
	if( HasFilters( filters ) )
	{
		NSI::Context nsi( m_nsi );
		m_num_mix_filters = filters.length() - 1;

		for(unsigned i = 0; i < filters.length(); i++)
		{
			// Check that the connected node is supported
			MString filter_param =
				GetShaderParameterName( filters[i], eOutput );

			if( filter_param.length() > 0 )
			{
				/*
					Connect the relevant filter output to light filter
				*/
				MString filter_handle =
					NSIShaderHandle( filters[i].node(), m_live );

				if (m_last_filter_handle.length() == 0)
				{
					m_last_filter_handle = filter_handle;
					m_last_filter_param = filter_param;
				}
				else
				{
					/*
						Because OSL won't let us use an array for the 'filter'
						input and connect to its elements, we resort to using an
						intermediate node to merge multiple filters together.
					*/
					MString filter_mix_handle = m_nsi_handle;
					filter_mix_handle += "|filter_mix_";
					filter_mix_handle += i;
					filter_mix_handle += "|shader";

					nsi.Create( filter_mix_handle.asChar(), "shader" );
					MString shaderName =
						OSLUtils::GetFullpathname("filter_multiply");
					nsi.SetAttribute(
						filter_mix_handle.asChar(), NSI::StringArg(
							"shaderfilename", shaderName.asChar()));

					/* Connect previous filter and current filters to mix node. */
					nsi.Connect(
						m_last_filter_handle.asChar(), m_last_filter_param.asChar(),
						filter_mix_handle.asChar(), "filter1" );
					nsi.Connect(
						filter_handle.asChar(), filter_param.asChar(),
						filter_mix_handle.asChar(), "filter2" );

					m_last_filter_handle = filter_mix_handle;
					m_last_filter_param = filter_param;
				}
			}
		}

		/* Connect the last filter node to the light shader. */
		if (m_last_filter_handle.length() > 0 && m_last_filter_param.length() > 0)
		{
			nsi.Connect(
				m_last_filter_handle.asChar(), m_last_filter_param.asChar(),
				m_shader_delegate.Handle(), "filter" );
		}
	}
}

void NSIExportLight::FlushLightFilters()
{
	NSI::Context nsi( m_nsi );
	/* Disconnect the last filter node to the light shader. */
	if (m_last_filter_handle.length() > 0 && m_last_filter_param.length() > 0)
	{
		nsi.Disconnect(
			m_last_filter_handle.asChar(), m_last_filter_param.asChar(),
			m_shader_delegate.Handle(), "filter" );
	}
	/* Delete all existing shaders filter_multiply. */
	for(unsigned i = 1; i <= m_num_mix_filters; i++)
	{
		MString filter_mix_handle = m_nsi_handle;
		filter_mix_handle += "|filter_mix_";
		filter_mix_handle += i;
		filter_mix_handle += "|shader";

		nsi.Delete( filter_mix_handle.asChar() );
	}
}

/**
	\brief Returns the DAG node of the geo attached to this light source

	\return DAG node or "" if nothing connected
*/
MStatus NSIExportLight::LightGeo( MObjectArray &o_meshes ) const
{
	MFnDependencyNode light( Object());
	MPlug geoArray = light.findPlug( "_3delight_geometry", true );

	assert(geoArray.isArray());

	for( int i=0; i<geoArray.numElements(); i++ )
	{
		MPlugArray connectedTo;
		MPlug p = geoArray.elementByPhysicalIndex(i);
		p.connectedTo( connectedTo, true, false );

		for (int j = 0; j < connectedTo.length(); j++)
		{
			MStatus status;
			MFnDagNode dagNode(connectedTo[j].node(), &status);
			if (status == MS::kSuccess)
				o_meshes.append(connectedTo[j].node());
			else if (connectedTo[j].node().hasFn(MFn::kSet))
			{
				MFnSet set(connectedTo[j].node());
				MSelectionList members;
				set.getMembers(members, true);
				for (unsigned k = 0; k < members.length(); k++)
				{
					MObject set_node;
					status = members.getDependNode(k, set_node);
					if (status == MS::kSuccess)
						o_meshes.append(set_node);
				}
			}
		}
	}

	return MStatus::kSuccess;
}

MString NSIExportLight::DefaultMeshHandle() const
{
	return MString(m_nsi_handle) + "|defaultMesh";
}

/**
	\brief Creates the good shape for the area light.

	Most of the code taken from 3delight_for_katana/src/ExportGeo.cpp
*/
void NSIExportLight::CreateDefaultAreaLightShape( void )
{
	NSI::Context nsi(m_nsi);

	bool dummy;

	/* 0=square, 1=disk, 2=sphere, 3=cylinder */
	int shape =
		getAttrInt( Object(), "_3delight_default_arealight_shape", 0, dummy );

	MString geo_name = DefaultMeshHandle();

	if( m_type == e_point )
	{
		/* Create a very small sphere particle */
		nsi.Create( geo_name.asChar(), "particles" );

		NSI::ArgumentList args;
		float P[3] = { 0.0f, 0.0f, 0.0f };
		args.push( NSI::Argument::New( "P" )
			->SetType( NSITypePoint )
			->SetValuePointer( &P[0] ) );
		args.push( new NSI::FloatArg( "width", 1e-3f ) );

		nsi.SetAttribute( geo_name.asChar(), args );
	}
	else if( m_type == e_spot || shape == 0 )
	{
		float scale = 1.0f;

		if( m_type == e_spot )
		{
			scale = getAttrDouble( Object(), "_3delight_radius", 0.001f );
		}

		float P[] = {
			-scale, -scale, 0.0f,  -scale, scale, 0.0f,
			scale, scale, 0.0f,   scale, -scale, 0.0f };
		int nvertices[] = { 4 };

		NSI::ArgumentList mesh_attributes;
		mesh_attributes.Add(
			NSI::Argument::New( "P" )
			->SetType( NSITypePoint )
			->SetCount( 4 )
			->SetValuePointer( const_cast<float*>(P)) );

		mesh_attributes.Add(
			NSI::Argument::New( "nvertices" )
			->SetType( NSITypeInteger )
			->SetCount( 1 )
			->SetValuePointer( const_cast<int*>(nvertices)) );

		nsi.Create( geo_name.asChar(), "mesh" );
		nsi.SetAttribute( geo_name.asChar(), mesh_attributes );
	}
	else if( shape == 1  || shape == 2 )
	{
		nsi.Create( geo_name.asChar(), "particles" );

		NSI::ArgumentList args;
		float P[3] = { 0.0f, 0.0f, 0.0f };
		float N[3] = { 0.0f, 0.0f, -1.0f }; /* for disk only */

		args.push( NSI::Argument::New( "P" )
			->SetType( NSITypePoint )
			->SetValuePointer( &P[0] ) );
		args.push( new NSI::FloatArg( "width", 2.0f ) );

		if( shape == 1 )
		{
			args.push( NSI::Argument::New( "N" )
				->SetType( NSITypeNormal )
				->SetValuePointer( &N[0] ) );
		}

		nsi.SetAttribute( geo_name.asChar(), args );
	}
	else if( shape == 3 )
	{
		nsi.Create( geo_name.asChar(), "mesh" );

		/* The cylinder is 1 unit long (in X) and has a radius of 0.5. */
		std::vector<float> P;
		std::vector<int> indices, nvertices;
		const int kNumSteps = 18;
		for( int i = 0; i < kNumSteps; ++i )
		{
			float angle = (float(i) / float(kNumSteps)) * float(2.0 * M_PI);
			float z = 1.0f * cos( angle );
			float y = 1.0f * sin( angle );
			P.push_back( y ); P.push_back( 1.0f ); P.push_back( z );
			P.push_back( y ); P.push_back( -1.0f ); P.push_back( z );

			nvertices.push_back( 4 );
			indices.push_back( i * 2 );
			indices.push_back( i * 2 + 1 );
			indices.push_back( (i * 2 + 3) % (2 * kNumSteps) );
			indices.push_back( (i * 2 + 2) % (2 * kNumSteps) );
		}

		NSI::ArgumentList args;
		args.push( NSI::Argument::New( "nvertices" )
			->SetType( NSITypeInteger )
			->SetCount( nvertices.size() )
			->SetValuePointer( &nvertices[0] ) );
		args.push( NSI::Argument::New( "P" )
			->SetType( NSITypePoint )
			->SetCount( 2 * kNumSteps )
			->SetValuePointer( &P[0] ) );
		args.push( NSI::Argument::New( "P.indices" )
			->SetType( NSITypeInteger )
			->SetCount( 4 * kNumSteps )
			->SetValuePointer( &indices[0] ) );
		nsi.SetAttribute( geo_name.asChar(), args );
	}
}


