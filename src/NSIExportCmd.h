/*
  Copyright (c) The 3Delight Team.
  Copyright (c) soho vfx inc.
*/

#ifndef __DL_NSIRENDERCMD_H__
#define __DL_NSIRENDERCMD_H__

#include <maya/MPxCommand.h>
#include <maya/MObject.h>

#define NSIEXPORT_CMD_STR "NSIExport"

class NSIExport;

class NSIExportCmd : public MPxCommand
{

public:
	virtual MStatus doIt(const MArgList& i_args);

	virtual bool isUndoable() const;
	static void* creator();
	static MSyntax newSyntax();

	/**
		Register a typical exporter, not viewport render.
	*/
	static void RegisterExporter( NSIExport *, const MObject &i_pass );

	/**
		Check if the render pass is active.
	*/
	static bool RenderPassActive( const MObject &i_pass );

	/**
 		Stop a render.
	*/
	static void StopRender( const MObject &i_pass );

	/**
		Stop the all renders.
	*/
	static void StopAllRenders();

private:
	static NSIExport* m_NSIExport;
};

#endif
