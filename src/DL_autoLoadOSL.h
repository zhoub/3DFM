/******************************************************************************/
/*                                                                            */
/*    Copyright (c)The 3Delight Developers. 2016                              */
/*    All Rights Reserved.                                                    */
/*                                                                            */
/******************************************************************************/

#ifndef __DL_AUTOLOADOSL_H__
#define __DL_AUTOLOADOSL_H__

#include <maya/MPxNode.h>
#include <maya/MStringArray.h>

#if MAYA_API_VERSION >= 201800
#include <maya/MApiNamespace.h>
#else
 class MFnPlugin;
 class MObjectArray;
#endif

#include <3Delight/ShaderQuery.h>

class DL_OSLShadingNode : public MPxNode
{
public:
  static void* creator();
  static MStatus initialize();

  virtual void postConstructor();
  virtual MStatus compute( const MPlug&, MDataBlock& );

  static MStatus RegisterOSLShaders( MFnPlugin& i_plugin );

  static void CreateAttributesFromShaderParameters(
    DlShaderInfo *i_shader,
    MObject* i_object,
    MObjectArray* o_objects,
    MStringArray* o_objectNames );

  static bool FindAttribute(
    const MString &i_name,
    MObject &o_object,
    const MObjectArray &i_objects,
    const MStringArray &i_objectNames);

  static void DefineShaderNiceName(
    const MString& i_name,
    const MString& i_niceName );

  static bool ReloadShader( const MString& i_nodeTypeName );

private:
  static MString AETemplate(DlShaderInfo *i_shader);

  static void RegisterSingleOSLShader(
    MFnPlugin &i_plugin, bool i_isUserPath, const char* i_path);
};

#endif

