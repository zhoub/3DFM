#include "dlColorBlend.h"

#include <maya/MColor.h>
#include <maya/MFloatVector.h>
#include <maya/MFnNumericAttribute.h>
#include <maya/MFnEnumAttribute.h>

#include "DL_utils.h"

MObject dlColorBlend::m_blendMode;
MObject dlColorBlend::m_fg;
MObject dlColorBlend::m_bg;
MObject dlColorBlend::m_blend;

MObject dlColorBlend::m_outColor;

void* dlColorBlend::creator()
{
	return new dlColorBlend;
}

MStatus dlColorBlend::initialize()
{
	using namespace Utilities;

	MFnEnumAttribute enumFn;
	m_blendMode = enumFn.create( "blendMode", "blendMode", 0 );
	enumFn.setNiceNameOverride( "Blend Mode" );
	enumFn.addField( "Over", 0 );
	enumFn.addField( "Multiply", 1 );
	enumFn.addField( "Darken (Min)", 2 );
	enumFn.addField( "Color Burn", 3 );
	enumFn.addField( "Add", 4 );
	enumFn.addField( "Screen", 5 );
	enumFn.addField( "Lighten (Max)", 6 );
	enumFn.addField( "Color Dodge", 7 );
	enumFn.addField( "Overlay", 8 );
	enumFn.addField( "Soft Light", 9 );
	enumFn.addField( "Hard Light", 10 );
	enumFn.addField( "Add/Substract", 11 );
	enumFn.addField( "Substract", 12 );
	enumFn.addField( "Difference", 13 );
	enumFn.addField( "Divide", 14 );
	enumFn.addField( "Hue", 15 );
	enumFn.addField( "Saturation", 16 );
	enumFn.addField( "Color", 17 );
	enumFn.addField( "Luminosity", 18 );
	makeShaderInputAttribute( enumFn );
	addAttribute( m_blendMode );

	MFnNumericAttribute numFn;

	m_blend = numFn.create( "blend", "blend", MFnNumericData::kDouble, 1.0 );
	numFn.setDefault( 1.0 );
	numFn.setMin( 0.0 );
	numFn.setMax( 1.0 );
	numFn.setNiceNameOverride( "Blend Factor" );
	makeShaderInputAttribute( numFn );
	addAttribute( m_blend );

	m_fg = numFn.createColor( "foreground", "fg" );
	numFn.setDefault( 1.0, 1.0, 1.0 );
	numFn.setNiceNameOverride( "Foreground" );
	makeShaderInputAttribute( numFn );
	addAttribute( m_fg );

	m_bg = numFn.createColor( "background", "bg" );
	numFn.setDefault( 0.0, 0.0, 0.0 );
	numFn.setNiceNameOverride( "Background" );
	makeShaderInputAttribute( numFn );
	addAttribute( m_bg );

	m_outColor = numFn.createColor( "outColor", "oc" );
	makeOutputAttribute( numFn );
	addAttribute( m_outColor );

	attributeAffects( m_blendMode, m_outColor );
	attributeAffects( m_fg, m_outColor );
	attributeAffects( m_bg, m_outColor );
	attributeAffects( m_blend, m_outColor );

	return MStatus::kSuccess;
}

void dlColorBlend::postConstructor()
{
	setExistWithoutInConnections(true);
	setExistWithoutOutConnections(true);
}

MStatus dlColorBlend::compute( const MPlug& i_plug, MDataBlock& i_dataBlock )
{
	using namespace Utilities;

	if ((i_plug != m_outColor) && (i_plug.parent() != m_outColor))
	{
		return MS::kUnknownParameter;
	}

	/* 
		We can replicate some of the shader computation on outColor. One visible
		effect of this is the swatch, which would still be better computed by some
		other means (hardware shader or our own swatch rendering, if possible).

		I just did a few easy ones quickly. This can be completed when the shader
		is stable.

		Note that using MColor would give use a mean to convert to / from HSV
		using its get() method. Useful for the hue / sat / color / lum modes.
	*/

	MFloatVector& fg = i_dataBlock.inputValue( m_fg ).asFloatVector();
	MFloatVector& bg = i_dataBlock.inputValue( m_bg ).asFloatVector();
	double blend = i_dataBlock.inputValue( m_blend ).asDouble();
	int blendMode = i_dataBlock.inputValue( m_blendMode ).asInt();

	MFloatVector blendedColors;

	switch( blendMode )
	{
		// Over
		case 0:	blendedColors = fg; break;

		// Multiply
		case 1: blendedColors = bg; break;

		// Darken (Min)
		case 2: 
		{
			float fg_val = CIEluminance( fg );
			float bg_val = CIEluminance( bg );
			blendedColors = fg_val < bg_val ? fg : bg;
			break;
		}

		// Color Burn
		case 3: 
			//blended_colors =  1 - ( ( 1 - bg ) / max(fg, color(0.001)) );		
			break;

		// Add
		case 4:	blendedColors = bg + fg; break;

		// Screen
		case 5:
			//	blended_colors = screen_blend(fg,bg);
			break;

		// Lighten (Max)
		case 6:
		{
			float fg_val = CIEluminance(fg); 
			float bg_val = CIEluminance(bg); 
			blendedColors = (fg_val > bg_val) ? fg : bg;
			break;
		}

		// Color Dodge
		case 7:
			//		blended_colors = bg / max( 1 - fg, color(0.001) );
			break;

		// Overlay
		case 8:
			/*
			{
			float fg_val = CIEluminance(fg);
			blended_colors = (fg_val > 0.5) ? bg * fg
											: screen_blend(fg,bg);
			}*/
			break;

		// Soft Light
		case 9:
			/*
			{
			blended_colors = (1 - fg) * bg * fg 
								+ fg * screen_blend(fg,bg);
			}
			*/
			break;

		// Hard Light
		case 10:
		/*
			{
			float fg_val = CIEluminance(fg);
			blended_colors = (fg_val < 0.5) ? bg * fg
											: screen_blend(fg,bg);		
			}
		*/
		break;

		// Add Sub
		case 11: 
			blendedColors = (CIEluminance(fg)>0.5) ? bg + fg : bg - fg;
			break;
		
		// Substract
		case 12: blendedColors = bg - fg; break;

		// Difference
		case 13: 
			blendedColors[0] = fabs( bg[0] - fg[0] );
			blendedColors[1] = fabs( bg[1] - fg[1] );
			blendedColors[2] = fabs( bg[2] - fg[2] );
			break;

		// Divide
		case 14:
			blendedColors[0] = bg[0] / fmax( fg[0], 0.001f );
			blendedColors[1] = bg[1] / fmax( fg[1], 0.001f );
			blendedColors[2] = bg[2] / fmax( fg[2], 0.001f );
			break;

		// Hue
		case 15:
		/*
			{
			color bg_hsv = transformc("rgb", "hsv", bg);
			color fg_hsv = transformc("rgb", "hsv", fg);
			blended_colors = transformc("hsv", "rgb", color(fg_hsv[0],bg_hsv[1],bg_hsv[2]));
			}
		*/
		break;

		// Saturation
		case 16:
		/*
			{
			color bg_hsv = transformc("rgb", "hsv", bg);
			color fg_hsv = transformc("rgb", "hsv", fg);
			blended_colors = transformc("hsv", "rgb", color(bg_hsv[0],fg_hsv[1],bg_hsv[2]));
			}
		*/
		break;

		// Color
		case 17:
			/*
			{
			color bg_hsv = transformc("rgb", "hsv", bg);
			color fg_hsv = transformc("rgb", "hsv", fg);
			blended_colors = transformc("hsv", "rgb", color(fg_hsv[0],fg_hsv[1],bg_hsv[2]));
			}
			*/
			break;

		// Luminosity
		case 18:
		/*
			{
			color bg_hsv = transformc("rgb", "hsv", bg);
			color fg_hsv = transformc("rgb", "hsv", fg);
			blended_colors = transformc("hsv", "rgb", color(bg_hsv[0],bg[1],fg_hsv[2]));
			}
		*/
		break;
	}

	MDataHandle outColorHandle = i_dataBlock.outputValue( m_outColor );
	MFloatVector& outColor = outColorHandle.asFloatVector();

	outColor[0] = bg[0] * ( 1 - blend ) + blendedColors[0] * blend;
	outColor[1] = bg[1] * ( 1 - blend ) + blendedColors[1] * blend;
	outColor[2] = bg[2] * ( 1 - blend ) + blendedColors[2] * blend;

	outColorHandle.setClean();
	return MStatus::kSuccess;
}
