/*
  Copyright (c) soho vfx inc.
  Copyright (c) The 3Delight Team.
*/

global float $DRP_crop_window_override[];

global proc
DRV_popupMayaRenderViewWindow( string $render_pass )
{
  if( ! `control -exists renderView` ||
      `control -q -isObscured renderView` ||
      `panel -q -tearOff renderView` )
  {
    RenderViewWindow;
  }
}

proc
mayaRenderWith3Delight()
{
  // We could call DRG_GUIrender directly but this would involve figuring out
  // which layer we have to render. Better leave this to Maya.

  string $curr_renderer_attr = "defaultRenderGlobals.currentRenderer";
  string $curr_renderer = getAttr($curr_renderer_attr);
  
  setAttr -type "string" $curr_renderer_attr "_3delight";
  RenderIntoNewWindow;
  setAttr -type "string" $curr_renderer_attr $curr_renderer;
}

global proc
DRV_renderRegion( string $render_pass )
{
  global float $DRP_crop_window_override[];

  string $globals[] = `ls -renderGlobals`;

  int $left = `getAttr( $globals[0] + ".left" )`;
  int $right = `getAttr( $globals[0] + ".rght" )`;
  int $bottom = `getAttr( $globals[0] + ".bot" )`;
  int $top = `getAttr( $globals[0] + ".top" )`;

  // The render pass node could have been deleted either by the user or because
  // the rendering was invoked with the default render pass node from the 
  // 3delight render globals
  int $render_pass_exists = `objExists $render_pass`;
  
  float $x_res;
  float $y_res;
  
  if ($render_pass_exists)
  {
    $x_res = getAttr( $render_pass + ".resolutionX" );
    $y_res = getAttr( $render_pass + ".resolutionY" );
  }
  else
  {
    string $resolution_node[];
    $resolution_node = `listConnections -type "resolution" "defaultRenderGlobals"`;

    $x_res = getAttr($resolution_node[0] + ".width");
    $y_res = getAttr($resolution_node[0] + ".height");
  }

  $DRP_crop_window_override =
  {
    $left / $x_res, 1.0 - ($top / $y_res),
    $right / $x_res, 1.0 - ($bottom / $y_res)
  };

  if ($render_pass_exists)
  {
    if (`nodeType $render_pass` == "delightRenderPass" )
    {
      delightRenderFromGUI( $render_pass );
    }
    else
    {
      DL_render($render_pass, 1);
    }
  }
  else
  {
    // This is likely a leftover case from the era we had an implicit default
    // render pass. Porbably never called nowadays.
    //
    mayaRenderWith3Delight();
  }

  clear($DRP_crop_window_override);
}
