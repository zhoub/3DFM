/* vim: set softtabstop=2 expandtab shiftwidth=2: */
/*
  Copyright (c) The 3Delight Team.
  Copyright (c) soho vfx inc.
*/

/*
  Standard Maya file. This is the name of the procedure we should call in our
  template procedure, but it generates frmae layouts that I don't want. So
  we call global procedures defined in this file to get the standard Maya ramp
  gadgets without the frame layout bloat.
*/
source "AEaddRampControl.mel";

proc AE_DOVS_densityRemapNew(
  string $dr_enable_plug,
  string $dr_range_plug,
  string $dr_curve_plug)
{
  string $name = AE_controlNameFromPlug( $dr_enable_plug );
  attrControlGrp
    -label "Enable"
    -hideMapButton true
    -attribute $dr_enable_plug
    $name;

  /*
    Put the rest of the controls in a layout so we can toggle its enable flag
    according to the value of $dr_enable_plug and all child gadgets will
    adjust accordingly.
  */
  columnLayout ($name + "DependentControlsLayout");

  $name = AE_controlNameFromPlug( $dr_range_plug );
  attrControlGrp
    -label "Range"
    -hideMapButton true
    -attribute $dr_range_plug
    $name;

  text -label "" -h 14;

  AEmakeRampControlInteractiveNew $dr_curve_plug;
  setParent ..;  // apparently required by makeRampControl

  setParent ..;  // from columnLayout
}

proc AE_DOVS_densityRemapReplace(
  string $dr_enable_plug,
  string $dr_range_plug,
  string $dr_curve_plug)
{
  string $name = AE_controlNameFromPlug( $dr_enable_plug );
  attrControlGrp
    -edit
    -attribute $dr_enable_plug
    $name;

  string $layout = $name + "DependentControlsLayout";
  $layout = `columnLayout -q -fullPathName $layout`;

  $name = AE_controlNameFromPlug( $dr_range_plug );
  attrControlGrp
    -edit
    -attribute $dr_range_plug
    $name;

  AEmakeRampControlInteractiveReplace $dr_curve_plug;
  setParent ..;  // apparently required by makeRampControl

  string $cb = "AE_DOVS_enableChangedCB " +
    "\"" + $dr_enable_plug + "\" " +
    "\"" + $layout + "\"";

  scriptJob
    -replacePrevious
    -parent $layout
    -attributeChange $dr_enable_plug
    $cb;

  eval( $cb );
}

global proc AE_DOVS_densityColorRampNew(
  string $dcr_enable_plug,
  string $dcr_range_plug,
  string $dcr_curve_plug)
{
  DL_separatorWithLabel( "densityColorRamp_sep", "Density Color Ramp", 70 );
  // Reuse density ramp code, it has exactly what we need.
  AE_DOVS_densityRemapNew(
    $dcr_enable_plug,
    $dcr_range_plug,
    $dcr_curve_plug);
  AE_DOVS_densityColorRampReplace(
    $dcr_enable_plug,
    $dcr_range_plug,
    $dcr_curve_plug);
}

global proc AE_DOVS_densityColorRampReplace(
  string $dcr_enable_plug,
  string $dcr_range_plug,
  string $dcr_curve_plug)
{
  // Reuse density ramp code, it has exactly what we need.
  AE_DOVS_densityRemapReplace(
    $dcr_enable_plug,
    $dcr_range_plug,
    $dcr_curve_plug);
}

global proc AE_DOVS_scatteringNew(
  string $scattering_density_plug,
  string $scattering_color_plug,
  string $scattering_anisotropy_plug,
  string $multiple_scattering_plug,
  string $dr_enable_plug,
  string $dr_range_plug,
  string $dr_curve_plug)
{
  DL_customSeparatorWithLabel(
    "scattering_sep",
    "Scattering", 55,
    22,
    0, 8,
    0, 0,
    0, 100 );

  string $name = AE_controlNameFromPlug( $scattering_density_plug );
  attrControlGrp
    -label "Density"
    -hideMapButton true
    -attribute $scattering_density_plug
    $name;

  $name = AE_controlNameFromPlug( $scattering_color_plug );
  attrColorSliderGrp
    -label "Color"
    -attribute $scattering_color_plug
    $name;

  $name = AE_controlNameFromPlug( $scattering_anisotropy_plug );
  attrControlGrp
    -label "Anisotropy"
    -hideMapButton true
    -attribute $scattering_anisotropy_plug
    $name;

  $name = AE_controlNameFromPlug( $multiple_scattering_plug );
  attrControlGrp
    -label "Multiple Scattering"
    -hideMapButton true
    -attribute $multiple_scattering_plug
    $name;

  DL_separatorWithLabel( "densityRemap_sep", "Density Ramp", 48 );
  AE_DOVS_densityRemapNew(
    $dr_enable_plug,
    $dr_range_plug,
    $dr_curve_plug);

  AE_DOVS_scatteringReplace(
    $scattering_density_plug,
    $scattering_color_plug,
    $scattering_anisotropy_plug,
    $multiple_scattering_plug,
    $dr_enable_plug,
    $dr_range_plug,
    $dr_curve_plug);
}

global proc AE_DOVS_scatteringReplace(
  string $scattering_density_plug,
  string $scattering_color_plug,
  string $scattering_anisotropy_plug,
  string $multiple_scattering_plug,
  string $dr_enable_plug,
  string $dr_range_plug,
  string $dr_curve_plug)
{
  string $name = AE_controlNameFromPlug( $scattering_density_plug );
  attrControlGrp
    -edit
    -attribute $scattering_density_plug
    $name;

  $name = AE_controlNameFromPlug( $scattering_color_plug );
  attrColorSliderGrp
    -edit
    -attribute $scattering_color_plug
    $name;

  $name = AE_controlNameFromPlug( $scattering_anisotropy_plug );
  attrControlGrp
    -edit
    -attribute $scattering_anisotropy_plug
    $name;

  $name = AE_controlNameFromPlug( $multiple_scattering_plug );
  attrControlGrp
    -edit
    -attribute $multiple_scattering_plug
    $name;

  AE_DOVS_densityRemapReplace(
    $dr_enable_plug,
    $dr_range_plug,
    $dr_curve_plug);
}

global proc AE_DOVS_transparencyNew(string $color_plug, string $scale_plug)
{
  DL_customSeparatorWithLabel(
    "transparency_sep",
    "Transparency", 74,
    40,
    0, 0,
    0, 0, 0, 100 );

  string $name = AE_controlNameFromPlug( $color_plug );
  attrColorSliderGrp
    -label "Color"
    -attribute $color_plug
    $name;

  $name = AE_controlNameFromPlug( $scale_plug );
  attrControlGrp
    -label "Scale"
    -hideMapButton true
    -attribute $scale_plug
    $name;

  AE_DOVS_transparencyReplace( $color_plug, $scale_plug );
}

global proc AE_DOVS_transparencyReplace(string $color_plug, string $scale_plug)
{
  string $name = AE_controlNameFromPlug( $color_plug );
  attrColorSliderGrp
    -edit
    -attribute $color_plug
    $name;

  $name = AE_controlNameFromPlug( $scale_plug );
  attrControlGrp
    -edit
    -attribute $scale_plug
    $name;
}

proc AE_DOVS_emissionIntensityNew(
  string $scale_plug,
  string $grid_enable_plug,
  string $range_plug,
  string $curve_plug )
{
  string $name = AE_controlNameFromPlug( $scale_plug );
  attrControlGrp
    -label "Scale"
    -hideMapButton true
    -attribute $scale_plug
    $name;

  DL_separatorWithLabel( "emissionIntensity_sep", "Intensity", 48 );

  $name = AE_controlNameFromPlug( $grid_enable_plug );
  attrControlGrp
    -label "Use Grid"
    -hideMapButton true
    -attribute $grid_enable_plug
    $name;

  /*
    Put the rest of the controls in a layout so we can toggle its enable flag
    according to the value of $grid_enable_plug and all child gadgets will
    adjust accordingly.
  */
  columnLayout ($name + "DependentControlsLayout");

  $name = AE_controlNameFromPlug( $range_plug );
  attrControlGrp
    -label "Range"
    -hideMapButton true
    -attribute $range_plug
    $name;

  text -label "" -h 14;

  AEmakeRampControlInteractiveNew $curve_plug;
  setParent ..;  // apparently required by makeRampControl

  setParent ..;  // from columnLayout
}

proc AE_DOVS_emissionIntensityReplace(
  string $scale_plug,
  string $grid_enable_plug,
  string $range_plug,
  string $curve_plug )
{
  string $name = AE_controlNameFromPlug( $scale_plug );
  attrControlGrp
    -edit
    -attribute $scale_plug
    $name;

  $name = AE_controlNameFromPlug( $grid_enable_plug );
  attrControlGrp
    -edit
    -attribute $grid_enable_plug
    $name;

  string $layout = $name + "DependentControlsLayout";
  $layout = `columnLayout -q -fullPathName $layout`;

  $name = AE_controlNameFromPlug( $range_plug );
  attrControlGrp
    -edit
    -attribute $range_plug
    $name;

  AEmakeRampControlInteractiveReplace $curve_plug;
  setParent ..;  // apparently required by makeRampControl

  string $cb = "AE_DOVS_enableChangedCB " +
    "\"" + $grid_enable_plug + "\" " +
    "\"" + $layout + "\"";

  scriptJob
    -replacePrevious
    -parent $layout
    -attributeChange $grid_enable_plug
    $cb;

  eval( $cb );
}

global proc AE_DOVS_enableChangedCB( string $plug, string $layout )
{
  layout -edit -enable ( getAttr( $plug ) ) $layout;
}

proc AE_DOVS_blackbodyNew(
  string $intensity_plug,
  string $mode_plug,
  string $kelvin_plug,
  string $tint_plug,
  string $range_plug,
  string $curve_plug)
{
  DL_separatorWithLabel( "blackbody_sep", "Blackbody", 56 );

  string $name = AE_controlNameFromPlug( $intensity_plug );
  attrControlGrp
    -label "Intensity"
    -hideMapButton true
    -attribute $intensity_plug
    $name;

  $name = AE_controlNameFromPlug( $mode_plug );
  optionMenuGrp -label "Mode" $name;
    menuItem -label "Physically Correct" -data 0;
    menuItem -label "Normalized" -data 1;
    menuItem -label "Artistic" -data 2;
    setParent -menu ..;

  $name = AE_controlNameFromPlug( $kelvin_plug );
  attrControlGrp
    -label "Kelvin"
    -hideMapButton true
    -attribute $kelvin_plug
    $name;

  $name = AE_controlNameFromPlug( $tint_plug );
  attrColorSliderGrp
    -label "Tint"
    -showButton false
    -attribute $tint_plug
    $name;

  $name = AE_controlNameFromPlug( $range_plug );
  attrControlGrp
    -label "Range"
    -hideMapButton true
    -attribute $range_plug
    $name;

  text -label "" -h 14;

  AEmakeRampControlInteractiveNew $curve_plug;
  setParent ..;  // apparently required by makeRampControl
}

proc AE_DOVS_blackbodyReplace(
  string $intensity_plug,
  string $mode_plug,
  string $kelvin_plug,
  string $tint_plug,
  string $range_plug,
  string $curve_plug)
{
  string $name = AE_controlNameFromPlug( $intensity_plug );
  attrControlGrp
    -edit
    -attribute $intensity_plug
    $name;

  $name = AE_controlNameFromPlug( $mode_plug );
  connectControl -index 2 $name $mode_plug;

  attrControlGrp
    -edit
    -attribute $intensity_plug
    $name;

  $name = AE_controlNameFromPlug( $kelvin_plug );
  attrControlGrp
    -edit
    -attribute $kelvin_plug
    $name;

  $name = AE_controlNameFromPlug( $tint_plug );
  attrColorSliderGrp
    -edit
    -attribute $tint_plug
    $name;

  $name = AE_controlNameFromPlug( $range_plug );
  attrControlGrp
    -edit
    -attribute $range_plug
    $name;

  AEmakeRampControlInteractiveReplace $curve_plug;
}

proc AE_DOVS_emissionRampNew(
  string $intensity_plug,
  string $tint_plug,
  string $range_plug,
  string $curve_plug)
{
  DL_separatorWithLabel( "ramp_sep", "Ramp", 32 );

  string $name = AE_controlNameFromPlug( $intensity_plug );
  attrControlGrp
    -label "Intensity"
    -hideMapButton true
    -attribute $intensity_plug
    $name;

  $name = AE_controlNameFromPlug( $tint_plug );
  attrColorSliderGrp
    -label "Tint"
    -showButton false
    -attribute $tint_plug
    $name;

  $name = AE_controlNameFromPlug( $range_plug );
  attrControlGrp
    -label "Range"
    -hideMapButton true
    -attribute $range_plug
    $name;

  text -label "" -h 14;

  AEmakeRampControlInteractiveNew $curve_plug;
  setParent ..;  // apparently required by makeRampControl
}

proc AE_DOVS_emissionRampReplace(
  string $intensity_plug,
  string $tint_plug,
  string $range_plug,
  string $curve_plug)
{
  string $name = AE_controlNameFromPlug( $intensity_plug );
  attrControlGrp
    -edit
    -attribute $intensity_plug
    $name;

  $name = AE_controlNameFromPlug( $tint_plug );
  attrColorSliderGrp
    -edit
    -attribute $tint_plug
    $name;

  $name = AE_controlNameFromPlug( $range_plug );
  attrControlGrp
    -edit
    -attribute $range_plug
    $name;

  AEmakeRampControlInteractiveReplace $curve_plug;
}

global proc AE_DOVS_emissionNew(
  string $int_scale_plug,
  string $int_grid_enable_plug,
  string $int_range_plug,
  string $int_curve_plug,

  string $bb_intensity_plug,
  string $bb_mode_plug,
  string $bb_kelvin_plug,
  string $bb_tint_plug,
  string $bb_range_plug,
  string $bb_curve_plug,

  string $ramp_intensity_plug,
  string $ramp_tint_plug,
  string $ramp_range_plug,
  string $ramp_curve_plug )
{
  DL_customSeparatorWithLabel(
    "emission_sep",
    "Emission", 48,
    40,
    0, 0,
    0, 0,
    0, 100 );

  AE_DOVS_emissionIntensityNew(
    $int_scale_plug,
    $int_grid_enable_plug,
    $int_range_plug,
    $int_curve_plug );

  AE_DOVS_blackbodyNew(
    $bb_intensity_plug,
    $bb_mode_plug,
    $bb_kelvin_plug,
    $bb_tint_plug,
    $bb_range_plug,
    $bb_curve_plug );

  AE_DOVS_emissionRampNew(
    $ramp_intensity_plug,
    $ramp_tint_plug,
    $ramp_range_plug,
    $ramp_curve_plug );

  AE_DOVS_emissionReplace(
    $int_scale_plug,
    $int_grid_enable_plug,
    $int_range_plug,
    $int_curve_plug,
    $bb_intensity_plug,
    $bb_mode_plug,
    $bb_kelvin_plug,
    $bb_tint_plug,
    $bb_range_plug,
    $bb_curve_plug,
    $ramp_intensity_plug,
    $ramp_tint_plug,
    $ramp_range_plug,
    $ramp_curve_plug );
}

global proc AE_DOVS_emissionReplace(
  string $int_scale_plug,
  string $int_grid_enable_plug,
  string $int_range_plug,
  string $int_curve_plug,

  string $bb_intensity_plug,
  string $bb_mode_plug,
  string $bb_kelvin_plug,
  string $bb_tint_plug,
  string $bb_range_plug,
  string $bb_curve_plug,

  string $ramp_intensity_plug,
  string $ramp_tint_plug,
  string $ramp_range_plug,
  string $ramp_curve_plug )
{
  AE_DOVS_emissionIntensityReplace(
    $int_scale_plug,
    $int_grid_enable_plug,
    $int_range_plug,
    $int_curve_plug );

  AE_DOVS_blackbodyReplace(
    $bb_intensity_plug,
    $bb_mode_plug,
    $bb_kelvin_plug,
    $bb_tint_plug,
    $bb_range_plug,
    $bb_curve_plug );

  AE_DOVS_emissionRampReplace(
    $ramp_intensity_plug,
    $ramp_tint_plug,
    $ramp_range_plug,
    $ramp_curve_plug );
}

global proc AEdlOpenVDBShaderTemplate( string $node )
{
  editorTemplate -beginScrollLayout;

  editorTemplate -callCustom
    "AE_DOVS_scatteringNew"
    "AE_DOVS_scatteringReplace"
    "scatteringDensity"
    "scatteringColor"
    "scatteringAnisotropy"
    "multipleScatteringIntensity"
    "densityRemapEnable"
    "densityRemapRange"
    "densityRemapCurve";
  // Hide the old checkbox multiple scattering toggle.
  editorTemplate -suppress multipleScattering;

  AE_addSeparatorWithLabel( "colorcorretcSep", "Scattering Color Correction", 90 );
  editorTemplate -label "Gamma" -addDynamicControl "scatteringColorCorrectGamma";
  editorTemplate -label "Hue Shit" -addDynamicControl "scatteringColorCorrectHueShift";
  editorTemplate -label "Saturation" -addDynamicControl "scatteringColorCorrectSaturation";
  editorTemplate -label "Vibrance" -addDynamicControl "scatteringColorCorrectVibrance";
  editorTemplate -label "Contrast" -addDynamicControl "scatteringColorCorrectContrast";
  editorTemplate -label "ContrastPivot" -addDynamicControl "scatteringColorCorrectContrastPivot";
  editorTemplate -label "Gain" -addDynamicControl "scatteringColorCorrectGain";
  editorTemplate -label "Offset" -addDynamicControl "scatteringColorCorrectOffset";

  editorTemplate -callCustom
    "AE_DOVS_densityColorRampNew"
    "AE_DOVS_densityColorRampReplace"
    "densityColorRampEnable"
    "densityColorRampRange"
    "densityColorRampCurve";

  editorTemplate -callCustom
    "AE_DOVS_transparencyNew"
    "AE_DOVS_transparencyReplace"
    "transparencyColor"
    "transparencyScale";
  editorTemplate -label "Shadow Density" -addDynamicControl "shadowDensity";
  editorTemplate -label "Shadow Density Tint" -addDynamicControl "shadowDensityTint";

  editorTemplate -callCustom
    "AE_DOVS_emissionNew"
    "AE_DOVS_emissionReplace"
    "emissionIntensityScale"
    "emissionIntensityGridEnable"
    "emissionIntensityRange"
    "emissionIntensityCurve"
    "blackbodyIntensity"
    "blackbodyMode"
    "blackbodyKelvin"
    "blackbodyTint"
    "blackbodyRange"
    "blackbodyTemperatureCurve"
    "emissionRampIntensity"
    "emissionRampTint"
    "emissionRampRange"
    "emissionRampColorCurve";

  AE_addSeparatorWithLabel( "emissionGridSep", "Grid", 80 );
  editorTemplate -label "Intensity" -addDynamicControl "emissionGridIntensity";
  editorTemplate -label "Tint" -addDynamicControl "emissionGridTint";

  AE_addFullLengthSeparatorWithLabel( "specularSep", "Specular", 45, 20, 0 );
  editorTemplate -label "Intensity"  -addDynamicControl "specularIntensity";
  editorTemplate -label "Roughness"  -addDynamicControl "specularRoughness";
  editorTemplate -label "Gradient Smoothing"  -addDynamicControl "specularGradientSmoothing";

  AE_addSpacer( 12 );
  editorTemplate -addExtraControls -extraControlsLabel "";

  editorTemplate -endScrollLayout;

  editorTemplate -suppress incandescence;
  editorTemplate -suppress caching;
  editorTemplate -suppress frozen;
  editorTemplate -suppress nodeState;
}
