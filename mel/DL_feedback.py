import json
import maya.mel
import maya.cmds

def _GetMELStringArray(a):
    if len(a) == 0:
        return '{}'
    return '{"' + '","'.join(a) + '"}'

def _GetMELFloatArray(a):
    return '{' + ','.join(['{0:f}'.format(v) for v in a]) + '}'

def _GetMELColorArray(a):
    # Just flatten the nested list.
    return _GetMELFloatArray([comp for color in a for comp in color])

def _StartRender(m):
    live = m.get('live', False)
    if live:
        maya.mel.eval('NSI_liveRenderActiveSettings()')
    else:
        maya.mel.eval('NSI_interactiveRenderActiveSettings()')

def _UpdateCrop(m):
    usecrop = m.get('usecrop', False)
    crop = m.get('cropregion', None)
    if not crop or len(crop) < 4:
        maya.cmds.warning('Bad crop region in feedback')
        return
    maya.mel.eval(
        'RS_updateCrop({0:d}, {1[0]:f}, {1[1]:f}, {1[2]:f}, {1[3]:f})'.format(
            usecrop, crop))

def _SelectLayer(m):
    layer = m.get('layer', {})
    attributes = layer.get('attributes', [])
    if len(attributes) == 0:
        # This happens with light groups. It's "normal".
        return
    maya.mel.eval(
        'DIF_selectLayer({0})'.format(_GetMELStringArray(attributes)))

def _ScaleLayerIntensity(m):
    layer = m.get('layer', {})
    factor = m.get('scale factor', None)
    attributes = layer.get('attributes', [])
    values = layer.get('values', [])
    if not factor or len(attributes) != len(values) or len(values) == 0:
        maya.cmds.warning('Bad parameters in scale layer feedback')
        return
    maya.mel.eval(
        'DIF_scaleLayerIntensity({0}, {1}, {2:f})'.format(
            _GetMELStringArray(attributes),
            _GetMELFloatArray(values),
            factor))

def _UpdateLayerFilter(m):
    layer = m.get('layer', {})
    factor = m.get('color multiplier', None)
    attributes = layer.get('color_attributes', [])
    values = layer.get('color_values', [])
    if not factor or len(attributes) != len(values) or len(values) == 0:
        maya.cmds.warning('Bad parameters in layer filter feedback')
        return
    maya.mel.eval(
        'DIF_multiplyLayerColor({0}, {1}, {2})'.format(
            _GetMELStringArray(attributes),
            _GetMELColorArray(values),
            _GetMELFloatArray(factor)))

def HandleFeedback(message):
    try:
        m = json.loads(message)
    except json.JSONDecodeError:
        print('Invalid feedback message: ' + message)

    op = m.get('operation', None)
    if not op:
        maya.cmds.warning('Missing feedback operation')
        return

    if op == 'start render':
        _UpdateCrop(m)
        _StartRender(m)
    elif op == 'update crop':
        _UpdateCrop(m)
    elif op == 'select layer':
        _SelectLayer(m)
    elif op == 'scale layer intensity':
        _ScaleLayerIntensity(m)
    elif op == 'update layer filter':
        _UpdateLayerFilter(m)

# vim: set softtabstop=4 expandtab shiftwidth=4:
