/*
   Copyright (c) soho vfx inc.
   Copyright (c) The 3Delight Team.
*/

/**
  \brief Returns the next available index for an AOV assignment.

  \param $aovGroup
    The dlAOVGroup node to inspect.

  \param $type
    The type of AOV to find the index for. Either "color" or "float".
*/
global proc int DAOVG_GetNextAOVIndex(string $aovGroup, string $type)
{
  // This is similar to maya's getNextFreeMultiIndex(), but adapted for this
  // node's multi compound attribute structure.
  //
  int $i;
  for($i = 0; $i < 10000000; $i++)
  {
    string $attr = $aovGroup + "." + DAOVG_GetAOVNodeAttrName($type, $i);
    string $connection = `connectionInfo -sourceFromDestination $attr`;

    if(size($connection) == 0)
    {
      return $i;
    }
  }

  return 0;
}

/**
  \brief Returns the indexed color or float AOV attribute name.

  This is the name of an attribute meant to be connected to a dlAOV* node.

  \param $type
    The AOV type. Either "color" or "float".

  \param $i
    The AOV index.
*/
global proc string DAOVG_GetAOVNodeAttrName(string $type, int $i)
{
  if($type == "color")
  {
    return "colorAOVs[" + $i + "].colorAOV";
  }

  return "floatAOVs[" + $i + "].floatAOV";
}

/**
  \brief Returns the indexed color or float AOV value attribute name.

  This is the name of an attribute meant to be connected to some shading node
  that defines the related AOV content.

  \param $type
    The AOV type. Either "color" or "float".

  \param $i
    The AOV index.
*/
global proc string DAOVG_GetAOVValueAttrName(string $type, int $i)
{
  if($type == "color")
  {
    return "colorAOVs[" + $i + "].colorValue";
  }

  return "floatAOVs[" + $i + "].floatValue";
}

/**
  \brief Connects a AOV node to the AOV group.

  The next available index in the color or float multi attribute of the AOV
  group will be used.

  \param $aovGroup
    The dlAOVGroup to connect the AOV to.

  \param $aovNode
    The dlAOV* node to connect.
*/
global proc DAOVG_ConnectAOV(string $aovGroup, string $aovNode)
{
  string $type = "color";
  if(`nodeType $aovNode` == "dlAOVFloat")
  {
    $type = "float";
  }

  int $i = DAOVG_GetNextAOVIndex($aovGroup, $type);
  string $msgAttr = DAOVG_GetAOVNodeAttrName($type, $i);

  connectAttr ($aovNode + ".message") ($aovGroup + "." + $msgAttr);
}

/**
  \brief Disconnects a AOV node from the AOV group.

  The whole compound element will be removed, so this will both disconnect the
  AOV node and delete or disconnect the input of the related AOV value
  attribute.

  \param $aovGroup
    The dlAOVGroup to disconnect the AOV from.

  \param $aovNode
    The dlAOV* node to disconnect.
*/
global proc DAOVG_DisconnectAOV(string $aovGroup, string $aovNode)
{
  string $connections[] = `listConnections
    -source true
    -destination false
    -connections true
    $aovGroup`;

  int $i;
  for($i = 0; $i < size($connections); $i += 2)
  {
    if($connections[$i + 1] == $aovNode)
    {
      string $tokens[];
      int $numTokens = tokenize($connections[$i], ".", $tokens);

      if($numTokens > 1)
      {
        removeMultiInstance -break true ($tokens[0] + "." + $tokens[1]);
      }

      break;
    }
  }
}

/**
  \brief Returns true if the specified aov node is connected to a given aov
  group.

  \param $aovGroup
    The dlAOVGroup to interrogate

  \param $aovNode
    The dlAOV* node to look for in the $aovGroup connections.
*/
global proc int DAOVG_IsAOVConnected(string $aovGroup, string $aovNode)
{
  string $connections[] =
    `listConnections -source true -destination false $aovGroup`;

  return stringArrayContains($aovNode, $connections);
}
