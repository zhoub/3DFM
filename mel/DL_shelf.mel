/* vim: set softtabstop=2 expandtab shiftwidth=2: */
/*
	Copyright (c) 2012 soho vfx inc.
	Copyright (c) 2012 The 3Delight Team.
*/

global proc string
DSH_getCurrentVersionShelfName()
{
  return "_3DelightV4";
}

global proc DSH_launchRender()
{
  NSI_interactiveRenderActiveSettings();
}

global proc DSH_launchIPR()
{
  NSI_liveRenderActiveSettings();
}

global proc DSH_renderingStartedOrEnded(
  string $abort_button,
  string $menu_name,
  string $postmenu_command )
{
  string $ongoing_render_settings[] = RS_getOngoingRenderings();
  int $num_ongoing_renderings = size( $ongoing_render_settings );

  string $menus[] = `shelfButton -q -popupMenuArray $abort_button`;
  for($curr_menu in $menus)
    deleteUI -menu $curr_menu;

  if( $num_ongoing_renderings == 0 )
  {
    shelfButton -edit -enable false $abort_button;
  }
  else if( $num_ongoing_renderings == 1 )
  {
    shelfButton
      -edit
      -enable true
      -annotation ( "Abort Rendering of " + $ongoing_render_settings[0] )
      -command ( "NSI_abort " + $ongoing_render_settings[0] )
      $abort_button;

    popupMenu
      -parent $abort_button
      //-button 1
      -postMenuCommand $postmenu_command
      $menu_name;
  }
  else
  {
    shelfButton
      -edit
      -enable true
      -annotation "Abort Rendering"
      -command ""
      $abort_button;

    popupMenu
      -parent $abort_button
      -button 1
      -postMenuCommand $postmenu_command
      $menu_name;
  }
}

//
// Assigns the (first) shading group of the specified shader to the specified
// objects. This will also set the current selection to $selectionList.
//
proc assignShaderToObjects( string $shader, string $selectionList[] )
{
  string $groups[] = `listConnections
    -source false -destination true -type shadingEngine $shader`;

  string $shading_group = "";

  if (size($groups) > 0)
    $shading_group = $groups[0];

  select $selectionList;

  if ($shading_group != "" && size($selectionList) > 0)
  {
    catchQuiet(`hyperShade -assign $shading_group`);
  }
}

global proc DSH_assignShaderToSelection( string $shader )
{
  assignShaderToObjects( $shader, `ls -sl` );
}

global proc DSH_createAndAssignShader(string $shader_type)
{
  createAndAssignShader( $shader_type, "" );
}

/*
  Creates a new incandescence light and set the current selection as its
  selected geometry.
*/
global proc DSH_createIncandescenceLight()
{
  string $selection[] = `ls -sl`;
  string $light = `shadingNode -asLight dlIncandescenceLightShape`;

  for( $curr_sel in $selection )
  {
    // Use the same filtering as the delight object and set selector to reject
    // irrelevant objects in the selection.
    //
    if( DOS_isGeoOrSet( $curr_sel ) )
    {
      DL_connectNodeToMultiMessagePlug(
        $curr_sel,
        ( $light + "._3delight_geometry" ) );
    }
  }
}

/*
  Creates a new Area light and sets its decay rate to quadratic.
*/
global proc DSH_createQuadraticAreaLight()
{
  DL_createQuadraticAreaLight();
}

/*
  Creates a new Point light and sets its decay rate to quadratic.
*/
global proc DSH_createQuadraticPointLight()
{
  DL_createQuadraticPointLight();
}

/*
  Creates a new Spot light and sets its decay rate to quadratic.
*/
global proc DSH_createQuadraticSpotLight()
{
  DL_createQuadraticSpotLight();
}

/*
  Creates a new Directional light.
*/
global proc DSH_createDirectionalLight()
{
  DL_createDirectionalLight();
}

/*
  Creates a new Environment light.
*/
global proc DSH_createEnvironmentLight()
{
  DL_createEnvironmentLight();
}


/*
  Creates a new set and set the current selection as its members

  The current selection remains untouched. This is what is done by Maya
  when creating a standard set.
*/
global proc string DSH_createSet()
{
  string $set = `createNode -skipSelect dlSet`;

  if (size(`ls -sl`) > 0)
  {
    // Make the current selection members of this new set.
    sets -e -add $set;
  }

  return $set;
}

global proc
DSH_buildLaunchRenderMenu(string $partial_menu_name)
{
  string $menu_name = "DL_" + $partial_menu_name + "RenderSettingsMenu";

  DL_buildRenderSettingsMenu(
    $partial_menu_name,
    "NSI_interactiveRender",
    1,
    "dlRenderSettings");

  menuItem
    -parent $menu_name
    -divider true
    -insertAfter "";
  menuItem
    -parent $menu_name
    -enable false
    -label "Render with:"
    -insertAfter "";
}

global proc
DSH_buildLaunchIPRMenu(string $partial_menu_name)
{
  string $menu_name = "DL_" + $partial_menu_name + "RenderSettingsMenu";

  DL_buildRenderSettingsMenu(
    $partial_menu_name,
    "NSI_liveRender",
    1,
    "dlRenderSettings");

  menuItem
    -parent $menu_name
    -divider true
    -insertAfter "";
  menuItem
    -parent $menu_name
    -enable false
    -label "Launch IPR with:"
    -insertAfter "";
}

global proc
DSH_buildAbortRenderMenu(string $partial_menu_name)
{
  string $menu_name = "DL_" + $partial_menu_name + "RenderSettingsMenu";

  DL_buildAbortMenu( $menu_name );

  menuItem
    -parent $menu_name
    -label "All Renderings"
    -command "NSI_abort \"\""
    -insertAfter "";

  menuItem
    -parent $menu_name
    -divider true
    -insertAfter "";
  menuItem
    -parent $menu_name
    -enable false
    -label "Abort Render:"
    -insertAfter "";
}

global proc
DSH_buildSelectMenu(
  string $menu_name,
  string $node_type,
  string $node_nice_name,
  string $header_cmd)
{
  DL_buildChoiceMenuWithHeader(
    $menu_name,
    ("ls -type " + $node_type),
    "",
    $node_nice_name,
    "select -r -ne",
    $header_cmd,
    ("Select " + $node_nice_name + ":"),
    ("<no " + $node_nice_name + ">"));
}

global proc DSH_buildAssignExistingMenu(
  string $menu_name,
  string $type,
  string $type_nice_name )
{
  DL_buildChoiceMenuWithHeader(
    $menu_name,
    ("ls -type " + $type),
    "",
    $type_nice_name,
    "DSH_assignShaderToSelection",
    "",
    ("Assign Existing " + $type_nice_name + ":"),
    ("<no " + $type_nice_name + ">"));
}

proc
editShelfButton(
  string $button_name,
  string $annotation,
  string $icon_name,
  string $label,
  string $cmd)
{
  shelfButton
    -edit
    -enable 1
    -width 35
    -height 35
    -annotation $annotation
    -enableBackground 0
    -align "center"
    -label $annotation
    -labelOffset 0
    -font "plainLabelFont"
    -imageOverlayLabel $label
    -overlayLabelColor 1 0.223529 0.223529
    -overlayLabelBackColor 0 0 0 0.2
    -image $icon_name
    -style "iconOnly"
    -marginWidth 1
    -marginHeight 1
    -command $cmd
    -sourceType "mel"
    $button_name;
}

proc recreateSelectPopupMenu(
  string $button,
  string $menu_name,
  string $type,
  string $type_nice_name,
  string $header_cmd )
{
  string $menus[] = `shelfButton -q -popupMenuArray $button`;
  for($curr_menu in $menus)
    deleteUI -menu $curr_menu;

  string $cmd = "DSH_buildSelectMenu(" +
    "\"" + $menu_name + "\", " +
    "\"" + $type + "\", " +
    "\"" + $type_nice_name + "\", " +
    "\"" + $header_cmd + "\")";

  popupMenu -parent $button -postMenuCommand $cmd $menu_name;
}

proc recreateAssignExistingPopupMenu(
  string $button,
  string $menu_name,
  string $type,
  string $type_nice_name )
{
  string $menus[] = `shelfButton -q -popupMenuArray $button`;
  for($curr_menu in $menus)
    deleteUI -menu $curr_menu;

  string $cmd = "DSH_buildAssignExistingMenu(" +
    "\"" + $menu_name + "\", " +
    "\"" + $type + "\", " +
    "\"" + $type_nice_name + "\")";

  popupMenu -parent $button -postMenuCommand $cmd $menu_name;
}

global proc
DSH_addShelf()
{
  // We can technically support older versions but we need to provide xpm
  // icons.
  //
  global string $gShelfTopLevel;

  // Store currently selected tab because it is changed when adding a new one
  //
  int $curr_tab_index = `shelfTabLayout -q -selectTabIndex $gShelfTopLevel`;

  string $delight_shelf_tab_name = DSH_getCurrentVersionShelfName();

  // Avoid adding twice our shelf
  string $shelf_tabs[] = `shelfTabLayout -q -childArray $gShelfTopLevel`;
  int $shelf_exists = 0;

  for($curr_tab in $shelf_tabs)
  {
    if($curr_tab == $delight_shelf_tab_name)
    {
      shelfTabLayout
        -e
        -tabLabel $delight_shelf_tab_name "3Delight"
        $gShelfTopLevel;

      $shelf_exists = 1;
      break;
    }
    else if(
      match("^_3DelightV", $curr_tab) != "" ||
      match("^_3Delight_v", $curr_tab) != "" || // pre-9.0 era shelf
      match("^_3Delight_NSI_v", $curr_tab) != "" || // 9.0 dev shelf
      $curr_tab == "_3Delight")
    {
      // Incorrect version of the shelf, so reconstruct it.
      deleteShelfTab($curr_tab);
      $shelf_exists = 0;
    }
  }

  if ($shelf_exists == 0)
  {
    addNewShelfTab($delight_shelf_tab_name);

    // Update the label to display "3Delight"
    shelfTabLayout
      -e
      -tabLabel $delight_shelf_tab_name "3Delight"
      $gShelfTopLevel;
  }

  // The commands associated with the shelf buttons. They will be used to
  // identify existing shelf buttons (which have generic names).
  //
  string $launch_render_cmd = "DSH_launchRender()";
  string $launch_ipr_cmd = "DSH_launchIPR()";
  string $abort_render_cmd = "//Abort Render";
  // Abort button has a varying command, so match the icon name instead.
  string $abort_icon = "shelf_abortRender.png";
  string $abort_dummy_cmd = "//Abort Dummy";
  string $abort_dummy2_cmd = "//Abort Dummy2";

  string $create_render_settings_cmd = "select `RS_create`";
  string $select_render_settings_cmd = "//Select Render Settings";

  string $create_directionalLight_cmd ="DSH_createDirectionalLight";
  string $create_pointLight_cmd ="DSH_createQuadraticPointLight";
  string $create_spotLight_cmd ="DSH_createQuadraticSpotLight";
  string $create_areaLight_cmd ="DSH_createQuadraticAreaLight";
  string $create_incLight_cmd = "DSH_createIncandescenceLight";
  string $create_env_cmd ="DSH_createEnvironmentLight";
  string $create_set_cmd = "DSH_createSet";
  string $create_vdb_cmd = "DL_createNode(\"dlOpenVDBShape\", \"\")";

  string $dl_principled_cmd = "DSH_createAndAssignShader(\"dlPrincipled\")";
  string $dl_skin_cmd = "DSH_createAndAssignShader(\"dlSkin\")";
  string $dl_hair_cmd = "DSH_createAndAssignShader(\"dlHairAndFur\")";
  string $dl_metal_cmd = "DSH_createAndAssignShader(\"dlMetal\")";
  string $dl_glass_cmd = "DSH_createAndAssignShader(\"dlGlass\")";
  string $dl_thin_cmd = "DSH_createAndAssignShader(\"dlThin\")";
  string $dl_toon_cmd = "DSH_createAndAssignShader(\"dlToon\")";
  string $dl_substance_cmd = "DSH_createAndAssignShader(\"dlSubstance\")";
  string $dl_layered_cmd = "DSH_createAndAssignShader(\"dlLayeredMaterial\")";

  // Figure if we must add new buttons or reuse existing ones (and possibly
  // modify them)
  //
  string $launch_render = "";
  string $launch_ipr = "";
  string $abort_render = "";
  string $abort_dummy = "";
  string $abort_dummy2 = "";
  string $create_render_pass = "";
  string $select_render_pass = "";

  string $separator1 = "";

  string $directionalLight = "";
  string $pointLight = "";
  string $spotLight = "";
  string $areaLight = "";
  string $incLight = "";
  string $env = "";
  string $set = "";
  string $vdb = "";

  string $separator2 = "";

  string $dl_principled = "";
  string $dl_skin = "";
  string $dl_hair = "";
  string $dl_metal = "";
  string $dl_glass = "";
  string $dl_thin = "";
  string $dl_toon = "";
  string $dl_substance = "";
  string $dl_layered = "";

  if ($shelf_exists != 0)
  {
    string $shelf_children[];
    $shelf_children = `shelfLayout -q -childArray $delight_shelf_tab_name`;

    for ($curr_child in $shelf_children)
    {
      string $cmd = `shelfButton -q -command $curr_child`;
      string $icon = `shelfButton -q -image $curr_child`;

      if ($cmd == $launch_render_cmd)
        $launch_render = $curr_child;
      else if ($cmd == $launch_ipr_cmd)
        $launch_ipr = $curr_child;
      else if ($icon == $abort_icon)
        $abort_render = $curr_child;
      else if ($cmd == $abort_dummy_cmd)
        $abort_dummy = $curr_child;
      else if ($cmd == $abort_dummy2_cmd)
        $abort_dummy2 = $curr_child;
      else if ($cmd == $create_render_settings_cmd)
        $create_render_pass = $curr_child;
      else if ($cmd == $select_render_settings_cmd)
        $select_render_pass = $curr_child;
      else if ($cmd == $create_directionalLight_cmd)
        $directionalLight = $curr_child;
      else if ($cmd == $create_pointLight_cmd)
        $pointLight = $curr_child;
      else if ($cmd == $create_spotLight_cmd)
        $spotLight = $curr_child;
      else if ($cmd == $create_areaLight_cmd)
        $areaLight = $curr_child;
      else if ($cmd == $create_incLight_cmd)
        $incLight = $curr_child;
      else if ($cmd == $create_env_cmd)
        $env = $curr_child;
      else if ($cmd == $create_set_cmd)
        $set = $curr_child;
      else if ($cmd == $create_vdb_cmd)
        $vdb = $curr_child;
      else if ($cmd == $dl_principled_cmd)
        $dl_principled = $curr_child;
      else if ($cmd == $dl_skin_cmd)
        $dl_skin = $curr_child;
      else if ($cmd == $dl_hair_cmd)
        $dl_hair = $curr_child;
      else if ($cmd == $dl_metal_cmd)
        $dl_metal = $curr_child;
      else if ($cmd == $dl_glass_cmd)
        $dl_glass = $curr_child;
      else if ($cmd == $dl_thin_cmd)
        $dl_thin = $curr_child;
      else if ($cmd == $dl_toon_cmd)
        $dl_toon = $curr_child;
      else if ($cmd == $dl_substance_cmd)
        $dl_substance = $curr_child;
      else if ($cmd == $dl_layered_cmd)
        $dl_layered = $curr_child;
      else if ($cmd == "")
      {
        if ($separator1 == "")
          $separator1 = $curr_child;
        else
          $separator2 = $curr_child;
      }
    }
  }

  // Launch Render button
  //
  if ($launch_render == "")
    $launch_render = `shelfButton -parent $delight_shelf_tab_name`;

  editShelfButton(
    $launch_render,
    "Render Active Render Settings",
    "shelf_launchRender.png",
    "",
    $launch_render_cmd);

  string $menus[];
  $menus = `shelfButton -q -popupMenuArray $launch_render`;
  for($curr_menu in $menus)
    deleteUI -menu $curr_menu;

  string $partial_menu_name = "_shelfLaunchRender_";
  string $menu_name = "DL_" + $partial_menu_name + "RenderSettingsMenu";
  string $cmd = "DSH_buildLaunchRenderMenu(\"" + $partial_menu_name + "\")";

  popupMenu -parent $launch_render -postMenuCommand $cmd $menu_name;

  // Launch IPR session button
  //
  if ($launch_ipr == "")
    $launch_ipr = `shelfButton -parent $delight_shelf_tab_name`;

  editShelfButton(
    $launch_ipr,
    "Launch IPR Session",
    "shelf_launchIPR.png",
    "",
    $launch_ipr_cmd);

  $base_name = "_shelfLaunchIPR_";
  $menus = `shelfButton -q -popupMenuArray $launch_ipr`;
  for($curr_menu in $menus)
    deleteUI -menu $curr_menu;

  $partial_menu_name = $base_name;
  $menu_name = "DL_" + $partial_menu_name + "RenderSettingsMenu";
  $cmd = "DSH_buildLaunchIPRMenu(\"" + $partial_menu_name + "\")";

  popupMenu -parent $launch_ipr -postMenuCommand $cmd $menu_name;

  // Abort Rendering button
  //
  if ($abort_render == "")
    $abort_render = `shelfButton -parent $delight_shelf_tab_name`;

  editShelfButton(
    $abort_render,
    "Abort Rendering",
    "shelf_abortRender.png",
    "",
    $abort_render_cmd);

  shelfButton -edit -enable false $abort_render;

  $base_name = "_shelfAbortRender_";
  $menus = `shelfButton -q -popupMenuArray $abort_render`;
  for($curr_menu in $menus)
    deleteUI -menu $curr_menu;

  $partial_menu_name = $base_name;
  $menu_name = "DL_" + $partial_menu_name + "RenderSettingsMenu";
  $cmd = "DSH_buildAbortRenderMenu(\"" + $partial_menu_name + "\")";

  popupMenu
    -parent $abort_render
    -postMenuCommand $cmd
    $menu_name;

  string $cb = "DSH_renderingStartedOrEnded " +
    "\"" + $abort_render + "\" " +
    "\"" + $menu_name + "\" " +
    "\"" + encodeString( $cmd ) + "\"";

  // Dummy button used as a script job parent.
  if ($abort_dummy == "")
    $abort_dummy = `shelfButton -parent $delight_shelf_tab_name`;

  shelfButton -edit -manage false -command $abort_dummy_cmd $abort_dummy;

  scriptJob -parent $abort_render -conditionTrue "dlRenderingStarted" $cb;
  scriptJob -parent $abort_dummy -conditionTrue "dlRenderingEnded" $cb;

  // Another dummy button used as a script job parent.
  //
  // The scene opened script job is necessary because while this event is
  // monitored in C++ and will cause ongoing renderings to be aborted, the
  // usual callback flow of events in this context is incomplete.
  // The SceneOpened event is triggered after a new scene or an open scene.
  //
  if ($abort_dummy2 == "")
    $abort_dummy2 = `shelfButton -parent $delight_shelf_tab_name`;

  shelfButton -edit -manage false -command $abort_dummy2_cmd $abort_dummy2;

  // Refresh abort render button status upon new scene / open scene event.
  scriptJob -parent $abort_dummy2 -event SceneOpened $cb;

  // Create Render Settings button
  //
  if ($create_render_pass == "")
    $create_render_pass = `shelfButton -parent $delight_shelf_tab_name`;

  editShelfButton(
    $create_render_pass,
    "Create Render Settings",
    "shelf_renderSettings.png",
    "",
    "select `RS_create`");

  $menus = `shelfButton -q -popupMenuArray $create_render_pass`;
  for($curr_menu in $menus)
    deleteUI -menu $curr_menu;

  // Select Render Pass Button
  //
  if ($select_render_pass == "")
    $select_render_pass = `shelfButton -parent $delight_shelf_tab_name`;

  editShelfButton(
    $select_render_pass,
    "Select Render Settings",
    "shelf_selectRenderSettings.png",
    "",
    $select_render_settings_cmd);

  $base_name = "_shelfSelectRenderSettings_";
  $cmd = "DL_buildRenderSettingsMenu(\"" + $base_name + "\", "
      + "\"select\", 1, \"dlRenderSettings\")";

  popupMenu
    -parent $select_render_pass
    -button 1
    -postMenuCommand $cmd
    ("DL_" + $base_name + "RenderSettingsMenu");

  // Separator
  //
  if ($separator1 == "")
    $separator1 = `shelfButton -parent $delight_shelf_tab_name`;

  editShelfButton(
    $separator1,
    "Separator",
    "shelf_separator.png",
    "",
    "");

  // Create Directional Light
  //
  if ($directionalLight == "")
    $directionalLight = `shelfButton -parent $delight_shelf_tab_name`;

  editShelfButton(
    $directionalLight,
    "Create a Directional Light",
    "shelf_dlDirectionalLight.png",
    "",
    $create_directionalLight_cmd);

  recreateSelectPopupMenu(
    $directionalLight,
    "DL_shelf_selectDirectionalLightMenu",
    "directionalLight",
    "Directional Light",
    "");

  // Create quadratic Point Light
  //
  if ($pointLight == "")
    $pointLight = `shelfButton -parent $delight_shelf_tab_name`;

  editShelfButton(
    $pointLight,
    "Create a Point Light with Quadratic Decay",
    "shelf_quadraticPointLight.png",
    "",
    $create_pointLight_cmd);

  recreateSelectPopupMenu(
    $pointLight,
    "DL_shelf_selectPointLightMenu",
    "pointLight",
    "Point Light",
    "");

  // Create quadratic Spot Light
  //
  if ($spotLight == "")
    $spotLight = `shelfButton -parent $delight_shelf_tab_name`;

  editShelfButton(
    $spotLight,
    "Create a Spot Light with Quadratic Decay",
    "shelf_quadraticSpotLight.png",
    "",
    $create_spotLight_cmd);

  recreateSelectPopupMenu(
    $spotLight,
    "DL_shelf_selectSpotLightMenu",
    "spotLight",
    "Spot Light",
    "");

  // Create quadratic Area Light
  //
  if ($areaLight == "")
    $areaLight = `shelfButton -parent $delight_shelf_tab_name`;

  editShelfButton(
    $areaLight,
    "Create an Area Light with Quadratic Decay",
    "shelf_quadraticAreaLight.png",
    "",
    $create_areaLight_cmd);

  string $areaLightHeaderCmd =
    "menuItem -enable false -label \"Create Area Light:\";" +
    "menuItem -divider true;" +
    "menuItem -label \"Square\" -c \"DL_createQuadraticShapedAreaLight(0)\";" +
    "menuItem -label \"Disk\" -c \"DL_createQuadraticShapedAreaLight(1)\";" +
    "menuItem -label \"Sphere\" -c \"DL_createQuadraticShapedAreaLight(2)\";" +
    "menuItem -label \"Cylinder\" -c \"DL_createQuadraticShapedAreaLight(3)\";"+
    "menuItem -divider true;" +
    "menuItem -divider true;" +
    "menuItem -enable false -label \"Select Area Light:\";" +
    "menuItem -divider true;";
  $areaLightHeaderCmd = encodeString($areaLightHeaderCmd);

  recreateSelectPopupMenu(
    $areaLight,
    "DL_shelf_selectAreaLightMenu",
    "areaLight",
    "Area Light",
    $areaLightHeaderCmd);

  // Create Incandescence Light
  //
  if ($incLight == "")
    $incLight = `shelfButton -parent $delight_shelf_tab_name`;

  editShelfButton(
    $incLight,
    "Create an Incandescence Light",
    "shelf_dlIncandescenceLightShape.png",
    "",
    $create_incLight_cmd);

  recreateSelectPopupMenu(
    $incLight,
    "DL_shelf_selectIncandescenceLightMenu",
    "dlIncandescenceLightShape",
    "Incandescence Light",
    "");

  // Create 3Delight Environment
  //
  if ($env == "")
    $env = `shelfButton -parent $delight_shelf_tab_name`;

  editShelfButton(
    $env,
    "Create an Environment",
    "shelf_dlEnvironmentShape.png",
    "",
    $create_env_cmd);

  recreateSelectPopupMenu(
    $env,
    "DL_shelf_selectEnvironmentMenu",
    "dlEnvironmentShape",
    "Environment",
    "");

  // Create Set
  //
  if ($set == "")
    $set = `shelfButton -parent $delight_shelf_tab_name`;

  editShelfButton(
    $set,
    "Create a Set",
    "shelf_dlSet.png",
    "",
    $create_set_cmd);

  recreateSelectPopupMenu(
    $set,
    "DL_shelf_selectSetMenu",
    "dlSet",
    "Set",
    "");

  // VDB plugin create buttons
  //
  if ($vdb == "")
    $vdb = `shelfButton -parent $delight_shelf_tab_name`;
  else
    shelfButton -edit -manage true $vdb;

  editShelfButton(
    $vdb,
    "Create OpenVDB",
    "shelf_dlOpenVDBShape.png",
    "",
    $create_vdb_cmd);

  recreateSelectPopupMenu(
    $vdb,
    "DL_shelf_selectdlOpenVDBShapeMenu",
    "dlOpenVDBShape",
    "Open VDB",
    "");

  // Separator
  if ($separator2 == "")
    $separator2 = `shelfButton -parent $delight_shelf_tab_name`;

  editShelfButton(
    $separator2,
    "Separator",
    "shelf_separator.png",
    "",
    "");

  // Principled button
  //
  if ($dl_principled == "")
    $dl_principled = `shelfButton -parent $delight_shelf_tab_name`;

  editShelfButton(
    $dl_principled,
    "Assign new Principled",
    "shelf_dlPrincipled.png",
    "",
    $dl_principled_cmd);

  recreateAssignExistingPopupMenu(
    $dl_principled,
    "DL_shelf_assignExistingPrincipledMenu",
    "dlPrincipled",
    "Principled");

  // Skin button
  //
  if ($dl_skin == "")
    $dl_skin = `shelfButton -parent $delight_shelf_tab_name`;

  editShelfButton(
    $dl_skin,
    "Assign new Skin",
    "shelf_dlSkin.png",
    "",
    $dl_skin_cmd);

  recreateAssignExistingPopupMenu(
    $dl_skin,
    "DL_shelf_assignExistingSkinMenu",
    "dlSkin",
    "Skin");

  // Hair & Fur button
  //
  if ($dl_hair == "")
    $dl_hair = `shelfButton -parent $delight_shelf_tab_name`;

  editShelfButton(
    $dl_hair,
    "Assign new Hair and Fur",
    "shelf_dlHairAndFur.png",
    "",
    $dl_hair_cmd);

  recreateAssignExistingPopupMenu(
    $dl_hair,
    "DL_shelf_assignExistingHairAndFurMenu",
    "dlHairAndFur",
    "Hair and Fur");

  // Metal button
  //
  if ($dl_metal == "")
    $dl_metal = `shelfButton -parent $delight_shelf_tab_name`;

  editShelfButton(
    $dl_metal,
    "Assign new Metal",
    "shelf_dlMetal.png",
    "",
    $dl_metal_cmd);

  recreateAssignExistingPopupMenu(
    $dl_metal,
    "DL_shelf_assignExistingMetalMenu",
    "dlMetal",
    "Metal");

  // 3Delight Glass button
  //
  if ($dl_glass == "")
    $dl_glass = `shelfButton -parent $delight_shelf_tab_name`;

  editShelfButton(
    $dl_glass,
    "Assign new Glass",
    "shelf_dlGlass.png",
    "",
    $dl_glass_cmd);

  recreateAssignExistingPopupMenu(
    $dl_glass,
    "DL_shelf_assignExistingGlassMenu",
    "dlGlass",
    "Glass");

  // Thin button
  //
  if ($dl_thin == "")
    $dl_thin = `shelfButton -parent $delight_shelf_tab_name`;

  editShelfButton(
    $dl_thin,
    "Assign new Thin",
    "shelf_dlThin.png",
    "",
    $dl_thin_cmd);

  recreateAssignExistingPopupMenu(
    $dl_thin,
    "DL_shelf_assignExistingThinMenu",
    "dlThin",
    "Thin");

  // Toon button
  //
  if ($dl_toon == "")
    $dl_toon = `shelfButton -parent $delight_shelf_tab_name`;

  editShelfButton(
    $dl_toon,
    "Assign new Toon",
    "shelf_dlToon.png",
    "",
    $dl_toon_cmd);

  recreateAssignExistingPopupMenu(
    $dl_toon,
    "DL_shelf_assignExistingToonMenu",
    "dlToon",
    "Toon");

  // 3Delight Substance button
  //
  if ($dl_substance == "")
    $dl_substance = `shelfButton -parent $delight_shelf_tab_name`;

  editShelfButton(
    $dl_substance,
    "Assign new Substance",
    "shelf_dlSubstance.png",
    "",
    $dl_substance_cmd);

  recreateAssignExistingPopupMenu(
    $dl_substance,
    "DL_shelf_assignExistingSubstanceMenu",
    "dlSubstance",
    "Substance");

  // Layered Material button
  //
  if ($dl_layered == "")
    $dl_layered = `shelfButton -parent $delight_shelf_tab_name`;

  editShelfButton(
    $dl_layered,
    "Assign new Layered Material",
    "shelf_dlLayeredMaterial.png",
    "",
    $dl_layered_cmd);

  recreateAssignExistingPopupMenu(
    $dl_layered,
    "DL_shelf_assignExistingLayeredMenu",
    "dlLayeredMaterial",
    "Layered Material");

  // Restore selected tab
  //
  if($curr_tab_index > 0)
  {
    tabLayout -e -selectTabIndex $curr_tab_index $gShelfTopLevel;
    optionVar -iv "selectedShelf" $curr_tab_index;
  }
}

