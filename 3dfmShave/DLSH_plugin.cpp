#include <maya/MGlobal.h>
#include <maya/MFnPlugin.h>
#include <maya/MModelMessage.h>
#include <maya/MSceneMessage.h>

#include "dlInterface.h"
#include "DLSH_cacheShave.h"

_3DL_ALWAYS_EXPORT MStatus initializePlugin(MObject);
_3DL_ALWAYS_EXPORT MStatus uninitializePlugin(MObject);

MStatus
initializePlugin( MObject obj )
{
  MString versionString;
  versionString = 2.000;

  MFnPlugin plugin( obj, "soho vfx", versionString.asChar(), "Any");
  
  return plugin.registerCommand("delightCacheShave",
                                DLSH_cacheShave::creator,
                                DLSH_cacheShave::newSyntax);
}

MStatus
uninitializePlugin( MObject obj)
{
  MStatus   result;
  MFnPlugin plugin( obj );

  result = plugin.deregisterCommand("delightCacheShave");
  return result;
}
