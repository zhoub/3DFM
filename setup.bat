@echo off

rem Set DEVROOT to the location of this script, as a cygwin path. Inspired by
rem stuff in winenv.bat from 3Delight.
for /f "usebackq tokens=*" %%a in (`%ComSpec% /C "cygpath -au %0 | sed -e 's,/setup\\(\\.bat\\)\\?,,'"`) do set DEVROOT=%%a

rem PLAT and OS should have been set by the 3Delight winenv.bat script

rem 3Delight home directory where various stuff is needed for the build.
if not "%HOME_3DELIGHT%" == "" goto home_3delight_defined
set HOME_3DELIGHT=q:
:home_3delight_defined

rem Convert DEVROOT to windows path so we can set the MAYA paths.
for /f "usebackq tokens=*" %%a in (`cygpath -w %DEVROOT%`) do set WIN_DEVROOT=%%a

rem Convert DEVROOT to windows path with slashes to pass to innoSetup
for /f "usebackq tokens=*" %%a in (`cygpath -m %DEVROOT%`) do set WIN_DEVROOT_WITH_SLASHES=%%a

rem # Set Maya environment (only needed to run maya, not for compiling)
set MAYA_PLUG_IN_PATH=%WIN_DEVROOT%\build\%PLAT%\2022\plugins;%MAYA_PLUG_IN_PATH%
set MAYA_PLUG_IN_PATH=%WIN_DEVROOT%\build\%PLAT%\2020\plugins;%MAYA_PLUG_IN_PATH%
set MAYA_PLUG_IN_PATH=%WIN_DEVROOT%\build\%PLAT%\2019\plugins;%MAYA_PLUG_IN_PATH%
set MAYA_PLUG_IN_PATH=%WIN_DEVROOT%\build\%PLAT%\2018\plugins;%MAYA_PLUG_IN_PATH%
set MAYA_SCRIPT_PATH=%WIN_DEVROOT%\build\%PLAT%\scripts;%MAYA_SCRIPT_PATH%
set PYTHONPATH=%WIN_DEVROOT%\build\%PLAT%\scripts;%PYTHONPATH%
set MAYA_RENDER_DESC_PATH=%WIN_DEVROOT%\render_desc;%MAYA_RENDER_DESC_PATH%
set XBMLANGPATH=%WIN_DEVROOT%\icons;%XBMLANGPATH%
set MAYA_PRESET_PATH=%WIN_DEVROOT%\presets;%MAYA_PRESET_PATH%
set _3DFM_OSL_PATH=%WIN_DEVROOT%\build\%PLAT%\osl;%_3DFM_OSL_PATH%
set _3DFM_USER_FRAGMENT_PATH=%WIN_DEVROOT%\shaderFragments
