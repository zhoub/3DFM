<fragment 
    uiName="dlGGX"
    name="dlGGX"
    type="plumbing"
    class="ShadeFragment"
    version="1.0">
    <description><![CDATA[GGX Specular fragment]]></description>
    <properties>
        <float3  name="color" />
        <float3  name="edge_color" />
        <float  name="roughness" />
        <float3  name="specularI" />
        <float  name="F0" />
        <float  name="metallic" />
        <float4  name="shaderGeomInput" />    
        <float3  name="Nw" flags="varyingInputParam" />
        <float3  name="SLw" />
        <float3  name="Vw" flags="varyingInputParam" />
    </properties>
    <values>
        <float3 name="color" value="0.000000,0.000000,0.000000"  />
        <float3 name="edge_color" value="0.000000,0.000000,0.000000"  />
        <float name="F0" value="0.04"  />
        <float name="roughness" value="0.300000"  />
    </values>
    <outputs>
            <float3 name="dlGGX" />
    </outputs>
    <implementation>
        <implementation render="OGSRenderer" language="GLSL" lang_version="3.0">
            <function_name val="dlGGX" />
            <source>
<![CDATA[


vec3 fresnel_dieletric_conductor(vec3 Eta, vec3 Etak, float CosTheta)
{  
   float CosTheta2 = CosTheta * CosTheta;
   float SinTheta2 = 1 - CosTheta2;
   vec3 Eta2 = Eta * Eta;
   vec3 Etak2 = Etak * Etak;

   vec3 t0 = Eta2 - Etak2 - SinTheta2;
   vec3 a2plusb2 = sqrt(t0 * t0 + 4 * Eta2 * Etak2);
   vec3 t1 = a2plusb2 + CosTheta2;
   vec3 a = sqrt(0.5f * (a2plusb2 + t0));
   vec3 t2 = 2 * a * CosTheta;
   vec3 Rs = (t1 - t2) / (t1 + t2);

   vec3 t3 = CosTheta2 * a2plusb2 + SinTheta2 * SinTheta2;
   vec3 t4 = t2 * SinTheta2;   
   vec3 Rp = Rs * (t3 - t4) / (t3 + t4);

   return 0.5 * (Rp + Rs);
}


float G1V(float dotNV, float k)
{
    return 1.0f/(dotNV*(1.0f-k)+k);
}


float n_min(float r)
{
    return (1.0-r)/(1.0+r);
}


float n_max(float r)
{
    float sqrtr = sqrt( r );
    return (1.0+sqrtr)/(1.0-sqrtr);
}


// Gulbrandsen_Mapping Equation 12 of [4]
float get_n(float r, float g)
{
    return n_min(r)*g + (1.0-g)*n_max(r);
}


// Gulbrandsen_Mapping Equation 2 of [4] 
float get_k2(float r, float n)
{
    float nplusone = n+1.0;
    float nminusone = n-1.0;
    float nr = nplusone*nplusone*r - nminusone*nminusone;
    return nr/(1.0-r);
}


// Gulbrandsen_Mapping that outputs only eta, skipping kappa for now
float Gulbrandsen_Mapping(
        float r, float g) // output float n, output float k)
{
    // Clamp parameters
    float rr = clamp(r, 0.0, 1.0 - 1e-3);

    // Remap map n and k like it's described in [4]
    float n = get_n(rr,g);
    return n;
    //k = sqrt(get_k2(rr,n));
}


// Gulbrandsen_Mapping that outputs only eta, skipping kappa for now, for R G B
vec3 Gulbrandsen_Mapping3(
        vec3 i_reflectivity, vec3 i_gamma) // , output color n, output color k2)
{   

    float result_x = Gulbrandsen_Mapping( i_reflectivity.x, i_gamma.x);
    float result_y = Gulbrandsen_Mapping( i_reflectivity.y, i_gamma.y);
    float result_z = Gulbrandsen_Mapping( i_reflectivity.z, i_gamma.z);

    return vec3(result_x,result_y,result_z);
}


float IntensityFromEta( float eta )
{
float intensity = ((eta-1.0)*(eta-1.0)) / ((eta+1.0)*(eta+1.0));
return intensity;
}


vec3 IntensityFromEta3( vec3 eta3 )
{
vec3 result;
result.x = IntensityFromEta( eta3.x );
result.y = IntensityFromEta( eta3.y );
result.z = IntensityFromEta( eta3.z );
return result;
}


vec3 dlGGX
( 
    vec3 color,
    vec3 edge_color,
    float roughness,
    vec3 specularI,
    float F0,
    float metallic,
    vec4 NL_NV_NH_VH,
    vec3 N,
    vec3 L,
    vec3 V
)
{
vec3 ggx;
vec3 F0rgb = vec3(F0,F0,F0);

// Matching smoothstep(0, 0.05, specular_level) from Principled
// F0 = specular_level * 0.08
// specular_level = F0 * 12.5
float fade = smoothstep(0.0, 0.05, F0 * 12.5); 

if (metallic > 0.0)
    {
    vec3 eta3 = Gulbrandsen_Mapping3(color,edge_color);
    F0rgb = mix(F0rgb,IntensityFromEta3(eta3),metallic);
    fade = mix(fade,1,metallic);
    }

float sigma2 = max(roughness * roughness, 0.0000001);

vec3 H = normalize(V+L);

float dotNL = saturate(dot(N,L));
float dotNV = saturate(dot(N,V));
float dotNH = saturate(dot(N,H));
float dotLH = saturate(dot(L,H));

float D, vis;
vec3 F3;

// D
float alphaSqr = sigma2*sigma2;
float pi = 3.14159;
float denom = dotNH * dotNH *(alphaSqr-1.0) + 1.0;
D = alphaSqr/(pi * denom * denom);

// F
float dotLH5 = pow(1.0f-dotLH,5);
F3.x = F0rgb.x + (1.0 - F0rgb.x )*(dotLH5);
F3.y = F0rgb.y + (1.0 - F0rgb.y )*(dotLH5);
F3.z = F0rgb.z + (1.0 - F0rgb.z )*(dotLH5);

// V
float k = sigma2/2.0;
vis = G1V(dotNL,k)*G1V(dotNV,k);

ggx.x = dotNL * D * F3.x * vis;
ggx.y = dotNL * D * F3.y * vis;
ggx.z = dotNL * D * F3.z * vis;

// at the end divide by Pi ( * 1/Pi ) (0.318309)
return ggx * specularI * 0.318309 * fade;
}
]]>
            </source>
        </implementation>
    </implementation>
</fragment>


